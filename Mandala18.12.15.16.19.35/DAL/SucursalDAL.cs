﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public sealed class SucursalDAL : AbstractDAL
    {
        #region Atributos, propiedades y constructores
        private Sucursal suc;

        public Sucursal Trat
        {
            get { return suc; }
            set { suc = value; }
        }
        public SucursalDAL()
        {

        }
        public SucursalDAL(Sucursal suc)
        {
            this.suc = suc;
        }
        #endregion


        public override void Insert()
        {
            string query = "INSERT INTO Sucursal(nombreSucursal,latitud,longitud,direccion,estado) VALUES (@nombreSucursal,@latitud,@longitud,@direccion,@estado)";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombreSucursal", suc.NombreSucursal);
                cmd.Parameters.AddWithValue("@latitud", suc.Latitud);
                cmd.Parameters.AddWithValue("@longitud", suc.Longitud);
                cmd.Parameters.AddWithValue("@direccion",suc.Direccion);
                cmd.Parameters.AddWithValue("@estado", suc.Estado);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override void Update()
        {
            string query = "UPDATE Sucursal SET nombreSucursal=@nombreSucursal, latitud=@latitud, longitud=@longitud, fUpdate=CURRENT_TIMESTAMP WHERE idSucursal=@idSucursal";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombreSucursal", suc.NombreSucursal);
                cmd.Parameters.AddWithValue("@latitud", suc.Latitud);
                cmd.Parameters.AddWithValue("@longitud", suc.Longitud);
                cmd.Parameters.AddWithValue("@idSucursal", suc.IdSucursal);
                cmd.Parameters.AddWithValue("@direccion", suc.Direccion);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public override void Delete()
        {
            string query = "UPDATE Sucursal SET estado=0 WHERE idSucursal=@idSucursal";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idSucursal", suc.IdSucursal);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public void Delete(int id)
        {
            string query = "UPDATE Sucursal SET estado=0 WHERE idSucursal=@idSucursal";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idSucursal", id);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override DataTable Select()
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectSucursales  ORDER BY 2";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                res = Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }

        public Sucursal Get(int idSucursal)
        {
            Sucursal res = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = "SELECT idSucursal, nombreSucursal, latitud, longitud, direccion, estado, fUpdate FROM Sucursal WHERE idSucursal=@idSucursal";
            try
            {
                cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idSucursal", idSucursal);
                dr = Metodos.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    res = new Sucursal(int.Parse(dr[0].ToString()), dr[1].ToString(), double.Parse(dr[2].ToString()), double.Parse(dr[3].ToString()), dr[4].ToString(), byte.Parse(dr[5].ToString()), DateTime.Parse(dr[6].ToString()));
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }

            return res;
        }
        public bool Existe(string nombre)
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = "SELECT idSucursal, nombreSucursal,latitud, longitud, direccion, estado, fUpdate FROM Sucursal WHERE nombreSucursal=@nombreSucursal";
            try
            {
                cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombreSucursal", nombre);
                dr = Metodos.ExecuteDataReaderCommand(cmd);
                return dr.Read();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
        }
    }
}
