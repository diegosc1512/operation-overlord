﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;
using System.Data.SqlClient;

namespace DAL
{
    public class HorarioEmpleadoDAL : AbstractDAL
    {
                #region Atributos, propiedades y constructores
        private HorarioEmpleado horemp;

        public HorarioEmpleado Horemp
        {
          get { return horemp; }
          set { horemp = value; }
        }
        public HorarioEmpleadoDAL ()
	        {

	        }       
        public HorarioEmpleadoDAL(HorarioEmpleado horemp)
        {
            this.horemp = horemp;
        }
        #endregion

        public override void Insert()
        {
            string query = "INSERT INTO HorarioEmpleado(lunes, martes, miercoles, jueves, viernes,sabado,domingo, horarioInicio, horarioFin,idEmpleado) values(@lunes,@martes,@miercoles, @jueves, @viernes, @sabado,@domingo,@horaInicio,@horaFin,@idEmpleado)";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@lunes",horemp.Lunes);
                cmd.Parameters.AddWithValue("@martes", horemp.Martes);
                cmd.Parameters.AddWithValue("@miercoles", horemp.Miercoles);
                cmd.Parameters.AddWithValue("@jueves", horemp.Jueves);
                cmd.Parameters.AddWithValue("@viernes", horemp.Viernes);
                cmd.Parameters.AddWithValue("@sabado", horemp.Sabado);
                cmd.Parameters.AddWithValue("@domingo", horemp.Domingo);
                cmd.Parameters.AddWithValue("@horaInicio", horemp.HoraInicio);
                cmd.Parameters.AddWithValue("@horaFin", horemp.HoraFin);
                //cmd.Parameters.AddWithValue("@idEmpleado", horemp.IdEmpleado);
                cmd.Parameters.AddWithValue("@idEmpleado", 1030);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Insert(int idEmpleado)
        {
            string query = "INSERT INTO HorarioEmpleado(lunes, martes, miercoles, jueves, viernes,sabado,domingo, horarioInicio, horarioFin,idEmpleado) values(@lunes,@martes,@miercoles, @jueves, @viernes, @sabado,@domingo,@horaInicio,@horaFin,@idEmpleado)";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@lunes", horemp.Lunes);
                cmd.Parameters.AddWithValue("@martes", horemp.Martes);
                cmd.Parameters.AddWithValue("@miercoles", horemp.Miercoles);
                cmd.Parameters.AddWithValue("@jueves", horemp.Jueves);
                cmd.Parameters.AddWithValue("@viernes", horemp.Viernes);
                cmd.Parameters.AddWithValue("@sabado", horemp.Sabado);
                cmd.Parameters.AddWithValue("@domingo", horemp.Domingo);
                cmd.Parameters.AddWithValue("@horaInicio", horemp.HoraInicio);
                cmd.Parameters.AddWithValue("@horaFin", horemp.HoraFin);
                //cmd.Parameters.AddWithValue("@idEmpleado", horemp.IdEmpleado);
                cmd.Parameters.AddWithValue("@idEmpleado", idEmpleado);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override void Update()
        {
            string query = "UPDATE HorarioEmpleado SET lunes=@lunes, martes=@martes, miercoles=@miercoles, jueves=@jueves, viernes=@viernes,sabado=@sabado,domingo=@domingo, horarioInicio=@horarioInicio, horarioFin=@horarioFin,fUpdate=CURRENT_TIMESTAMP WHERE idHorario=@idHorario";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@lunes", horemp.Lunes);
                cmd.Parameters.AddWithValue("@martes", horemp.Martes);
                cmd.Parameters.AddWithValue("@miercoles", horemp.Miercoles);
                cmd.Parameters.AddWithValue("@jueves", horemp.Jueves);
                cmd.Parameters.AddWithValue("@viernes", horemp.Viernes);
                cmd.Parameters.AddWithValue("@sabado", horemp.Sabado);
                cmd.Parameters.AddWithValue("@domingo", horemp.Domingo);
                cmd.Parameters.AddWithValue("@horarioInicio", horemp.HoraInicio);
                cmd.Parameters.AddWithValue("@horarioFin", horemp.HoraFin);
                cmd.Parameters.AddWithValue("@idHorario", horemp.IdHorario);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override void Delete()
        {
            string query = "UPDATE HorarioEmpleado SET estado=0 WHERE idHorario=@idHorario";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idHorario", horemp.IdHorario);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            } 
        }

        public void Delete(int id)
        {
            string query = "UPDATE HorarioEmpleado SET estado=0 WHERE idHorario=@idHorario";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idHorario", id);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public override DataTable Select()
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectHorariosEmpleados ";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                res = Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
        public HorarioEmpleado Get(int idHorario)
        {
            HorarioEmpleado res = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = "SELECT idHorario,lunes, martes, miercoles, jueves, viernes,sabado,domingo, horarioInicio, horarioFin,idEmpleado, fUpdate, estado FROM HorarioEmpleado WHERE idHorario=@idHorario";
            try
            {
                cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idHorario", idHorario);
                dr = Metodos.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    //res = new HorarioEmpleado(int.Parse(dr[0].ToString()), char.Parse(dr[1].ToString()), char.Parse(dr[2].ToString()), char.Parse(dr[3].ToString()), char.Parse(dr[4].ToString()), char.Parse(dr[5].ToString()), char.Parse(dr[6].ToString()), char.Parse(dr[7].ToString()), DateTime.Parse(dr[8].ToString()), DateTime.Parse(dr[9].ToString()), int.Parse(dr[10].ToString()), DateTime.Parse(dr[11].ToString()), byte.Parse(dr[12].ToString()));
                    res = new HorarioEmpleado(int.Parse(dr[0].ToString()), char.Parse(dr[1].ToString()), char.Parse(dr[2].ToString()), char.Parse(dr[3].ToString()), char.Parse(dr[4].ToString()), char.Parse(dr[5].ToString()), char.Parse(dr[6].ToString()), char.Parse(dr[7].ToString()), DateTime.Parse(dr[8].ToString()), DateTime.Parse(dr[9].ToString()), DateTime.Parse(dr[11].ToString()), byte.Parse(dr[12].ToString()));
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }

            return res;
        }
        /// <summary>
        /// Select Like del documento o nombre completo en funcion de la opcionLike
        /// </summary>
        /// <param name="opcionLike">0 es busqueda por documnento, 1 busqueda por nombre completo</param>
        /// <returns></returns>
        public DataTable SelectLikeCiFullName(byte opcionLike, string texto)
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwEmpleadoIdDocumentoFullName  ";

            switch (opcionLike)
            {
                case 0:
                    query = query + "WHERE Documento LIKE @texto ";
                    break;
                case 1:
                    query = query + "WHERE [Nombre completo] LIKE @texto ";
                    break;
            }
            query = query + " ORDER BY 2";


            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@texto", "%"+texto+"%");

                res = Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
    }
}
