﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
using System.IO;

namespace DAL
{
    public sealed class UsuarioDAL : AbstractDAL
    {
       
        #region atributos, propiedades  constructores
        private Usuario user;

        public Usuario User
        {
            get { return user; }
            set { user = value; }
        }
         public UsuarioDAL()
        {

        }
        public UsuarioDAL(Usuario user)
        {
            this.user = user;
        }

        #endregion
        #region Metodos de la Clase

        public override void Insert()
        {
           // string query = "INSERT INTO Usuario(nombreUsuario,rol,password) VALUES (@nombreUsuario , @rol , HASHBYTES('md5',@password), 1)"; //nota, el idpersona siempre se le asigna el valor de 1 y SE DEBE QUITAR EL ID AUTOINCREMENTABLE UNA VEZ SE REALICE LA RELACION CON PERSONA
            string query = "INSERT INTO Usuario(nombreUsuario,rol,password,email,estado,idusuario) VALUES (@nombreUsuario , @rol , HASHBYTES('md5',@password),'',1,1)";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombreUsuario", user.NombreUsuario);
                cmd.Parameters.AddWithValue("@password", user.Password);
                cmd.Parameters.AddWithValue("@rol", user.Rol);
                //cmd.Parameters.AddWithValue("@estado", user.Estado);
                //cmd.Parameters.AddWithValue("@idUsuario", user.IdUsuario);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override void Update()
        {
            string query = "UPDATE Usuario SET nombreUsuario=@nombreUsuario, password=HASHBYTES('md5',@password), rol=rol, estado=estado, fUpdate=CURRENT_TIMESTAMP WHERE nombreUsuario=@nombreUsuario";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombreUsuario", user.NombreUsuario);
                cmd.Parameters.AddWithValue("@password", user.Password);
                //cmd.Parameters.AddWithValue("@rol", user.Rol);
                //cmd.Parameters.AddWithValue("@estado", user.Estado);
                //cmd.Parameters.AddWithValue("@idUsuario", user.IdUsuario);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public  void Update2()
        {
            string query = "UPDATE Usuario SET nombreUsuario=@nombreUsuario, password=HASHBYTES('md5',@password), rol=@rol, estado=estado, fUpdate=CURRENT_TIMESTAMP WHERE nombreUsuario=@nombreUsuario";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombreUsuario", user.NombreUsuario);
                cmd.Parameters.AddWithValue("@password", user.Password);
                cmd.Parameters.AddWithValue("@rol", user.Rol);
                //cmd.Parameters.AddWithValue("@estado", user.Estado);
                //cmd.Parameters.AddWithValue("@idUsuario", user.IdUsuario);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override void Delete()
        {
            string query = "UPDATE Usuario SET estado=0 WHERE idUsuario=@idUsuario OR nombreUsuario=@nombreUsuario";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idUsuario", user.IdUsuario);
                cmd.Parameters.AddWithValue("@nombreUsuario", user.NombreUsuario);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override DataTable Select()
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectUsuario  ORDER BY 2";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                res = Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
        public  DataTable Select2()
        {
            DataTable res = new DataTable();
            string query = "nombreUsuario, CONVERT(varchar(MAX), password,1) AS password, rol FROM Usuario WHERE estado=1 AND idUsuario=@nombreUsuario";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                //string usuario=Sesion.VerUsuario;
                cmd.Parameters.AddWithValue("@nombreUsuario", Sesion.idSesion);
                res = Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
        public DataTable Login(string usuario, string password)
        {
            DataTable dt = new DataTable();
        

            string query = "SELECT idUsuario,nombreUsuario, rol FROM Usuario WHERE estado=1 AND  nombreUsuario=@nombreUsuario AND password=HASHBYTES('md5', @password)";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombreUsuario", usuario);
               // cmd.Parameters.AddWithValue("@password", password).SqlDbType = SqlDbType.VarChar;  //no se porque funciona, pero dejalo así, en la anterior version funciona con el comentado
                cmd.Parameters.AddWithValue("@password", password);
                
                dt = Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dt;
        }
        static string BytesToString(byte[] bytes)
        {
            using (var stream = new MemoryStream(bytes))
            {
                using (var streamReader = new StreamReader(stream))
                {
                    return streamReader.ReadToEnd();
                }
            }
        }
        
        
        public Usuario Get(int idUsuario)
        {
            Usuario res = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
           // string query = "SELECT idUsuario, nombreUsuario, password, rol, estado, idPersona, fUpdate FROM Usuario WHERE idUsuario=@idUsuario";
            string query = "SELECT idUsuario, nombreUsuario, password, rol, estado, idusuario, fUpdate FROM Usuario WHERE idUsuario=@idUsuario";
            try
            {
                cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idUsuario", idUsuario);
                dr = Metodos.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    //se debe agregar el dr de email
                  //  res = new Usuario(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), byte.Parse(dr[4].ToString()), int.Parse(dr[5].ToString()), DateTime.Parse(dr[6].ToString()));
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }

            return res;
        }
        public bool Existe(string nombre)
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = "SELECT idUsuario, nombreUsuario, password, rol, estado, idUsuario, fUpdate FROM Usuario WHERE nombreUsuario=@nombreUsuario AND estado=1";
            try
            {
                cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombreUsuario", nombre);
                dr = Metodos.ExecuteDataReaderCommand(cmd);
                return dr.Read();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }

        }
        #endregion

    }
}
