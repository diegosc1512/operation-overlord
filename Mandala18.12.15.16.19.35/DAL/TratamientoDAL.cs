﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public sealed class TratamientoDAL : AbstractDAL
    {
        #region Atributos, propiedades y constructores
        private Tratamiento trat;

        public Tratamiento Trat
        {
            get { return trat; }
            set { trat = value; }
        }
        public TratamientoDAL()
        {

        }
        public TratamientoDAL(Tratamiento trat)
        {
            this.trat = trat;
        }
        #endregion


        public override void Insert()
        {
            string query = "INSERT INTO Tratamiento(nombreTratamiento,descripcion,precio) VALUES (@nombre, @descripcion, @precio)";
            
            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombre", trat.NombreTratamiento);
                cmd.Parameters.AddWithValue("@descripcion", trat.Descripcion);
                cmd.Parameters.AddWithValue("@precio", trat.Precio);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override void Update()
        {
            string query = "UPDATE Tratamiento SET nombreTratamiento=@nombre, descripcion=@descripcion, precio=@precio, fUpdate=CURRENT_TIMESTAMP WHERE idTratamiento=@idTratamiento";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombre", trat.NombreTratamiento);
                cmd.Parameters.AddWithValue("@descripcion", trat.Descripcion);
                cmd.Parameters.AddWithValue("@precio", trat.Precio);
                cmd.Parameters.AddWithValue("@idTratamiento", trat.IdTratamiento);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override void Delete()
        {
            string query = "UPDATE Tratamiento SET estado=0 WHERE idTratamiento=@idTratamiento";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idTratamiento", trat.IdTratamiento);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }   
        }
        public void Delete(int id)
        {
            string query = "UPDATE Tratamiento SET estado=0 WHERE idTratamiento=@idTratamiento";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idTratamiento", id);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override DataTable Select()
        {
            DataTable res= new DataTable();
                string query="SELECT * FROM vwSelectTratamientos  ORDER BY 2";
            try
            {
                SqlCommand cmd= Metodos.CreateBasicCommand(query);
                res=Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            return res;
        }

        public Tratamiento Get(int idTratamiento)
        {
            Tratamiento res = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = "SELECT idTratamiento, nombreTratamiento, descripcion, precio, estado, fUpdate FROM Tratamiento WHERE idTratamiento=@idTratamiento";
            try
            {
                cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idTratamiento", idTratamiento);
                dr=Metodos.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    res = new Tratamiento(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), decimal.Parse(dr[3].ToString()), byte.Parse(dr[4].ToString()), DateTime.Parse(dr[5].ToString()));
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }

            return res;
        }
        public bool Existe(string nombre)
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = "SELECT idTratamiento, nombreTratamiento, descripcion, precio, estado, fUpdate FROM Tratamiento WHERE nombreTratamiento=@nombreTratamiento";
            try
            {
                cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombreTratamiento", nombre);
                dr = Metodos.ExecuteDataReaderCommand(cmd);
                return dr.Read();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
        }
    }
}
