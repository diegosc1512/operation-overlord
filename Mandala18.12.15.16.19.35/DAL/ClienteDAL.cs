﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class ClienteDAL : AbstractDAL
    {
        #region atributos y Constructores
        private Persona pers;
        private Cliente clie;

        public Cliente Clie
        {
            get { return clie; }
            set { clie = value; }
        }

        public Persona Pers
        {
            get { return pers; }
            set { pers = value; }
        }
        public ClienteDAL()
        {

        }
        public ClienteDAL( Persona pers, Cliente clie)
        {
            this.pers = pers;
            this.clie = clie;
        }
      
        #endregion
        public override void Insert()
        {
            //string query1 = "INSERT INTO Persona(nombres,primerApellido,segundoApellido,sexo, fechaNacimiento,documento, idUsuario) VALUES (@nombres, @primerApellido, @segundoApellido, @sexo,@fechaNacimiento, @documento, @idUsuario) ";
            //string query2 = "INSERT INTO Cliente(idCliente,nit,razonSocial) VALUES (@idCliente, @nit, @razonSocial)";
            //string query3 = "INSERT INTO Telefono(numero,tipo,idPersona,fUpdate) VALUES(@numero,@tipo,@idPersona,CURRENT_TIMESTAMP)";
            //string query4 = "SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY];";
            //List<SqlCommand> cmds = new List<SqlCommand>();
            //try
            //{
            //    //List<Telefono> tel = new List<Telefono>();
                
            //    //SqlCommand cmd1 = Metodos.CreateBasicCommand(query1);
            //    cmds = Metodos.CreateNBasicCommand(2);
            //    SqlCommand cmd1 = cmds[0];
            //    SqlCommand cmd2 = cmds[1];
            //    SqlCommand cmd4 = cmds[2];

            //    cmd1.CommandText = query1;
            //    cmd2.CommandText = query2;
            //    cmd4.CommandText = query4;

            //    cmd1.Parameters.AddWithValue("@nombres",pers.Nombres );
            //    cmd1.Parameters.AddWithValue("@primerApellido", pers.PrimerApellido);
            //    cmd1.Parameters.AddWithValue("@segundoApellido", pers.SegundoApellido);
            //    cmd1.Parameters.AddWithValue("@sexo", pers.Sexo);
            //    cmd1.Parameters.AddWithValue("@fechaNacimiento", pers.FechaNacimiento );
            //    cmd1.Parameters.AddWithValue("@documento", pers.Documento);
            //    cmd1.Parameters.AddWithValue("@idUsuario", 1);

            //    //int id = Metodos.GetCurrentValueIDTable("Persona");

            //    cmd2.Parameters.AddWithValue("@idCliente",id);
            //    cmd2.Parameters.AddWithValue("@nit",clie.Nit);
            //    cmd2.Parameters.AddWithValue("@razonSocial",clie.RazonSocial);

            //    //Metodos.ExecuteBasicCommand(cmd1);
            //    //Metodos.ExecuteBasicCommand(cmd2);

            //    Metodos.Execute2BasicCommand(cmd1, cmd2);
            //    foreach (Telefono item in pers.Telefonos)
            //    {
            //        SqlCommand cmd3 = Metodos.CreateBasicCommand(query3);
            //        cmd3.Parameters.AddWithValue("@numero",item.Numero);
            //        cmd3.Parameters.AddWithValue("@tipo",item.Tipo);
            //        cmd3.Parameters.AddWithValue("@idPersona",id);
            //        Metodos.ExecuteBasicCommand(cmd3);
            //    }

            //}
            //catch (Exception ex)
            //{

            //    throw ex;
            //}
            List<string> querys = new List<string>();
            querys.Add("INSERT INTO Persona(nombres,primerApellido,segundoApellido,sexo, fechaNacimiento,documento, idUsuario) VALUES (@nombres, @primerApellido, @segundoApellido, @sexo,@fechaNacimiento, @documento, @idUsuario) ");
            //querys.Add("SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY];");
            querys.Add("INSERT INTO Cliente(idCliente,nit,razonSocial) VALUES (@idCliente, @nit, @razonSocial)");
            int id = Metodos.GetCurrentValueIDTable("Persona");
            for (int i = 0; i < pers.Telefonos.Count; i++)
            {
                querys.Add("INSERT INTO Telefono(numero,tipo,idPersona,fUpdate) VALUES(@numero,@tipo,@idPersona,CURRENT_TIMESTAMP)");
            }
            try
            {
                List<SqlCommand> cmds = Metodos.CreateNBasicCommand2(querys);

                cmds[0].Parameters.AddWithValue("@nombres", pers.Nombres);
                cmds[0].Parameters.AddWithValue("@primerApellido", pers.PrimerApellido);
                cmds[0].Parameters.AddWithValue("@segundoApellido", pers.SegundoApellido);
                cmds[0].Parameters.AddWithValue("@sexo", pers.Sexo);
                cmds[0].Parameters.AddWithValue("@fechaNacimiento", pers.FechaNacimiento);
                cmds[0].Parameters.AddWithValue("@documento", pers.Documento);
                cmds[0].Parameters.AddWithValue("@idUsuario", 1);



                cmds[1].Parameters.AddWithValue("@idCliente", id);
                cmds[1].Parameters.AddWithValue("@nit", clie.Nit);
                cmds[1].Parameters.AddWithValue("@razonSocial", clie.RazonSocial);


             
                for (int i = 2; i < cmds.Count; i++) 
                {
                    cmds[i].Parameters.AddWithValue("@numero", pers.Telefonos[i-2].Numero);
                    cmds[i].Parameters.AddWithValue("@tipo",pers.Telefonos[i-2].Tipo);
                    cmds[i].Parameters.AddWithValue("@idPersona", id);
                }

                Metodos.ExecuteNBasicCommand(cmds);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public override void Update()
        {
            string query1 = "UPDATE Persona SET nombres=@nombres,primerApellido=@primerApellido,segundoApellido=@segundoApellido,sexo=@sexo, fechaNacimiento=@fechaNacimiento,documento=@documento WHERE idPersona=@idPersona ";
            string query2 = "UPDATE Cliente SET nit=@nit,razonSocial=@razonSocial WHERE idCliente=@idCliente ";
            //string query3 = "INSERT INTO Telefono(numero,tipo,idPersona,fUpdate) VALUES(@numero,@tipo,@idPersona,CURRENT_TIMESTAMP)";
            List<SqlCommand> cmds = new List<SqlCommand>();
            try
            {
                //List<Telefono> tel = new List<Telefono>();

                //SqlCommand cmd1 = Metodos.CreateBasicCommand(query1);
                cmds = Metodos.CreateNBasicCommand(2);
                SqlCommand cmd1 = cmds[0];
                SqlCommand cmd2 = cmds[1];

                cmd1.CommandText = query1;
                cmd2.CommandText = query2;

                cmd1.Parameters.AddWithValue("@nombres", pers.Nombres);
                cmd1.Parameters.AddWithValue("@primerApellido", pers.PrimerApellido);
                cmd1.Parameters.AddWithValue("@segundoApellido", pers.SegundoApellido);
                cmd1.Parameters.AddWithValue("@sexo", pers.Sexo);
                cmd1.Parameters.AddWithValue("@fechaNacimiento", pers.FechaNacimiento);
                cmd1.Parameters.AddWithValue("@documento", pers.Documento);
                cmd1.Parameters.AddWithValue("@idPersona", pers.IdPersona);


                cmd2.Parameters.AddWithValue("@idCliente", clie.IdCliente);
                cmd2.Parameters.AddWithValue("@nit", clie.Nit);
                cmd2.Parameters.AddWithValue("@razonSocial", clie.RazonSocial);

                //Metodos.ExecuteBasicCommand(cmd1);
                //Metodos.ExecuteBasicCommand(cmd2);

                Metodos.Execute2BasicCommand(cmd1, cmd2);
                //foreach (Telefono item in pers.Telefonos)
                //{
                //    SqlCommand cmd3 = Metodos.CreateBasicCommand(query3);
                //    cmd3.Parameters.AddWithValue("@numero", item.Numero);
                //    cmd3.Parameters.AddWithValue("@tipo", item.Tipo);
                //    cmd3.Parameters.AddWithValue("@idPersona", id);
                //    Metodos.ExecuteBasicCommand(cmd3);
                //}

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public override void Delete()
        {

        }
        public  void Delete(int id)
        {
            string query = "UPDATE Persona SET estado=0 WHERE idPersona=@idPersona";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idPersona", id);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }  
        }


        public override DataTable Select()
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectClientes ORDER BY 2";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                res = Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
        public Persona Get(int idPersona)
        {
            Persona res = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = "SELECT  P.idpersona, p.nombres,P.primerApellido,P.segundoApellido,P.sexo,P.fechaNacimiento,P.documento, P.fUpdate, P.estado FROM Persona P WHERE idPersona=@idPersona";
            try
            {
                cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idPersona", idPersona);
                dr = Metodos.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    res = new Persona(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), char.Parse(dr[4].ToString()),DateTime.Parse(dr[5].ToString()),dr[6].ToString(),DateTime.Parse(dr[7].ToString()),byte.Parse(dr[8].ToString()));
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }

            return res;
        }
        public DataTable GetTelefono(int idPersona)
        {
            DataTable res = new DataTable();
            string query = "SELECT idTelefono, numero, tipo, idPersona FROM Telefono WHERE idPersona=@idPersona";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idPersona", idPersona);
                res = Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
        public Telefono GetUnTelefono(int idTelefono)
        {
            Telefono res = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = "SELECT idTelefono, numero, tipo, idPersona FROM Telefono WHERE idTelefono=@idTelefono";
            try
            {
                cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idTelefono", idTelefono);
                dr = Metodos.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    res = new Telefono(int.Parse(dr[0].ToString()), dr[1].ToString(),Convert.ToChar(dr[2].ToString()), int.Parse(dr[3].ToString()));
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }

            return res;
        }
        public List<Telefono> GetTelefonoNoFunciona(int idPersona)
        {
            List<Telefono> res = null ;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            string query = "SELECT idTelefono, numero, tipo, idPersona FROM Telefono WHERE idPersona=@idPersona";

            try
            {
                cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idPersona", idPersona);
                dr = Metodos.ExecuteDataReaderCommand(cmd);


                while (dr.Read())
                {
                   //res =new Telefono(int.Parse(dr[0].ToString()), dr[1].ToString(),Convert.ToChar(dr[2].ToString()), int.Parse(dr[3].ToString()));

                   // res.Add(dr[0].ToString()+ dr[1].ToString()+ dr[2].ToString()+ dr[3].ToString() );
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }

            return res;
        }
        public Cliente Get2(int idCliente)
        {
            Cliente res = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = "SELECT idCliente, nit, razonSocial  FROM Cliente WHERE idCliente=@idCliente ";
            try
            {
                cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idCliente", idCliente);
                dr = Metodos.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    res = new Cliente(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString());
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }

            return res;
        }
        public bool Existe(string nombre)
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = "SELECT idPersona, nombres FROM Persona WHERE nombres=@nombres";
            try
            {
                cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombres", nombre);
                dr = Metodos.ExecuteDataReaderCommand(cmd);
                return dr.Read();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }

        }
    }
}
