﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class AgendaAtencionDAL : AbstractDAL
    {
        #region Atributos, propiedades y constructores
        private AgendaAtencion agat;

        public AgendaAtencion Agat
        {
            get { return agat; }
            set { agat = value; }
        }
        public AgendaAtencionDAL(AgendaAtencion agat)
        {
            this.agat = agat;
        }
        public AgendaAtencionDAL()
        {

        }
        #endregion

        public override void Insert()
        {
            string query = "INSERT INTO AgendaAtencion(dia, horaInicio, horaFin) VALUES (@dia, @horaInicio, @horaFin)";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@dia", agat.Dia);
                cmd.Parameters.AddWithValue("@horaInicio", agat.HoraInicio);
                cmd.Parameters.AddWithValue("@horaFin", agat.HoraFin);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override void Update()
        {
            throw new NotImplementedException();
        }

        public override void Delete()
        {
            throw new NotImplementedException();
        }

        public override System.Data.DataTable Select()
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectAgenda ORDER BY 2";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                res = Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
        public AgendaAtencion Get (int idAgenda)
        {
            AgendaAtencion res = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = "SELECT idAgenda, dia, horaInicio, horaFin,  fUpdate, estado, idReserva FROM AgendaAtencion WHERE idAgenda=@idAgenda";
            try
            {
                cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idAgenda", idAgenda);
                dr = Metodos.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    res = new AgendaAtencion(int.Parse(dr[0].ToString()), DateTime.Parse(dr[1].ToString()), DateTime.Parse(dr[2].ToString()), DateTime.Parse(dr[3].ToString()), DateTime.Parse(dr[4].ToString()), char.Parse(dr[5].ToString()), int.Parse(dr[6].ToString()));
                   // res = new AgendaAtencion(int.Parse(dr[0].ToString()), Convert.ToDateTime(dr[1].ToString()), Convert.ToDateTime(dr[2].ToString()), Convert.ToDateTime(dr[3].ToString()), Convert.ToChar(dr[4].ToString()), Convert.ToDateTime(dr[5].ToString()), int.Parse(dr[6].ToString()));
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }

            return res;
        }
    }
}
