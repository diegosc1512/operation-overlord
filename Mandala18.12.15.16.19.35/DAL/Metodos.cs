﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public sealed class Metodos
    {
        ///<sumary>
        /// cadena obtenida por el app.config
        ///</sumary>


        #region Creacion de sqlCommand
        private static string conexionString = ConfigurationManager.ConnectionStrings["BDMandalaConnectionString"].ConnectionString;

        public static int  GetCurrentValueIDTable(string tabla)
        {
            int res = -1;
            string query = "SELECT IDENT_CURRENT('"+tabla+"')+ IDENT_INCR('"+tabla+"')";
            try
            {
                SqlCommand cmd = CreateBasicCommand(query);
                res=int.Parse(ExecuteScalarCommand(cmd));  
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            return res;
        }
        /// <summary>
        /// crear un  sqlcommand y lo relacione con una conexion
        /// </summary>
        /// <param name="query">Una consulta sql</param>
        /// <returns>sqlcommand</returns>
        public static SqlCommand CreateBasicCommand()
        {
            SqlConnection connection = new SqlConnection(conexionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            return cmd;
        }
        public static SqlCommand CreateBasicCommand(string query)
        {
            SqlConnection connection = new SqlConnection(conexionString);
            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = connection;
            return cmd;
        }
        /// <summary>
        /// Crear n comandos y retornar lista con ese comando
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public static List<SqlCommand> CreateNBasicCommand( int n)
        {
            List<SqlCommand> res = new List<SqlCommand>();
            SqlConnection connection = new SqlConnection(conexionString);
            for (int i = 0; i < n; i++)
            {
                SqlCommand cmd= new SqlCommand();
                cmd.Connection = connection;
                //cmd.CommandType = CommandType.Text;
                res.Add(cmd);
            }

            return res;
        }
        /// <summary>
        /// se crea la instanciacion de N comandos
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static List<SqlCommand> CreateNBasicCommand2(List<string> list)
        {
            List<SqlCommand> res = new List<SqlCommand>();

            SqlConnection connection = new SqlConnection(conexionString);
            for (int i = 0; i < list.Count; i++)
            {
                SqlCommand cmd = new SqlCommand(list[i]);
                cmd.Connection = connection;
                cmd.CommandType = CommandType.Text;
                res.Add(cmd);
            }


            return res;

        }
        #endregion
        #region ejecucion de sqlCommand
        /// <summary>
        /// se ejecutan 2 comandos recibiendo como parametros dos comandos
        /// </summary>
        /// <param name="cmd1"></param>
        /// <param name="cmd2"></param>
        public static void Execute2BasicCommand(SqlCommand cmd1, SqlCommand cmd2)
        {
            SqlTransaction tran = null;
            try
            {
                cmd1.Connection.Open();
                tran = cmd1.Connection.BeginTransaction();
                cmd1.Transaction = tran;
                cmd1.ExecuteNonQuery();
                cmd2.Transaction = tran;
                cmd2.ExecuteNonQuery();
                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();
                throw ex;
            }
            finally
            {
                cmd1.Connection.Close();
            }
        }
        /// <summary>
        /// Aqui Realizamos la ejecucion de N comandos
        /// </summary>
        /// <param name="cmds"></param>
        public static void ExecuteNBasicCommand(List<SqlCommand> cmds)
        {
            SqlTransaction tran = null;

            try
            {
                cmds[0].Connection.Open();
                tran = cmds[0].Connection.BeginTransaction();
                foreach (SqlCommand cmd in cmds)
                {
                    cmd.Transaction = tran;
                    cmd.ExecuteNonQuery();
                }


                //Si llegamoks aqui todo OK
                tran.Commit();

            }
            catch (Exception ex)
            {
                tran.Rollback();
                throw ex;
            }
            finally
            {
                cmds[0].Connection.Close();
            }

        }
        /// <summary>
        /// ejecutamos un comando simple
        /// </summary>
        /// <param name="cmd"></param>
        public static void ExecuteBasicCommand(SqlCommand cmd)
        {
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        public static void ExecuteBasicCommand(SqlCommand cmd, string query)
        {
            try
            {
                cmd.CommandText = query;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        public static DataTable ExecuteDataTableCommand(SqlCommand cmd)
        {
            DataTable res = new DataTable();
            try
            {
                cmd.Connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(res);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }
        public static SqlDataReader ExecuteDataReaderCommand(SqlCommand cmd)
        {
            SqlDataReader res = null;
            try
            {
                cmd.Connection.Open();
                res=cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                
                throw ex ;
            }
            return res;
        }

        public static string ExecuteScalarCommand(SqlCommand cmd)
        {
            try
            {
                cmd.Connection.Open();
                return cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        #endregion
    }
}
