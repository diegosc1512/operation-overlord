﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;
using System.Data.SqlClient;

namespace DAL
{
    public class ConsultaDAL : AbstractDAL
    {
        #region Atributos, propiedades y constructores
        private Consulta cons;
        private List<TratamientoConsulta> lisTrat;

        public List<TratamientoConsulta> LisTrat
        {
            get { return lisTrat; }
            set { lisTrat = value; }
        }

        public Consulta Cons
        {
          get { return cons; }
          set { cons = value; }
        }

       
        public ConsultaDAL ()
	        {

	        }
        public ConsultaDAL(Consulta cons, List<TratamientoConsulta> lisTrat)
        {
            this.cons = cons;
            this.lisTrat = lisTrat;
        }
        #endregion
        public override void Insert()
        {
            List<string> querys = new List<string>();
            querys.Add("INSERT INTO Consulta(idCliente,fechaConsulta,idEmpleado,horaInicio, horaFin, diagnostico, total) VALUES (@idCliente,@fechaConsulta,@idEmpleado,@horaInicio, @horaFin, @diagnostico, @total)");
            int id = Metodos.GetCurrentValueIDTable("Consulta");
            foreach (TratamientoConsulta item in lisTrat)
	        {
                querys.Add(" INSERT INTO TratamientoConsulta( idTratamiento, idConsulta, cantidadDeSesiones, precioTratamientoConsulta) VALUES ( @idTratamiento, @idConsulta, @cantidadDeSesiones, @precio)");
		 
	        }
            //for (int i = 0; i < pers.Telefonos.Count; i++)
            //{
            //    querys.Add("INSERT INTO Telefono(numero,tipo,idPersona,fUpdate) VALUES(@numero,@tipo,@idPersona,CURRENT_TIMESTAMP)");
            //}
            try
            {
                List<SqlCommand> cmds = Metodos.CreateNBasicCommand2(querys);

                cmds[0].Parameters.AddWithValue("@idCliente", cons.IdCliente);
                cmds[0].Parameters.AddWithValue("@fechaConsulta", cons.FechaConsulta);
                cmds[0].Parameters.AddWithValue("@idEmpleado",cons.IdEmpleado );
                cmds[0].Parameters.AddWithValue("@horaInicio", cons.HoraInicio);
                cmds[0].Parameters.AddWithValue("@horaFin", cons.HoraFin);
                cmds[0].Parameters.AddWithValue("@diagnostico", cons.Diagnostico);
                cmds[0].Parameters.AddWithValue("@total", cons.Total);

                for (int i = 2; i < cmds.Count; i++) 
                {
                    cmds[i].Parameters.AddWithValue("@idTratamiento",cons.TratamientoConsulta[i-2].IdTratamiento);
                    cmds[i].Parameters.AddWithValue("@cantidadDeSesiones",cons.TratamientoConsulta[i-2].CantidadDeSesiones);
                    cmds[i].Parameters.AddWithValue("@precio",cons.TratamientoConsulta[i-2].Precio);
                    cmds[i].Parameters.AddWithValue("@idConsulta", id);
                }

                Metodos.ExecuteNBasicCommand(cmds);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public  void Insert(Consulta cons, List<TratamientoConsulta> lisTrat)
        {
            List<string> querys = new List<string>();
            querys.Add("INSERT INTO Consulta(idCliente,fechaConsulta,idEmpleado,horaInicio, horaFin, diagnostico, total) VALUES (@idCliente,@fechaConsulta,@idEmpleado,@horaInicio, @horaFin, @diagnostico, @total)");
            int id = Metodos.GetCurrentValueIDTable("Consulta");
            foreach (TratamientoConsulta item in lisTrat)
            {
                querys.Add(" INSERT INTO TratamientoConsulta( idTratamiento, idConsulta, cantidadDeSesiones, precioTratamientoConsulta) VALUES ( @idTratamiento, @idConsulta, @cantidadDeSesiones, @precio)");

            }
            //for (int i = 0; i < pers.Telefonos.Count; i++)
            //{
            //    querys.Add("INSERT INTO Telefono(numero,tipo,idPersona,fUpdate) VALUES(@numero,@tipo,@idPersona,CURRENT_TIMESTAMP)");
            //}
            try
            {
                List<SqlCommand> cmds = Metodos.CreateNBasicCommand2(querys);

                cmds[0].Parameters.AddWithValue("@idCliente", cons.IdCliente);
                cmds[0].Parameters.AddWithValue("@fechaConsulta", cons.FechaConsulta);
                cmds[0].Parameters.AddWithValue("@idEmpleado", cons.IdEmpleado);
                cmds[0].Parameters.AddWithValue("@horaInicio", cons.HoraInicio);
                cmds[0].Parameters.AddWithValue("@horaFin", cons.HoraFin);
                cmds[0].Parameters.AddWithValue("@diagnostico", cons.Diagnostico);
                cmds[0].Parameters.AddWithValue("@total", cons.Total);

                //for (int i = 1; i < cmds.Count; i++)
                //{
                //    cmds[i].Parameters.AddWithValue("@idTratamiento", cons.TratamientoConsulta[i ].IdTratamiento);
                //    cmds[i].Parameters.AddWithValue("@cantidadDeSesiones", cons.TratamientoConsulta[i ].CantidadDeSesiones);
                //    cmds[i].Parameters.AddWithValue("@precio", cons.TratamientoConsulta[i ].Precio);
                //    cmds[i].Parameters.AddWithValue("@idConsulta", id);
                //}
                int i=1;
                foreach (TratamientoConsulta item in lisTrat)
                {
                    cmds[i].Parameters.AddWithValue("@idTratamiento", item.IdTratamiento);
                    cmds[i].Parameters.AddWithValue("@cantidadDeSesiones", item.CantidadDeSesiones);
                    cmds[i].Parameters.AddWithValue("@precio", item.Precio);
                    cmds[i].Parameters.AddWithValue("@idConsulta", id);
                    i++;
                }

                Metodos.ExecuteNBasicCommand(cmds);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public override void Update()
        {
            throw new NotImplementedException();
        }

        public override void Delete()
        {
            throw new NotImplementedException();
        }

        public override DataTable Select()
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectHorariosEmpleados ";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                res = Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
        public DataTable SelectCliente()
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectClientes ";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                res = Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
        public DataTable SelectLikeCiFullNameCliente(byte opcionLike, string texto)
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwClientesIdDocumentoFullName  ";

            switch (opcionLike)
            {
                case 0:
                    query = query + "WHERE Documento LIKE @texto ";
                    break;
                case 1:
                    query = query + "WHERE [Nombre completo] LIKE @texto ";
                    break;
            }
            query = query + " ORDER BY 2";


            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@texto", "%" + texto + "%");

                res = Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }

        public DataTable SelectLikeCiFullNameEmpleado(byte opcionLike, string texto)
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwEmpleadosIdDocumentoFullName  ";

            switch (opcionLike)
            {
                case 0:
                    query = query + "WHERE Documento LIKE @texto ";
                    break;
                case 1:
                    query = query + "WHERE [Nombre completo] LIKE @texto ";
                    break;
            }
            query = query + " ORDER BY 2";


            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@texto", "%" + texto + "%");

                res = Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }

        public DataTable SelectHorariosEmpleado(int id)
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectHorarioEmpleado WHERE idEmpleado="+id;
            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                res = Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }

        public DataTable LoadDatagridTratamiento(string txttra)
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwTratamientoFull  ";


            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@texto", "%" + txttra + "%");

                res = Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }

        public DataTable SelectConsulta()
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectConsulta ";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                res = Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
    }
}
