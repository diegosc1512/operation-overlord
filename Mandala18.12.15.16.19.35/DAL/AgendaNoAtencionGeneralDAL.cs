﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class AgendaNoAtencionGeneralDAL : AbstractDAL
    {
                #region Atributos, propiedades y constructores
        private AgendaNoAtencionGeneral agnat;

        public AgendaNoAtencionGeneral Agnat
        {
          get { return agnat; }
          set { agnat = value; }
        }

        public AgendaNoAtencionGeneralDAL(AgendaNoAtencionGeneral agnat)
        {
            this.agnat = agnat;
        }
        public AgendaNoAtencionGeneralDAL()
        {

        }
        #endregion
        public override void Insert()
        {
            string query = "INSERT INTO AgendaNoAtencionGeneral(horaInicio, horaFin, fecha,motivo ) VALUES ( @horaInicio, @horaFin, @fecha,@motivo)";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@horaInicio", agnat.HoraInicio);
                cmd.Parameters.AddWithValue("@horaFin", agnat.HoraFin);
                cmd.Parameters.AddWithValue("@fecha", agnat.Fecha);
                cmd.Parameters.AddWithValue("@motivo", agnat.Motivo);

                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override void Update()
        {
            string query = "UPDATE AgendaNoAtencionGeneral SET horaInicio=@horaInicio, horaFin=@horaFin, fecha=@fecha, motivo=@motivo,fUpdate=CURRENT_TIMESTAMP WHERE idAgenda=@idAgenda";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@horaInicio", agnat.HoraInicio);
                cmd.Parameters.AddWithValue("@horaFin", agnat.HoraFin);
                cmd.Parameters.AddWithValue("@fecha", agnat.HoraFin);
                cmd.Parameters.AddWithValue("@motivo", agnat.HoraFin);
                cmd.Parameters.AddWithValue("@idAgenda", agnat.IdAgenda);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override void Delete()
        {
            throw new NotImplementedException();
        }
        public void Delete(int id)
        {
            string query = "UPDATE AgendaNoAtencionGeneral SET estado=0 WHERE idAgenda=@idAgenda";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idAgenda", id);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override DataTable Select()
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectAgendaNoAtencionGeneral ";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                res = Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
        public AgendaNoAtencionGeneral Get(int idAgenda)
        {
            AgendaNoAtencionGeneral res = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = "SELECT idAgenda,horaInicio, horaFin, fecha, motivo, fUpdate, estado FROM AgendaNoAtencionGeneral WHERE idAgenda=@idAgenda";
            try
            {
                cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idAgenda", idAgenda);
                dr = Metodos.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    res = new AgendaNoAtencionGeneral(int.Parse(dr[0].ToString()), DateTime.Parse(dr[1].ToString()), DateTime.Parse(dr[2].ToString()), DateTime.Parse(dr[3].ToString()), dr[4].ToString(), DateTime.Parse(dr[5].ToString()), Byte.Parse(dr[6].ToString()));
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }

            return res;
        }

        public DataTable SelectLikeCiFullName(byte opcionLike, string texto)
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwEmpleadoIdDocumentoFullName  ";

            switch (opcionLike)
            {
                case 0:
                    query = query + "WHERE Documento LIKE @texto ";
                    break;
                case 1:
                    query = query + "WHERE [Nombre completo] LIKE @texto ";
                    break;
            }
            query = query + " ORDER BY 2";


            try
            {
                SqlCommand cmd = Metodos.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@texto", "%" + texto + "%");

                res = Metodos.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
    }
}
