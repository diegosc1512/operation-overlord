﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using DAL;
using System.Data;

namespace BRL
{
    public class HorarioEmpleadoBRL : AbstractBRL
    {
                #region atributos, propiedades y constructores
        private HorarioEmpleado horemp;
        private HorarioEmpleadoDAL dal;

        public HorarioEmpleadoDAL Dal
        {
          get { return dal; }
          set { dal = value; }
        }

        public HorarioEmpleado Horemp
        {
            get { return horemp; }
            set { horemp = value; }
        }
        public HorarioEmpleadoBRL()
        {

        }
        public HorarioEmpleadoBRL(HorarioEmpleado horemp)
        {
            this.horemp = horemp;
            dal = new HorarioEmpleadoDAL(horemp);
        }
        #endregion
        public override void Insert()
        {
            dal.Insert();
        }
        public void Insert(int idEmpleado)
        {
            //dal = new HorarioEmpleadoDAL();
            dal.Insert(idEmpleado);
        }
        public override void Update()
        {
            dal.Update();
        }

        public override void Delete()
        {
            throw new NotImplementedException();
        }
        public void Delete(int id)
        {
            dal.Delete(id);
        }

        public override DataTable Select()
        {
            dal = new HorarioEmpleadoDAL();
            return dal.Select();
        }
        public HorarioEmpleado Get(int idHorario)
        {
            dal = new HorarioEmpleadoDAL();
            return dal.Get(idHorario);
        }
        public DataTable SelectLikeCiFullName(byte opcionLike, string texto)
        {
            dal = new HorarioEmpleadoDAL();
            return dal.SelectLikeCiFullName(opcionLike, texto);
        }
    }
}
