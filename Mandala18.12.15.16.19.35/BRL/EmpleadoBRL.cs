﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using Common;
using System.Data;

namespace BRL
{
    public class EmpleadoBRL : AbstractBRL
    {
        #region atributos y Constructores
        private Persona pers;
        private Empleado emp;
        private EmpleadoDAL dal;

        public EmpleadoDAL Dal
        {
          get { return dal; }
          set { dal = value; }
        }

        public Empleado Emp
        {
            get { return emp; }
            set { emp = value; }
        }

        public Persona Pers
        {
            get { return pers; }
            set { pers = value; }
        }
        public EmpleadoBRL()
        {

        }
        public EmpleadoBRL( Persona pers, Empleado emp)
        {
            this.pers = pers;
            this.emp = emp;
            dal = new EmpleadoDAL(pers, emp);
        }
      
        #endregion
        public override void Insert()
        {
            dal.Insert();
        }

        public override void Update()
        {
            dal.Update();

        }

         public override void Delete()
             {

             }
        public void Delete(int id)
        {
            dal.Delete(id);
        }

        public override DataTable Select()
        {
            dal = new EmpleadoDAL();
            return dal.Select();
        }

        public Persona Get(int idPersona)
        {
            dal = new EmpleadoDAL();
            return dal.Get(idPersona);
        }
        public Empleado Get2(int idEmpleado)
        {
            dal = new EmpleadoDAL();
            return dal.Get2(idEmpleado);
        }
        public DataTable GetTelefono(int idEmpleado)
        {
            dal = new EmpleadoDAL();
            return dal.GetTelefono(idEmpleado);
        }
        public Telefono GetUnTelefono(int idTelefono)
        {
            dal = new EmpleadoDAL();
            return dal.GetUnTelefono(idTelefono);
        }
        public bool Existe(string nombre)
        {
            return dal.Existe(nombre);
        }
    }
}
