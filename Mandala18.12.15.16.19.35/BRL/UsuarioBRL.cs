﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using Common;
using System.Data;
namespace BRL
{
    public class UsuarioBRL : AbstractBRL
    {
        #region atributos, propiedades y constructores
        UsuarioDAL dal;
        Usuario user;

        public Usuario User
        {
            get { return user; }
            set { user = value; }
        }

        public UsuarioDAL Dal
        {
            get { return dal; }
            set { dal = value; }
        }

        public UsuarioBRL()
        {

        }
        public UsuarioBRL(Usuario user)
        {
            this.user = user;
            dal = new UsuarioDAL(user);
        }
        #endregion
        #region metodos
       
        public override void Insert()
        {
            dal.Insert();
        }

        public override void Update()
        {
            dal.Update();
        }
        public  void Update2()
        {
            dal.Update2();
        }
        public override void Delete()
        {
            dal.Delete();
        }

        public override DataTable Select()
        {
            dal = new UsuarioDAL();
            return dal.Select();
        }
        public  DataTable Select2()
        {
            dal = new UsuarioDAL();
            return dal.Select2();
        }
        public Usuario Get(int idUsuario)
        {
            dal = new UsuarioDAL();
            return dal.Get(idUsuario);
        }
        public DataTable Login(string usuario, string password)
        {
            dal = new UsuarioDAL();
            return dal.Login(usuario, password);
        }
        public bool Existe(string nombre)
        {
            return dal.Existe(nombre);
        }
        #endregion
    }
}
