﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using DAL;
using System.Data;

namespace BRL
{
    public class TratamientoBRL : AbstractBRL
    {
        #region atributos, propiedades y constructores
        private Tratamiento trat;
        private TratamientoDAL dal;

        public TratamientoDAL Dal
        {
            get { return dal; }
            set { dal = value; }
        }
        public TratamientoBRL()
        {

        }
        public TratamientoBRL(Tratamiento trat)
        {
            this.trat = trat;
            dal = new TratamientoDAL(trat);
        }

        public Tratamiento Trat
        {
            get { return trat; }
            set { trat = value; }
        }



        #endregion
        #region metodos de la clase
        public override void Insert()
        {
            dal.Insert();
        }

        public override void Update()
        {
            dal.Update();
        }

        public override void Delete()
        {
            dal.Delete();
        }
        public void Delete(int id)
        {
            dal.Delete(id);
        }

        public override DataTable Select()
        {
            dal = new TratamientoDAL();
           return dal.Select();
        }
        public Tratamiento  Get(int idTatamiento)
        {
            dal = new TratamientoDAL();
            return dal.Get(idTatamiento);
        }
        public bool Existe(string nombre)
        {
            return dal.Existe(nombre);
        }
        #endregion
    }
}
