﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using DAL;
using System.Data;

namespace BRL
{
    public class SucursalBRL : AbstractBRL
    {
        #region atributos, propiedades y constructores
        private Sucursal suc;
        private SucursalDAL dal;

        public SucursalDAL Dal
        {
            get { return dal; }
            set { dal = value; }
        }
        public SucursalBRL()
        {

        }
        public SucursalBRL(Sucursal suc)
        {
            this.suc = suc;
            dal = new SucursalDAL(suc);
        }

        public Sucursal Suc
        {
            get { return suc; }
            set { suc = value; }
        }



        #endregion
        #region metodos de la clase
        public override void Insert()
        {
            dal.Insert();
        }

        public override void Update()
        {
            dal.Update();
        }

        public override void Delete()
        {
            dal.Delete();
        }
        public void Delete(int id)
        {
            dal.Delete(id);
        }

        public override DataTable Select()
        {
            dal = new SucursalDAL();
            return dal.Select();
        }
        public Sucursal Get(int idSucursal)
        {
            dal = new SucursalDAL();
            return dal.Get(idSucursal);
        }
        public bool Existe(string nombre)
        {
            return dal.Existe(nombre);
        }
        #endregion
    }
}
