﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using DAL;
using System.Data;

namespace BRL
{
    public class AgendaAtencionBRL : AbstractBRL
    {

        #region atributos, propiedades y constructores
        private AgendaAtencion agat;
        private AgendaAtencionDAL dal;

        public AgendaAtencionDAL Dal
        {
            get { return dal; }
            set { dal = value; }
        }

        public AgendaAtencion Agat
        {
            get { return agat; }
            set { agat = value; }
        }
        public AgendaAtencionBRL(AgendaAtencion agat)
        {
            this.agat = agat;
            dal = new AgendaAtencionDAL(agat);
        }
        public AgendaAtencionBRL()
        {

        }
        #endregion
        public override void Insert()
        {
            dal.Insert();
        }

        public override void Update()
        {
            dal.Update();
        }

        public override void Delete()
        {
            dal.Delete();
        }

        public override DataTable Select()
        {
            dal = new AgendaAtencionDAL();
            return dal.Select();
        }
        public AgendaAtencion Get(int idAgenda)
        {
            dal = new AgendaAtencionDAL();
            return dal.Get(idAgenda);
        }
      
    }
}
