﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using DAL;
using System.Data;

namespace BRL
{
    public class ClienteBRL : AbstractBRL
    {
                #region atributos y Constructores
        private Persona pers;
        private Cliente clie;
        private ClienteDAL dal;

        public ClienteDAL Dal
        {
          get { return dal; }
          set { dal = value; }
        }

        public Cliente Clie
        {
            get { return clie; }
            set { clie = value; }
        }

        public Persona Pers
        {
            get { return pers; }
            set { pers = value; }
        }
        public ClienteBRL()
        {

        }
        public ClienteBRL( Persona pers, Cliente clie)
        {
            this.pers = pers;
            this.clie = clie;
            dal = new ClienteDAL(pers, clie);
        }
      
        #endregion
        public override void Insert()
        {
            dal.Insert();
        }

        public override void Update()
        {
            dal.Update();

        }

        public  void Delete(int id)
        {
            dal.Delete(id);
        }
        public override void Delete()
        {
        }
        public override DataTable Select()
        {
            dal = new ClienteDAL();
            return dal.Select();
        }
        public Persona Get(int idPersona)
        {
            dal = new ClienteDAL();
            return dal.Get(idPersona);
        }
        public Cliente Get2(int idCliente)
        {
            dal = new ClienteDAL();
            return dal.Get2(idCliente);
        }
        public DataTable GetTelefono(int idCliente)
        {
            dal = new ClienteDAL();
            return dal.GetTelefono(idCliente);
        }
         public Telefono GetUnTelefono(int idTelefono)
        {
            dal = new ClienteDAL();
            return dal.GetUnTelefono(idTelefono);
        }
        public bool Existe(string nombre)
        {
            return dal.Existe(nombre);
        }
    }
}
