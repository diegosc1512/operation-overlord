﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Oracle.DataAccess.Client;
using Oracle.DataAccess;
using Common;

namespace DALOracle
{
    public class FacturaDAL
    {
          #region Atributos, propiedades y constructores
        private Factura fac;
        Metodos met= new Metodos();
        public Factura Fac
        {
            get { return fac; }
            set { fac = value; }
        }
        public FacturaDAL()
        {

        }
        public FacturaDAL(Factura fac)
        {
            this.fac = fac;
        }
        #endregion


        public  void Insert()
        {
            try
            {
                string query = @"INSERT INTO FACTURA (NUMEROFACTURA,FECHA,TOTAL,NIT,RAZONSOCIAL,CODIGOCONTROL,CONSULTA_IDCONSULTA,DOSIFICACION_IDDOSIFICACION)
                                VALUES (:NUMEROFACTURA,:FECHA,:TOTAL,:NIT,:RAZONSOCIAL,:CODIGOCONTROL,:CONSULTA_IDCONSULTA,:DOSIFICACION_IDDOSIFICACION);";
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("NUMEROFACTURA",fac.NumeroFactura),
                new OracleParameter("FECHA", fac.Fecha),
                new OracleParameter("TOTAL",fac.Total),
                new OracleParameter("NIT", fac.Nit),
                new OracleParameter("RAZONSOCIAL", fac.RazonSocial),
                new OracleParameter("CODIGOCONTROL", fac.CodigoControl),
                new OracleParameter("CONSULTA_IDCONSULTA", fac.IdConsulta),
                new OracleParameter("DOSIFICACION_IDDOSIFICACION", fac.IdDosificacion)
            };
                met.ExecuteBasicCommand(query, pararmetros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public  void Update()
        {

        }

        public  void Delete()
        {
            string query = "UPDATE Factura SET estado='A' WHERE idfactura=:idfactura";

            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idfactura",fac.IdFactura)

            };
                met.ExecuteBasicCommand(query, pararmetros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public void Delete(int id)
        {
            string query = "UPDATE Factura SET estado='A' WHERE idfactura=:idfactura";
            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idfactura",id)

            };
                met.ExecuteBasicCommand(query, pararmetros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public  DataTable Select()
        {
            DataTable res = new DataTable();
            string query = @"SELECT IDFACTURA ,NUMEROFACTURA ,FECHA ,TOTAL ,NIT ,RAZONSOCIAL ,CODIGOCONTROL ,ESTADO , CONSULTA_IDCONSULTA,
                            DOSIFICACION_IDDOSIFICACION ,NUMEROAUTORIZACION ,IMPORTETOTAL ,SUBTOTAL,DESCUENTO,CREDITOFISCAL FROM FACTURA ;";
            try
            {
                DataTable dt = new DataTable();
                dt.Load(met.ExecuteDataReaderCommand(query));
                res = dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
           
        }

        public Factura Get(int idFactura)
        {
            Factura res = new Factura();
            OracleDataReader dr = null;
            string query = @"SELECT IDFACTURA ,NUMEROFACTURA ,FECHA ,TOTAL ,NIT ,RAZONSOCIAL ,CODIGOCONTROL ,ESTADO , CONSULTA_IDCONSULTA,
                            DOSIFICACION_IDDOSIFICACION ,NUMEROAUTORIZACION ,IMPORTETOTAL ,SUBTOTAL,DESCUENTO,CREDITOFISCAL FROM FACTURA 
                            WHERE IDFACTURA=:idFactura;";
            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idFactura",idFactura)
            };
                dr = met.ExecuteDataReaderCommandParametros(query, pararmetros);
                while (dr.Read())
                {
                    res = new Factura(int.Parse(dr[0].ToString()), int.Parse(dr[1].ToString()), DateTime.Parse(dr[2].ToString()), double.Parse(dr[3].ToString()),
                        dr[4].ToString(), dr[5].ToString(), dr[6].ToString(), char.Parse(dr[7].ToString()), int.Parse(dr[8].ToString()), int.Parse(dr[9].ToString()),
                        double.Parse(dr[10].ToString()), double.Parse(dr[11].ToString()), double.Parse(dr[12].ToString()), double.Parse(dr[13].ToString()),
                        double.Parse(dr[14].ToString()));
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                dr.Close();
            }

            return res;
        }
        public bool Existe(string nombre)
        {
            //OracleDataReader dr = null;
            //string query = "SELECT idSucursal, nombreSucursal,latitud, longitud, direccion, estado, fUpdate FROM Sucursal WHERE nombreSucursal=:nombreSucursal";
            //try
            //{
            //    OracleParameter[] pararmetros = new OracleParameter[]
            //{
            //    new OracleParameter("nombreSucursal",nombre)
            //};
            //    dr = met.ExecuteDataReaderCommand(query);
            //    return dr.Read();
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            //finally
            //{
            //    dr.Close();
            //}
            return true; //borrar si se implementa el codigo correcto
        }
    }
}
