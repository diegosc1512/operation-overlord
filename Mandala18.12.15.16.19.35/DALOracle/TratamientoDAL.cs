﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Oracle.DataAccess.Client;
using Oracle.DataAccess;
using Common;


namespace DALOracle
{
    public class TratamientoDAL : AbstractDAL
    {
        #region Atributos, propiedades y constructores
        private Tratamiento trat;
        private Metodos met= new Metodos();

        public Tratamiento Trat
        {
            get { return trat; }
            set { trat = value; }
        }
        public TratamientoDAL()
        {

        }
        public TratamientoDAL(Tratamiento trat)
        {
            this.trat = trat;
        }
        #endregion




        public override void Insert()
        {
            try
            {
                string query = @"INSERT INTO tratamiento(nombreTratamiento,descripcion,precio,estado,fupdate)
                                    VALUES (:nombreTratamiento,:descripcion,:precio,1,CURRENT_TIMESTAMP)";
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("nombreTratamiento",trat.NombreTratamiento),
                new OracleParameter("descripcion", trat.Descripcion),
                new OracleParameter("precio",Convert.ToDouble(trat.Precio))
            };
                met.ExecuteBasicCommand(query, pararmetros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override void Update()
        {
            try
            {
                string query = @"Update tratamiento SET nombreTratamiento=:nombreTratamiento, descripcion=:descripcion, 
                                 precio=:precio, fUpdate=CURRENT_TIMESTAMP WHERE idTratamiento=:idTratamiento";
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("nombreTratamiento",trat.NombreTratamiento),
                new OracleParameter("descripcion", trat.Descripcion),
                new OracleParameter("precio",trat.Precio),
                new OracleParameter("idTratamiento",trat.IdTratamiento)

            };
                met.ExecuteBasicCommand(query, pararmetros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
       
        }

        public override void Delete()
        {

            try
            {
                string query = "UPDATE Tratamiento SET estado=0 WHERE idTratamiento=:idTratamiento";

                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idtratamiento",trat.IdTratamiento)
            };
                met.ExecuteBasicCommand(query, pararmetros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public  void Delete(int id)
        {
            try
            {
                string query = "UPDATE Tratamiento SET estado=0 WHERE idTratamiento=:idTratamiento";

                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idTratamiento",id)
            };
                met.ExecuteBasicCommand(query, pararmetros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override DataTable Select()
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM Tratamiento WHERE estado=1 ";
            try
            {
                DataTable dt = new DataTable();
                dt.Load(met.ExecuteDataReaderCommand(query));
                res = dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }

        public Tratamiento Get(int idTratamiento)
        {
            Tratamiento res = new Tratamiento();
            OracleDataReader dr=null;
            
            try
            {
            //string query = @"SELECT idTratamiento, nombreTratamiento, descripcion, precio, estado, fUpdate 
              //               FROM Tratamiento WHERE idTratamiento=:idTratamiento";
            string query = @"SELECT idTratamiento, nombreTratamiento, descripcion, precio, estado, fUpdate 
                             FROM Tratamiento WHERE idTratamiento=:idTratamiento";
            
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idTratamiento",idTratamiento)
            };
                dr = met.ExecuteDataReaderCommandParametros(query, pararmetros);
                while (dr.Read())
                {
                    res = new Tratamiento(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), decimal.Parse(dr[3].ToString()), byte.Parse(dr[4].ToString()), DateTime.Parse(dr[5].ToString()));
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                
                dr.Close();
            }

            return res;
        }
        public bool Existe(string nombre)
        {
            OracleDataReader dr = null;
            string query = @"SELECT idTratamiento, nombreTratamiento, descripcion, precio, estado, fUpdate 
                               FROM Tratamiento WHERE nombreTratamiento=:nombreTratamiento";
            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("nombreTratamiento",nombre)
            };
                dr = met.ExecuteDataReaderCommand(query);
                return dr.Read();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dr.Close();
            }
        }
    }
}
