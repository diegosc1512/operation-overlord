﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace DALOracle
{
    public abstract class AbstractDAL
    {
        public abstract void Insert();
        public abstract void Update();
        public abstract void Delete();
        public abstract DataTable Select();

    }
}
