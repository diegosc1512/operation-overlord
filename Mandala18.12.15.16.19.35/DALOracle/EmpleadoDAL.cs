﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Oracle.DataAccess.Client;
using System.Configuration;
using Oracle.DataAccess;
using System.Data;

namespace DALOracle
{
    public class EmpleadoDAL
    {
        #region atributos y Constructores
        private Persona pers;
        private Empleado emp;
        private Metodos met;
        private Usuario usu;

        public Usuario Usu
        {
            get { return usu; }
            set { usu = value; }
        }

        public Empleado Emp
        {
            get { return emp; }
            set { emp = value; }
        }

        public Persona Pers
        {
            get { return pers; }
            set { pers = value; }
        }
        public EmpleadoDAL()
        {

        }
        public EmpleadoDAL(Persona pers, Empleado emp, Usuario usu)
        {
            this.pers = pers;
            this.emp = emp;
            this.usu = usu;
        }

        #endregion
        public  void Insert()
        {
            int contador = 3;
            int contadorHorarios = 0;
            met = new Metodos();
            List<string> querys = new List<string>();
            querys.Add("INSERT INTO Persona(idPersona,nombres,primerApellido,segundoApellido,sexo, fechaNacimiento,documento, idUsuario) VALUES (:idPersona, :nombres, :primerApellido, :segundoApellido, :sexo, :fechaNacimiento, :documento, :idUsuario) ");
            querys.Add("INSERT INTO Empleado(PERSONA_IDPERSONA,grado,salario) VALUES (:idEmpleado, :grado ,:salario)");
            querys.Add("INSERT INTO Usuario (NOMBREUSUARIO,PASSWORD,ROL,EMAIL,PERSONA_IDPERSONA)VALUES(:NOMBREUSUARIO,:PASSWORD,:ROL,:EMAIL,:PERSONA_IDPERSONA)");
            int id = met.GetCurrentValueIDSecuencia("SEQ_PERSONA");
            for (int i = 0; i < pers.Telefonos.Count; i++)
            {
                querys.Add("INSERT INTO Telefono(NUMERO,TIPO,PERSONA_IDPERSONA,FUPDATE) VALUES(:numero,:tipo,:idPersona,CURRENT_TIMESTAMP)");
            }
            for (int i = 0; i < Emp.Horarios.Count; i++)
            {
                querys.Add("INSERT INTO EmpleadoHorario(HORARIOEMPLEADO_IDHORARIO,EMPLEADO_PERSONA_IDPERSONA) VALUES(:idHorario,:idEmpleado)");
                contadorHorarios++;
            }
            try
            {

                List<OracleCommand> cmds = met.CreateNBasicCommand2(querys);

                cmds[0].Parameters.Add("idPersona",id );
                cmds[0].Parameters.Add("nombres", pers.Nombres);
                cmds[0].Parameters.Add("primerApellido", pers.PrimerApellido);
                cmds[0].Parameters.Add("segundoApellido", pers.SegundoApellido);
                cmds[0].Parameters.Add("sexo", pers.Sexo);
                cmds[0].Parameters.Add("fechaNacimiento", pers.FechaNacimiento);
                cmds[0].Parameters.Add("documento", pers.Documento);
                cmds[0].Parameters.Add("idUsuario", 1);



                cmds[1].Parameters.Add("idEmpleado", id);
                cmds[1].Parameters.Add("grado", emp.Grado);
                cmds[1].Parameters.Add("salario", emp.Salario);

                cmds[2].Parameters.Add("NOMBREUSUARIO", usu.NombreUsuario);
                cmds[2].Parameters.Add("PASSWORD", usu.Password);
                cmds[2].Parameters.Add("ROL", usu.Rol);
                cmds[2].Parameters.Add("EMAIL", usu.Email);
                cmds[2].Parameters.Add("PERSONA_IDPERSONA", id);

                //antes i era 2
                for (int i = 3; i < cmds.Count - contadorHorarios; i++)
                {
                    cmds[i].Parameters.Add("numero", pers.Telefonos[i - 3].Numero);
                    cmds[i].Parameters.Add("tipo", pers.Telefonos[i - 3].Tipo);
                    cmds[i].Parameters.Add("idPersona", id);
                    contador++;
                }

                for (int i = contador; i < cmds.Count; i++)
                {
                    cmds[i].Parameters.Add("idHorario", Emp.Horarios[i - contador].IdHorario);
                    cmds[i].Parameters.Add("idEmpleado",id);
                }
                met.ExecuteNBasicCommand(cmds);
                Metodos.GenerateLogs("Empleado", "Insert", "Se inserto " + pers.IdPersona);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public  void Update()
        {
            met = new Metodos();
            List<string> querys = new List<string>();
            querys.Add("UPDATE Persona SET nombres=:nombres,primerApellido=:primerApellido,segundoApellido=:segundoApellido,sexo=:sexo, fechaNacimiento=:fechaNacimiento,documento=:documento, fupdate=CURRENT_TIMESTAMP WHERE IDPERSONA=:idPersona ");
            querys.Add("UPDATE Empleado SET grado=:grado,salario=:salario WHERE PERSONA_IDPERSONA=:idEmpleado ");
            querys.Add("UPDATE Usuario SET NOMBREUSUARIO=:NOMBREUSUARIO,PASSWORD=:PASSWORD,ROL=:ROL,EMAIL=:EMAIL WHERE PERSONA_IDPERSONA=:PERSONA_IDPERSONA");
            
            //string query3 = "INSERT INTO Telefono(numero,tipo,idPersona,fUpdate) VALUES(@numero,@tipo,@idPersona,CURRENT_TIMESTAMP)";
            try
            {
                List<OracleCommand> cmds = met.CreateNBasicCommand2(querys);

                cmds[0].Parameters.Add("nombres", pers.Nombres);
                cmds[0].Parameters.Add("primerApellido", pers.PrimerApellido);
                cmds[0].Parameters.Add("segundoApellido", pers.SegundoApellido);
                cmds[0].Parameters.Add("sexo", pers.Sexo);
                cmds[0].Parameters.Add("fechaNacimiento", pers.FechaNacimiento);
                cmds[0].Parameters.Add("documento", pers.Documento);
                cmds[0].Parameters.Add("idPersona", pers.IdPersona);


                cmds[1].Parameters.Add("grado", emp.Grado);
                cmds[1].Parameters.Add("salario", emp.Salario);
                cmds[1].Parameters.Add("idEmpleado", pers.IdPersona);


                cmds[2].Parameters.Add("NOMBREUSUARIO", usu.NombreUsuario);
                cmds[2].Parameters.Add("PASSWORD", usu.Password);
                cmds[2].Parameters.Add("ROL", usu.Rol);
                cmds[2].Parameters.Add("EMAIL", usu.Email);
                cmds[2].Parameters.Add("PERSONA_IDPERSONA", pers.IdPersona);

                met.ExecuteNBasicCommand(cmds);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        //public override void Delete()
        //{

        //}
        public void Delete(int id)
        {
            try
            {
                string query = "UPDATE Persona SET estado=0 WHERE idPersona=:idPersona";

                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idPersona",id)
            };
                met.ExecuteBasicCommand(query, pararmetros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public  DataTable Select()
        {
            
            met = new Metodos();
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectEmpleados";
            try
            {
                DataTable dt = new DataTable();
                dt.Load(met.ExecuteDataReaderCommand(query));
                res = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
        public Persona Get(int idPersona)
        {
            met = new Metodos();
            Persona res = null;
            OracleDataReader dr = null;
            string query = "SELECT  P.idpersona, p.nombres,P.primerApellido,P.segundoApellido,P.sexo,P.fechaNacimiento,P.documento, P.fUpdate, P.estado FROM Persona P WHERE idPersona=:idPersona";
            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idPersona",idPersona)
            };
                dr = met.ExecuteDataReaderCommandParametros(query, pararmetros);


                while (dr.Read())
                {
                    res = new Persona(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), char.Parse(dr[4].ToString()), DateTime.Parse(dr[5].ToString()), dr[6].ToString(), DateTime.Parse(dr[7].ToString()), byte.Parse(dr[8].ToString()));
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                dr.Close();
            }

            return res;
        }
        public DataTable GetTelefono(int idPersona)
        {
            met = new Metodos();
            DataTable res = new DataTable();
            string query = "SELECT idTelefono, numero, tipo FROM Telefono WHERE PERSONA_IDPERSONA=:idPersona";
            try
            {
                OracleCommand cmd = met.CreateBasicCommand(query);
                cmd.Parameters.Add("idPersona", idPersona);
                res = met.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }

        public DataTable GetHorarios(int idPersona)
        {
            met = new Metodos();
            DataTable res = new DataTable();
            string query = (@"SELECT IDHORARIO,LUNES,MARTES,MIERCOLES,JUEVES,VIERNES,SABADO,DOMINGO,HORAINICIO,HORAFIN
                            FROM HORARIOEMPLEADO HE
                            INNER JOIN EMPLEADOHORARIO EH ON EH.HORARIOEMPLEADO_IDHORARIO=HE.IDHORARIO 
                            WHERE ESTADO=1 AND EH.EMPLEADO_PERSONA_IDPERSONA=:idPersona");
            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idPersona",idPersona)
            };
                DataTable dt = new DataTable();
                dt.Load(met.ExecuteDataReaderCommandParametros(query, pararmetros));
                res = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
        public Telefono GetUnTelefono(int idTelefono)
        {
            Telefono res = null;
            OracleDataReader dr = null;
            string query = "SELECT idTelefono, numero, tipo, PERSONA_IDPERSONA FROM Telefono WHERE idTelefono=:idTelefono";

            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idTelefono",idTelefono)
            };
                dr = met.ExecuteDataReaderCommandParametros(query, pararmetros);
                while (dr.Read())
                {
                    res = new Telefono(int.Parse(dr[0].ToString()), dr[1].ToString(), Convert.ToChar(dr[2].ToString()), int.Parse(dr[3].ToString()));
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                dr.Close();
            }

            return res;
        }
        public void InsertTelefono(int id)
        {
            met = new Metodos();
            List<string> querys = new List<string>();
            for (int i = 0; i < pers.Telefonos.Count; i++)
            {
                querys.Add("INSERT INTO Telefono(NUMERO,TIPO,PERSONA_IDPERSONA,FUPDATE) VALUES(:numero,:tipo,:idPersona,CURRENT_TIMESTAMP)");
            }
            try
            {
                List<OracleCommand> cmds = met.CreateNBasicCommand2(querys);
                for (int i = 0; i < cmds.Count; i++)
                {
                    cmds[i].Parameters.Add("numero", pers.Telefonos[i - 0].Numero);
                    cmds[i].Parameters.Add("tipo", pers.Telefonos[i - 0].Tipo);
                    cmds[i].Parameters.Add("idPersona", id);
                }
                met.ExecuteNBasicCommand(cmds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool ExisteTelefono(int id,string numero, char tipo)
        {
            OracleDataReader dr = null;
            string query = "SELECT IDTELEFONO, NUMERO, TIPO, PERSONA_IDPERSONA FROM TELEFONO WHERE numero=:numero AND PERSONA_IDPERSONA=:PERSONA_IDPERSONA AND tipo=:tipo";
            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("numero",numero),
                new OracleParameter("PERSONA_IDPERSONA",id),
                new OracleParameter("tipo",tipo)

            };
                dr = met.ExecuteDataReaderCommandParametros(query, pararmetros);

                return dr.Read();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                dr.Close();
            }
        }


        public Empleado Get2(int idEmpleado)
        {
            met = new Metodos();
            Empleado res = null;
            OracleDataReader dr = null;
            string query = "SELECT PERSONA_IDPERSONA, grado, salario  FROM Empleado WHERE PERSONA_IDPERSONA=:idEmpleado ";
            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idEmpleado",idEmpleado)
            };
                dr = met.ExecuteDataReaderCommandParametros(query, pararmetros);
                while (dr.Read())
                {
                    res = new Empleado(int.Parse(dr[0].ToString()), dr[1].ToString(), Convert.ToDouble(dr[2].ToString()));
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                dr.Close();
            }

            return res;
        }
        public Usuario Get3(int idUsuario)
        {
            met = new Metodos();
            Usuario res = null;
            OracleDataReader dr = null;
            string query = "SELECT PERSONA_IDPERSONA,NOMBREUSUARIO,PASSWORD,ROL,EMAIL  FROM USUARIO WHERE PERSONA_IDPERSONA=:idUsuario ";
            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idUsuario",idUsuario)
            };
                dr = met.ExecuteDataReaderCommandParametros(query, pararmetros);
                while (dr.Read())
                {
                    res = new Usuario(int.Parse(dr[0].ToString()), dr[1].ToString(),dr[2].ToString(),dr[3].ToString(),dr[4].ToString());
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                dr.Close();
            }

            return res;
        }
        public bool Existe(string nombre)
        {
            OracleDataReader dr = null;
            string query = "SELECT idPersona, nombres FROM Persona WHERE nombres=:nombres";
            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("nombres",nombre)
            };
                dr = met.ExecuteDataReaderCommandParametros(query, pararmetros);

                return dr.Read();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                dr.Close();
            }
        }

        public DataTable SelectHorarios()
        {
            met = new Metodos();
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectHorarioEmpleado";
            try
            {
                DataTable dt = new DataTable();
                dt.Load(met.ExecuteDataReaderCommand(query));
                res = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
    }
}
