﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using System.Configuration;
using Oracle.DataAccess;
using System.Data;

namespace DALOracle
{
    public class Metodos
    {
         OracleConnection cn = new OracleConnection("DATA SOURCE=xe ; PASSWORD=bdmandala1234; USER ID=BDMandala");

        public void MatarConexion()
         {
             cn.Close();
         }
        public void ExecuteBasicCommand(string consulta, OracleParameter[] parametrosRecepcion)
        {
            try
            {
                MatarConexion();
                OracleCommand cmd;
                string query = consulta;
                cmd = new OracleCommand(query);
                OracleParameter[] pararmetros = parametrosRecepcion;
                cmd.Parameters.AddRange(pararmetros);
                cmd.Connection = cn;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public void ExecuteBasicCommandSinParametros(string consulta)
        {
            try
            {
                OracleCommand cmd;
                string query = consulta;
                cmd = new OracleCommand(query);
                cmd.Connection = cn;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public void ExecuteNBasicCommand(List<OracleCommand> cmds)
        {
                OracleCommand cmd;
                OracleTransaction tran = null;
                try
                {
                    cmds[0].Connection.Open();
                    tran = cmds[0].Connection.BeginTransaction();
                    foreach (OracleCommand cmdItem in cmds)
                    {
                        cmdItem.Transaction = tran;
                        cmdItem.ExecuteNonQuery();
                    }
                    tran.Commit();


                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
                finally
                {
                    cmds[0].Connection.Close();
                }
        }
        public  List<OracleCommand> CreateNBasicCommand(int n)
        {
            List<OracleCommand> res = new List<OracleCommand>();
            OracleConnection connection = new OracleConnection("DATA SOURCE=xe ; PASSWORD=bdmandala1234; USER ID=BDMandala");
            for (int i = 0; i < n; i++)
            {
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = connection;
                //cmd.CommandType = CommandType.Text;
                res.Add(cmd);
            }

            return res;
        }
        public List<OracleCommand> CreateNBasicCommand2(List<string> list)
        {
            List<OracleCommand> res = new List<OracleCommand>();

            OracleConnection connection = new OracleConnection("DATA SOURCE=xe ; PASSWORD=bdmandala1234; USER ID=BDMandala");
            for (int i = 0; i < list.Count; i++)
            {
                OracleCommand cmd = new OracleCommand(list[i]);
                cmd.Connection = connection;
                cmd.CommandType = CommandType.Text;
                res.Add(cmd);
            }


            return res;

        }
        public OracleDataReader ExecuteDataReaderCommand(string consulta)
        {


            OracleDataReader res = null;
            try
            {
                OracleCommand cmd;
                string query = consulta;
                cmd = new OracleCommand(query);
                cmd.Connection = cn;
                cn.Open();
                cmd.ExecuteNonQuery();
              
                res = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }

        public OracleDataReader ExecuteDataReaderCommandParametros(string consulta, OracleParameter[] parametrosRecepcion)
        {
            OracleDataReader res = null;
            try
            {
                MatarConexion();
                OracleCommand cmd;
                string query = consulta;
                cmd = new OracleCommand(query);
                OracleParameter[] pararmetros = parametrosRecepcion;
                cmd.Parameters.AddRange(pararmetros);
                cmd.Connection = cn;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                res = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {

                throw ex;
            }

          
            return res;
            cn.Close();

        }


        public  int GetCurrentValueIDSecuencia(string nombreSecuencia)
        {
            int res = -1;
          
            ///revisar comandos de oracle para obtener este id
            ///select secuencia.nextval from dual
            string query = "SELECT " + nombreSecuencia + ".NEXTVAL FROM DUAL";
            try
            {
                OracleCommand cmd = CreateBasicCommand(query);
                res = int.Parse(ExecuteScalarCommand(cmd));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }

        public  OracleCommand CreateBasicCommand(string query)
        {
            OracleConnection connection = new OracleConnection("DATA SOURCE=xe ; PASSWORD=bdmandala1234; USER ID=BDMandala");
            OracleCommand cmd = new OracleCommand(query);
            cmd.Connection = connection;
            return cmd;
        }
        public  string ExecuteScalarCommand(OracleCommand cmd)
        {
            try
            {
                cmd.Connection.Open();
                return cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        public  DataTable ExecuteDataTableCommand(OracleCommand cmd)
        {
            DataTable res = new DataTable();
            try
            {
                cmd.Connection.Open();
                OracleDataAdapter da = new OracleDataAdapter(cmd);
                da.Fill(res);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }
        public void Execute2BasicCommand(OracleCommand cmd1, OracleCommand cmd2)
        {
            OracleTransaction tran = null;
            try
            {
                cmd1.Connection.Open();
                tran = cmd1.Connection.BeginTransaction();
                cmd1.Transaction = tran;
                cmd1.ExecuteNonQuery();
                cmd2.Transaction = tran;
                cmd2.ExecuteNonQuery();
                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();
                throw ex;
            }
            finally
            {
                cmd1.Connection.Close();
            }
        }
        public static void GenerateLogs(string tabla, string metodo, string mensaje)
        {
            System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} {2}", DateTime.Now.ToShortDateString(), mensaje + metodo, tabla));
        }
    }


}
