﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Oracle.DataAccess.Client;
using Oracle.DataAccess;
using Common;
using System.Security.Cryptography;

namespace DALOracle
{
    public class UsuarioDAL
    {
         #region Atributos, propiedades y constructores
        private Usuario usu;
        private Metodos met= new Metodos();

        public Usuario Usu
        {
            get { return usu; }
            set { usu = value; }
        }
        public UsuarioDAL ()
	{

	}
        public UsuarioDAL(Usuario usu)
        {
            this.usu = usu;
        }
        #endregion

        public  void Insert()
        {
            try
            {
                string query = @"INSERT INTO DOSIFICACION (IDUSUARIO,NOMBREUSUARIO,PASSWORD,ROL,EMAIL,ESTADO,FUPDATE,PERSONA_IDPERSONA)VALUES(IDUSUARIO,NOMBREUSUARIO,PASSWORD,ROL,EMAIL,ESTADO,FUPDATE,PERSONA_IDPERSONA)";
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                //new OracleParameter("NUMEROAUTORIZACION",usu.NumeroAutorizacion),
                //new OracleParameter("FECHALIMITEEMISION", usu.FechaLimite),
                //new OracleParameter("LLAVEDOCIFICACION", usu.LlaveDocificada),
                //new OracleParameter("NROINICIALFACTURA", usu.NroInicial),
                //new OracleParameter("NROFINALFACTURA", usu.NroFinal),
                //new OracleParameter("SEGUNDALEYENDA", usu.SegundaLeyenda),
                //new OracleParameter("ACTIVIDADECONOMICA", usu.ActividadEconomica),
                //new OracleParameter("SUCURSAL", usu.Sucursal),
                //new OracleParameter("USUARIO", usu.Usuario),
                //new OracleParameter("SFC", usu.Sfc)
            };
                met.ExecuteBasicCommand(query, pararmetros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public  void Update()
        {
            try
            {
                string query = @"UPDATE Usuario SET NOMBREUSUARIO=:NOMBREUSUARIO,PASSWORD=:PASSWORD,ROL=:ROL,EMAIL=:EMAIL,FUPDATE=CURRENT_TIMESTAMP WHERE PERSONA_IDPERSONA=:PERSONA_IDPERSONA";
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("NOMBREUSUARIO",usu.NombreUsuario),
                new OracleParameter("PASSWORD", usu.Password),
                new OracleParameter("ROL", usu.Rol),
                new OracleParameter("EMAIL", usu),

            };
                met.ExecuteBasicCommand(query, pararmetros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
       
        }






        public  void Delete()
        {
            //string query = "UPDATE Usuario SET estado=0 WHERE idUsuario=@idUsuario OR nombreUsuario=@nombreUsuario";
            //try
            //{
            //    SqlCommand cmd = Metodos.CreateBasicCommand(query);
            //    cmd.Parameters.AddWithValue("@idUsuario", user.IdUsuario);
            //    cmd.Parameters.AddWithValue("@nombreUsuario", user.NombreUsuario);
            //    Metodos.ExecuteBasicCommand(cmd);
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
        }

        public  DataTable Select()
        {
            DataTable res = new DataTable();
            //string query = "SELECT * FROM vwSelectUsuario  ORDER BY 2";
            //try
            //{
            //    SqlCommand cmd = Metodos.CreateBasicCommand(query);
            //    res = Metodos.ExecuteDataTableCommand(cmd);
            //}
            //catch (Exception ex)
            //{

            //    throw ex;
            //}
            return res;
        }
        public DataTable Select2()
        {
            DataTable res = new DataTable();
            //string query = "nombreUsuario, CONVERT(varchar(MAX), password,1) AS password, rol FROM Usuario WHERE estado=1 AND idUsuario=@nombreUsuario";
            //try
            //{
            //    SqlCommand cmd = Metodos.CreateBasicCommand(query);
            //    cmd.Parameters.AddWithValue("@nombreUsuario", Sesion.idSesion);
            //    res = Metodos.ExecuteDataTableCommand(cmd);
            //}
            //catch (Exception ex)
            //{

            //    throw ex;
            //}
            return res;
        }
        public DataTable Login(string usuario, string password)
        {
            met = new Metodos();
            DataTable res = new DataTable();
            MD5 md5Hash = MD5.Create();// revisar si esto funciona
            password =  GetMd5Hash(md5Hash, password).ToString();

            string query = "SELECT idUsuario,nombreUsuario, rol FROM Usuario WHERE estado=1 AND  nombreUsuario=:nombreUsuario AND password=:password";
            try
            {
                OracleCommand cmd = met.CreateBasicCommand(query);
                cmd.Parameters.Add("usuario", usuario);
                cmd.Parameters.Add("password", password);
                res = met.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
        static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
        //static string BytesToString(byte[] bytes)
        //{
        //    using (var stream = new MemoryStream(bytes))
        //    {
        //        using (var streamReader = new StreamReader(stream))
        //        {
        //            return streamReader.ReadToEnd();
        //        }
        //    }
        //}




    }
}
