﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Oracle.DataAccess.Client;
using Oracle.DataAccess;
using Common;

namespace DALOracle
{
    public class DosificacionDAL
    {
          #region Atributos, propiedades y constructores
        private Dosificacion dosi;
        private Metodos met= new Metodos();

        public Dosificacion Trat
        {
            get { return dosi; }
            set { dosi = value; }
        }
        public DosificacionDAL()
        {

        }
        public DosificacionDAL(Dosificacion dosi)
        {
            this.dosi = dosi;
        }
        #endregion

        public  void Insert()
        {
            try
            {
                string query = @"INSERT INTO DOSIFICACION (NUMEROAUTORIZACION,FECHALIMITEEMISION,LLAVEDOCIFICACION,
                                NROINICIALFACTURA,NROFINALFACTURA,SEGUNDALEYENDA, ACTIVIDADECONOMICA, SUCURSAL, USUARIO,SFC)
                                 VALUES(:NUMEROAUTORIZACION,:FECHALIMITEEMISION,:LLAVEDOCIFICACION,:NROINICIALFACTURA,
                                :NROFINALFACTURA,:SEGUNDALEYENDA, :ACTIVIDADECONOMICA, :SUCURSAL, :USUARIO,:SFC)";
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("NUMEROAUTORIZACION",dosi.NumeroAutorizacion),
                new OracleParameter("FECHALIMITEEMISION", dosi.FechaLimite),
                new OracleParameter("LLAVEDOCIFICACION", dosi.LlaveDocificada),
                new OracleParameter("NROINICIALFACTURA", dosi.NroInicial),
                new OracleParameter("NROFINALFACTURA", dosi.NroFinal),
                new OracleParameter("SEGUNDALEYENDA", dosi.SegundaLeyenda),
                new OracleParameter("ACTIVIDADECONOMICA", dosi.ActividadEconomica),
                new OracleParameter("SUCURSAL", dosi.Sucursal),
                new OracleParameter("USUARIO", dosi.Usuario),
                new OracleParameter("SFC", dosi.Sfc)
            };
                met.ExecuteBasicCommand(query, pararmetros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public  void Update()
        {
            try
            {
                string query = @"UPDATE DOSIFICACION SET NUMEROAUTORIZACION=:NUMEROAUTORIZACION,FECHALIMITEEMISION=:FECHALIMITEEMISION,LLAVEDOCIFICACION=:LLAVEDOCIFICACION,
                                NROINICIALFACTURA=:NROINICIALFACTURA,NROFINALFACTURA=:NROFINALFACTURA,SEGUNDALEYENDA=:SEGUNDALEYENDA,
                                    ACTIVIDADECONOMICA=:ACTIVIDADECONOMICA, SUCURSAL=:SUCURSAL, USUARIO=:USUARIO,SFC=:SFC, fUpdate=CURRENT_TIMESTAMP WHERE IDDOSIFICACION=:IDDOSIFICACION";
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("NUMEROAUTORIZACION",dosi.NumeroAutorizacion),
                new OracleParameter("FECHALIMITEEMISION", dosi.FechaLimite),
                new OracleParameter("LLAVEDOCIFICACION", dosi.LlaveDocificada),
                new OracleParameter("NROINICIALFACTURA", dosi.NroInicial),
                new OracleParameter("NROFINALFACTURA", dosi.NroFinal),
                new OracleParameter("SEGUNDALEYENDA", dosi.SegundaLeyenda),
                new OracleParameter("ACTIVIDADECONOMICA", dosi.ActividadEconomica),
                new OracleParameter("SUCURSAL", dosi.Sucursal),
                new OracleParameter("USUARIO", 1),
                new OracleParameter("SFC", dosi.Sfc),
                new OracleParameter("IDDOSIFICACION",dosi.IdDosificacion)

            };
                met.ExecuteBasicCommand(query, pararmetros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
       
        }

        public  void Delete()
        {

            try
            {
                string query = "UPDATE DOSIFICACION SET estado=0 WHERE IDDOSIFICACION=:idTratamiento";

                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idtratamiento",dosi.IdDosificacion)
            };
                met.ExecuteBasicCommand(query, pararmetros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public void Delete(int id)
        {
            try
            {
                string query = "UPDATE DOSIFICACION SET estado=0 WHERE IDDOSIFICACION=:IDDOSIFICACION";

                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("IDDOSIFICACION",id)
            };
                met.ExecuteBasicCommand(query, pararmetros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public  DataTable Select()
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectDosificaciones ";
            try
            {
                DataTable dt = new DataTable();
                dt.Load(met.ExecuteDataReaderCommand(query));
                res = dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }

        public Dosificacion Get(int idDosificacion)
        {
            Dosificacion res = new Dosificacion();
            OracleDataReader dr=null;
            
            try
            {
                string query = @"SELECT IDDOSIFICACION, NUMEROAUTORIZACION,FECHALIMITEEMISION,LLAVEDOCIFICACION,
                                NROINICIALFACTURA,NROFINALFACTURA,SEGUNDALEYENDA, ACTIVIDADECONOMICA, SUCURSAL, SFC 
                             FROM DOSIFICACION WHERE IDDOSIFICACION=:IDDOSIFICACION";
            
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("IDDOSIFICACION",idDosificacion)
            };
                dr = met.ExecuteDataReaderCommandParametros(query, pararmetros);
                while (dr.Read())
                {
                    res = new Dosificacion(int.Parse(dr[0].ToString()), double.Parse(dr[1].ToString()), DateTime.Parse(dr[2].ToString()), dr[3].ToString(), double.Parse(dr[4].ToString()),double.Parse( dr[5].ToString()), dr[6].ToString(), dr[7].ToString(), dr[8].ToString(), dr[9].ToString());
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                
                dr.Close();
            }

            return res;
        }
        public bool Existe(string llaveDocificacion)
        {
            OracleDataReader dr = null;
            string query = @"SELECT IDDOSIFICACION, NUMEROAUTORIZACION
                               FROM DOSIFICACION WHERE LLAVEDOCIFICACION=:LLAVEDOCIFICACION";
            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("LLAVEDOCIFICACION",llaveDocificacion)
            };
                dr = met.ExecuteDataReaderCommandParametros(query, pararmetros);
                return dr.Read();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dr.Close();
            }
        }
    }
}
