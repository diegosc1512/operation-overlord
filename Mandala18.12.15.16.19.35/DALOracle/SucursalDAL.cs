﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Oracle.DataAccess.Client;
using Oracle.DataAccess;
using Common;

namespace DALOracle
{
    public class SucursalDAL
    {
        #region Atributos, propiedades y constructores
        private Sucursal suc;
        private Empleado emp;
        Metodos met = new Metodos();
        public Empleado Emp
        {
            get { return emp; }
            set { emp = value; }
        }
        public Sucursal Trat
        {
            get { return suc; }
            set { suc = value; }
        }
        public SucursalDAL()
        {

        }
        public SucursalDAL(Sucursal suc)
        {
            this.suc = suc;
        }
        #endregion


        public  void Insert()
        {
            met = new Metodos();
            List<string> querys = new List<string>();
            querys.Add("INSERT INTO Sucursal(nombreSucursal,latitud,longitud,direccion)  VALUES (:nombreSucursal,:latitud,:longitud,:direccion)");
            int id = met.GetCurrentValueIDSecuencia("SEQ_sucursal");
            for (int i = 0; i < suc.Empleados.Count; i++)
            {
                querys.Add("INSERT INTO EMPLEADOSUCURSAL (SUCURSAL_IDSUCURSAL, EMPLEADO_PERSONA_IDPERSONA) VALUES(:idSucursal,:idEmpleado)");
            }
            try
            {
                List<OracleCommand> cmds = met.CreateNBasicCommand2(querys);
                cmds[0].Parameters.Add("nombreSucursal",suc.NombreSucursal);
                cmds[0].Parameters.Add("latitud", suc.Latitud);
                cmds[0].Parameters.Add("longitud",suc.Longitud);
                cmds[0].Parameters.Add("direccion", suc.Direccion);
               
                for (int i = 1; i < cmds.Count ; i++)
                {
                    cmds[i].Parameters.Add("idSucursal", id);
                    cmds[1].Parameters.Add("idEmpleado", suc.Empleados[i - 1].IdEmpleado);
                }
                met.ExecuteNBasicCommand(cmds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public  void Update()
        {
            string query = "UPDATE Sucursal SET nombreSucursal=:nombreSucursal, latitud=:latitud, longitud=:longitud, direccion=:direccion, fUpdate=CURRENT_TIMESTAMP WHERE idSucursal=:idSucursal";

            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("nombreSucursal",suc.NombreSucursal),
                new OracleParameter("latitud", suc.Latitud),
                new OracleParameter("longitud",suc.Longitud),
                new OracleParameter("direccion",suc.Direccion),
                new OracleParameter("idSucursal",suc.IdSucursal)

            };
                met.ExecuteBasicCommand(query, pararmetros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public  void Delete()
        {
            string query = "UPDATE Sucursal SET estado=0 WHERE idSucursal=:idSucursal";

            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idSucursal",suc.IdSucursal)

            };
                met.ExecuteBasicCommand(query, pararmetros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public void Delete(int id)
        {
            string query = "UPDATE Sucursal SET estado=0 WHERE idSucursal=:idSucursal";

            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idSucursal",id)

            };
                met.ExecuteBasicCommand(query, pararmetros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public  DataTable Select()
        {
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectSucursales  ORDER BY 2";
            try
            {
                DataTable dt = new DataTable();
                dt.Load(met.ExecuteDataReaderCommand(query));
                res = dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }

        public Sucursal Get(int idSucursal)
        {
            Sucursal res = new Sucursal();
            OracleDataReader dr = null;
            string query = "SELECT idSucursal, nombreSucursal, latitud, longitud, direccion, estado, fUpdate FROM Sucursal WHERE idSucursal=:idSucursal";
            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idSucursal",idSucursal)
            };
                dr = met.ExecuteDataReaderCommandParametros(query, pararmetros);
                while (dr.Read())
                {
                    res = new Sucursal(int.Parse(dr[0].ToString()), dr[1].ToString(), double.Parse(dr[2].ToString()), double.Parse(dr[3].ToString()), dr[4].ToString(), byte.Parse(dr[5].ToString()), DateTime.Parse(dr[6].ToString()));
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                dr.Close();
            }

            return res;
        }
        public bool Existe(string nombre)
        {
            OracleDataReader dr = null;
            string query = "SELECT idSucursal, nombreSucursal,latitud, longitud, direccion, estado, fUpdate FROM Sucursal WHERE nombreSucursal=:nombreSucursal";
            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("nombreSucursal",nombre)
            };
                dr = met.ExecuteDataReaderCommand(query);
                return dr.Read();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dr.Close();
            }
        }

        public DataTable GetEmpleados(int idHorario)
        {
            met = new Metodos();
            DataTable res = new DataTable();
            string query = (@"SELECT E.PERSONA_IDPERSONA AS IDEMPLEADO, (P.NOMBRES||' '||P.PRIMERAPELLIDO||' '|
                            |P.SEGUNDOAPELLIDO) AS NOMBRE_COMPLETO, P.DOCUMENTO as documento 
                            FROM EMPLEADOHORARIO EH
                            INNER JOIN EMPLEADO E ON E.PERSONA_IDPERSONA=EH.EMPLEADO_PERSONA_IDPERSONA
                            INNER JOIN PERSONA P ON P.IDPERSONA=E.PERSONA_IDPERSONA 
                            WHERE P.ESTADO=1 AND EH.HORARIOEMPLEADO_IDHORARIO=:idHorario ");
            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idHorario",idHorario)
            };
                DataTable dt = new DataTable();
                dt.Load(met.ExecuteDataReaderCommandParametros(query, pararmetros));
                res = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
        public DataTable SelectEmpleados()
        {
            met = new Metodos();
            DataTable res = new DataTable();
            string query = "SELECT * FROM VWEMPLEADOSENHORARIOS ";
            try
            {
                DataTable dt = new DataTable();
                dt.Load(met.ExecuteDataReaderCommand(query));
                res = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
    }
}
