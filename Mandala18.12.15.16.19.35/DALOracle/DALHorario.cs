﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Oracle.DataAccess.Client;
using System.Configuration;
using Oracle.DataAccess;
using System.Data;
namespace DALOracle
{
    public class DALHorario
    {
             #region Atributos, propiedades y constructores
        private HorarioEmpleado horemp;
        private Metodos met;
        private Empleado emp;

        public Empleado Emp
        {
            get { return emp; }
            set { emp = value; }
        }

        public HorarioEmpleado Horemp
        {
          get { return horemp; }
          set { horemp = value; }
        }
        public DALHorario ()
	        {

	        }
        public DALHorario(HorarioEmpleado horemp)
        {
            this.horemp = horemp;
        }
        public DALHorario(HorarioEmpleado horemp, Empleado emp)
        {
            this.horemp = horemp;
            this.emp = emp;
        }
        #endregion

        public void Insert()
        {
            met = new Metodos();
            List<string> querys = new List<string>();
           querys.Add(@"INSERT INTO HorarioEmpleado(idhorario,lunes, martes, miercoles, jueves, viernes,sabado,domingo, horaInicio, horaFin) 
                        values(:idhorario, :lunes,:martes,:miercoles, :jueves, :viernes, :sabado,:domingo,:horaInicio,:horaFin) ");
            int id = met.GetCurrentValueIDSecuencia("SEQ_HorarioEmpleado");
            for (int i = 0; i < horemp.Empleados.Count; i++)
            {
                querys.Add("INSERT INTO EmpleadoHorario(HORARIOEMPLEADO_IDHORARIO,EMPLEADO_PERSONA_IDPERSONA) VALUES(:idHorario,:idEmpleado)");
            }

            try
            {

                List<OracleCommand> cmds = met.CreateNBasicCommand2(querys);

                cmds[0].Parameters.Add("idhorario", id);
                cmds[0].Parameters.Add("lunes", horemp.Lunes);
                cmds[0].Parameters.Add("martes", horemp.Martes);
                cmds[0].Parameters.Add("miercoles", horemp.Miercoles);
                cmds[0].Parameters.Add("jueves", horemp.Jueves);
                cmds[0].Parameters.Add("viernes", horemp.Viernes);
                cmds[0].Parameters.Add("sabado", horemp.Sabado);
                cmds[0].Parameters.Add("domingo", horemp.Domingo);
                cmds[0].Parameters.Add("horaInicio", horemp.HoraInicio);
                cmds[0].Parameters.Add("horaFin", horemp.HoraFin);
                
                for (int i = 1; i < cmds.Count; i++)
                {
                    cmds[i].Parameters.Add("idHorario", id);
                    cmds[i].Parameters.Add("idEmpleado", horemp.Empleados[i - 1].IdEmpleado);
                }
                met.ExecuteNBasicCommand(cmds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public  void Update()
        {
            met = new Metodos();
            List<string> querys = new List<string>();
            querys.Add(@"UPDATE HorarioEmpleado SET lunes=:lunes, martes=:martes, miercoles=:miercoles, jueves=:jueves,
                        viernes=:viernes,sabado=:sabado,domingo=:domingo, horaInicio=:horaInicio, horaFin=:horaFin
                        ,fUpdate=CURRENT_TIMESTAMP WHERE idHorario=:idHorario ");
            //int id = met.GetCurrentValueIDSecuencia("SEQ_HorarioEmpleado");
            for (int i = 0; i < horemp.Empleados.Count; i++)
            {
                querys.Add("UPDATE EmpleadoHorario(HORARIOEMPLEADO_IDHORARIO,EMPLEADO_PERSONA_IDPERSONA) VALUES(:idHorario,:idEmpleado)");
            }

            try
            {

                List<OracleCommand> cmds = met.CreateNBasicCommand2(querys);

                cmds[0].Parameters.Add("lunes", horemp.Lunes);
                cmds[0].Parameters.Add("martes", horemp.Martes);
                cmds[0].Parameters.Add("miercoles", horemp.Miercoles);
                cmds[0].Parameters.Add("jueves", horemp.Jueves);
                cmds[0].Parameters.Add("viernes", horemp.Viernes);
                cmds[0].Parameters.Add("sabado", horemp.Sabado);
                cmds[0].Parameters.Add("domingo", horemp.Domingo);
                cmds[0].Parameters.Add("horaInicio", horemp.HoraInicio);
                cmds[0].Parameters.Add("horaFin", horemp.HoraFin);
                cmds[0].Parameters.Add("idhorario", horemp.IdHorario);

                for (int i = 1; i < cmds.Count; i++)
                {
                    cmds[i].Parameters.Add("idHorario", horemp.IdHorario);
                    cmds[i].Parameters.Add("idEmpleado", horemp.Empleados[i - 1].IdEmpleado);
                }
                met.ExecuteNBasicCommand(cmds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Delete(int id)
        {
            string query = "UPDATE HorarioEmpleado SET estado=0 WHERE idHorario=:idHorario";
            try{

            OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idHorario",id)
            };
                met.ExecuteBasicCommand(query, pararmetros);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public  DataTable Select()
        {
            met = new Metodos();
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectHorariosEmpleados ";
            try
            {
                DataTable dt = new DataTable();
                dt.Load(met.ExecuteDataReaderCommand(query));
                res = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
        public HorarioEmpleado Get(int idHorario)
        {
            met = new Metodos();
            HorarioEmpleado res = null;
            OracleDataReader dr = null;
            string query = "SELECT idHorario,lunes, martes, miercoles, jueves, viernes,sabado,domingo, horaInicio, horaFin, fUpdate, estado FROM HorarioEmpleado WHERE idHorario=:idHorario";
            try
            {

                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idHorario",idHorario)
            };
                dr = met.ExecuteDataReaderCommandParametros(query, pararmetros);

                while (dr.Read())
                {
                    //res = new HorarioEmpleado(int.Parse(dr[0].ToString()), char.Parse(dr[1].ToString()), char.Parse(dr[2].ToString()), char.Parse(dr[3].ToString()), char.Parse(dr[4].ToString()), char.Parse(dr[5].ToString()), char.Parse(dr[6].ToString()), char.Parse(dr[7].ToString()), DateTime.Parse(dr[8].ToString()), DateTime.Parse(dr[9].ToString()), int.Parse(dr[10].ToString()), DateTime.Parse(dr[11].ToString()), byte.Parse(dr[12].ToString()));
                    res = new HorarioEmpleado(int.Parse(dr[0].ToString()), char.Parse(dr[1].ToString()), char.Parse(dr[2].ToString()), char.Parse(dr[3].ToString()), char.Parse(dr[4].ToString()), char.Parse(dr[5].ToString()), char.Parse(dr[6].ToString()), char.Parse(dr[7].ToString()), DateTime.Parse(dr[8].ToString()), DateTime.Parse(dr[9].ToString()), DateTime.Parse(dr[10].ToString()), byte.Parse(dr[11].ToString()));
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                dr.Close();
            }

            return res;
        }
        /// <summary>
        /// Select Like del documento o nombre completo en funcion de la opcionLike
        /// </summary>
        /// <param name="opcionLike">0 es busqueda por documnento, 1 busqueda por nombre completo</param>
        /// <returns></returns>
        public DataTable SelectLikeCiFullName(byte opcionLike, string texto)
        {
            met = new Metodos();
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwEmpleadoIdDocumentoFullName  ";

            switch (opcionLike)
            {
                case 0:
                    query = query + "WHERE Documento LIKE :texto ";
                    break;
                case 1:
                    query = query + "WHERE \"NOMBRE COMPLETO\" LIKE :texto ";
                    break;
            }
            query = query + " ORDER BY 2";


            try
            {
                OracleCommand cmd = met.CreateBasicCommand(query);

                cmd.Parameters.Add(":texto", "%"+texto+"%");

                res = met.ExecuteDataTableCommand(cmd);


            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }

        public DataTable SelectEmpleados()
        {
            met = new Metodos();
            DataTable res = new DataTable();
            string query = "SELECT * FROM VWEMPLEADOSENHORARIOS ";
            try
            {
                DataTable dt = new DataTable();
                dt.Load(met.ExecuteDataReaderCommand(query));
                res = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }

        public DataTable GetEmpleados(int idHorario)
        {
            met = new Metodos();
            DataTable res = new DataTable();
            string query = (@"SELECT E.PERSONA_IDPERSONA AS IDEMPLEADO, (P.NOMBRES||' '||P.PRIMERAPELLIDO||' '||P.SEGUNDOAPELLIDO) AS NOMBRE_COMPLETO , P.DOCUMENTO as documento FROM EMPLEADOHORARIO EH INNER JOIN EMPLEADO E ON E.PERSONA_IDPERSONA=EH.EMPLEADO_PERSONA_IDPERSONA INNER JOIN PERSONA P ON P.IDPERSONA=E.PERSONA_IDPERSONA WHERE P.ESTADO=1 AND EH.HORARIOEMPLEADO_IDHORARIO=:idHorario ");
            try
            {
                OracleParameter[] pararmetros = new OracleParameter[]
            {
                new OracleParameter("idHorario",idHorario)
            };
                DataTable dt = new DataTable();
                dt.Load(met.ExecuteDataReaderCommandParametros(query, pararmetros));
                res = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
    }
}
