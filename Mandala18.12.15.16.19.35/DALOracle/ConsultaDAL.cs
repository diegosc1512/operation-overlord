﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Common;
using Oracle.DataAccess.Client;

namespace DALOracle
{
    public class ConsultaDAL
    {
        #region Atributos, propiedades y constructores
        private Consulta cons;
        private Metodos met;
        private List<TratamientoConsulta> lisTrat;

        public List<TratamientoConsulta> LisTrat
        {
            get { return lisTrat; }
            set { lisTrat = value; }
        }

        public Consulta Cons
        {
            get { return cons; }
            set { cons = value; }
        }


        public ConsultaDAL()
        {

        }
        public ConsultaDAL(Consulta consulta, List<TratamientoConsulta> lisTrat)
        {
            this.cons = consulta;
            this.lisTrat = lisTrat;
        }
        #endregion
        public  void Insert()
        {
            met = new Metodos();
           // cons = new Consulta();
            List<string> querys = new List<string>();
            querys.Add(@"INSERT INTO Consulta(idconsulta, cliente_persona_idpersona,fechaConsulta,Empleado_persona_idpersona,horaInicio, horaFin,
                        diagnostico, total, estado) VALUES (:idConsulta,:idCliente,:fechaConsulta,:idEmpleado,:horaInicio,
                        :horaFin, :diagnostico, :total, 'R')");
            int id = met.GetCurrentValueIDSecuencia("SEQ_CONSULTA");
            for (int i = 0; i < cons.TratamientoConsulta.Count; i++)
            {
                querys.Add(@"INSERT INTO TratamientoConsulta( consulta_idconsulta,tratamiento_idTratamiento,cantidadSesiones,
                    precioTratamiento) VALUES (:idConsulta,:idTratamiento,:cantidadDeSesiones,:precio)");
            }
            try
            {

                List<OracleCommand> cmds = met.CreateNBasicCommand2(querys);
                cmds[0].Parameters.Add(":idConsulta", id);
                cmds[0].Parameters.Add(":idCliente", cons.IdCliente);
                cmds[0].Parameters.Add(":fechaConsulta", cons.FechaConsulta);
                cmds[0].Parameters.Add(":idEmpleado", cons.IdEmpleado);
                cmds[0].Parameters.Add(":horaInicio", cons.HoraInicio);
                cmds[0].Parameters.Add(":horaFin", cons.HoraFin);
                cmds[0].Parameters.Add(":diagnostico", cons.Diagnostico);
                cmds[0].Parameters.Add(":total", cons.Total);

                for (int i = 1; i < cmds.Count; i++)
                {
                    cmds[i].Parameters.Add(":idConsulta", id);
                    cmds[i].Parameters.Add(":idTratamiento", cons.TratamientoConsulta[i - 1].IdTratamiento);
                    cmds[i].Parameters.Add(":cantidadDeSesiones", cons.TratamientoConsulta[i - 1].CantidadDeSesiones);
                    cmds[i].Parameters.Add(":precio", cons.TratamientoConsulta[i - 1].Precio);
                }
                met.ExecuteNBasicCommand(cmds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public void Insert2(Consulta cons, List<TratamientoConsulta> lisTrat)
        //{
        //    List<string> querys = new List<string>();
        //    querys.Add("INSERT INTO Consulta(idCliente,fechaConsulta,idEmpleado,horaInicio, horaFin, diagnostico, total) VALUES (@idCliente,@fechaConsulta,@idEmpleado,@horaInicio, @horaFin, @diagnostico, @total)");
        //    int id = Metodos.GetCurrentValueIDTable("Consulta");
        //    foreach (TratamientoConsulta item in lisTrat)
        //    {
        //        querys.Add(" INSERT INTO TratamientoConsulta( idTratamiento, idConsulta, cantidadDeSesiones, precioTratamientoConsulta) VALUES ( @idTratamiento, @idConsulta, @cantidadDeSesiones, @precio)");

        //    }
        //    try
        //    {
        //        List<SqlCommand> cmds = Metodos.CreateNBasicCommand2(querys);

        //        cmds[0].Parameters.AddWithValue("@idCliente", cons.IdCliente);
        //        cmds[0].Parameters.AddWithValue("@fechaConsulta", cons.FechaConsulta);
        //        cmds[0].Parameters.AddWithValue("@idEmpleado", cons.IdEmpleado);
        //        cmds[0].Parameters.AddWithValue("@horaInicio", cons.HoraInicio);
        //        cmds[0].Parameters.AddWithValue("@horaFin", cons.HoraFin);
        //        cmds[0].Parameters.AddWithValue("@diagnostico", cons.Diagnostico);
        //        cmds[0].Parameters.AddWithValue("@total", cons.Total);


        //        int i = 1;
        //        foreach (TratamientoConsulta item in lisTrat)
        //        {
        //            cmds[i].Parameters.AddWithValue("@idTratamiento", item.IdTratamiento);
        //            cmds[i].Parameters.AddWithValue("@cantidadDeSesiones", item.CantidadDeSesiones);
        //            cmds[i].Parameters.AddWithValue("@precio", item.Precio);
        //            cmds[i].Parameters.AddWithValue("@idConsulta", id);
        //            i++;
        //        }

        //        Metodos.ExecuteNBasicCommand(cmds);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        public  void Update()
        {
            throw new NotImplementedException();
        }

        public  void Delete()
        {
            throw new NotImplementedException();
        }

        public  DataTable Select()
        {
            met = new Metodos();
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectHorEmpleadosCONS ";
            try
            {
                DataTable dt = new DataTable();
                dt.Load(met.ExecuteDataReaderCommand(query));
                res = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
        public DataTable SelectCliente()
        {
            met = new Metodos();
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectClientes ";
            try
            {
                DataTable dt = new DataTable();
                dt.Load(met.ExecuteDataReaderCommand(query));
                res = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
        public DataTable SelectLikeCiFullNameCliente(byte opcionLike, string texto)
        {
            met = new Metodos();
            DataTable res = new DataTable();
            string query = "SELECT * FROM SelectLikeCiFullNameCliente  ";

            switch (opcionLike)
            {
                case 0:
                    query = query + "WHERE Documento LIKE :texto ";
                    break;
                case 1:
                    query = query + "WHERE NOMBRE_COMPLETO LIKE :texto ";
                    break;
            }
            query = query + " ORDER BY 2";


            try
            {
                OracleCommand cmd = met.CreateBasicCommand(query);

                cmd.Parameters.Add("texto", "%" + texto + "%");

                res = met.ExecuteDataTableCommand(cmd);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }

        public DataTable SelectLikeCiFullNameEmpleado(byte opcionLike, string texto)
        {
            met = new Metodos();
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwEmpleadosIdDocumentoFullName  ";

            switch (opcionLike)
            {
                case 0:
                    query = query + "WHERE Documento LIKE :texto ";
                    break;
                case 1:
                    query = query + "WHERE NOMBRE_COMPLETO LIKE :texto ";
                    break;
            }
            query = query + " ORDER BY 2";


            try
            {
                OracleCommand cmd = met.CreateBasicCommand(query);
                cmd.Parameters.Add("texto", "%" + texto + "%");
                res = met.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }

        public DataTable SelectHorariosEmpleado(int id)
        {
            met = new Metodos();
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectHorarioEmpleado WHERE IDEMPLEADO=" + id;
            try
            {
                DataTable dt = new DataTable();
                dt.Load(met.ExecuteDataReaderCommand(query));
                res = dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }

        public DataTable LoadDatagridTratamiento(string txttra)
        {
            met = new Metodos();
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwTratamientoFull WHERE nombretratamiento LIKE :texto ";


            try
            {
                OracleCommand cmd = met.CreateBasicCommand(query);
                cmd.Parameters.Add(":texto", "%" + txttra + "%");
                res = met.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }

        public DataTable SelectConsulta()
        {
            met = new Metodos();
            DataTable res = new DataTable();
            string query = "SELECT * FROM vwSelectConsulta";
            try
            {
                DataTable dt = new DataTable();
                dt.Load(met.ExecuteDataReaderCommand(query));
                res = dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
    }
}
