﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Empleado :Persona
    {
        #region Atributos
        private int idEmpleado;
        private string grado;
        private double salario;
        private string nombreCompleto;
        private string documento2;
        private int p1;
        private string p2;
        private List<HorarioEmpleado> horarios;
        #endregion

        #region propiedades
        public List<HorarioEmpleado> Horarios
        {
            get { return horarios; }
            set { horarios = value; }
        }
        public string Documento2
        {
            get { return documento2; }
            set { documento2 = value; }
        }

        public string NombreCompleto
        {
            get { return nombreCompleto; }
            set { nombreCompleto = value; }
        }
        
        public double Salario
        {
            get { return salario; }
            set { salario = value; }
        }


        public string Grado
        {
            get { return grado; }
            set { grado = value; }
        }

        public int IdEmpleado
        {
            get { return idEmpleado; }
            set { idEmpleado = value; }
        }
        #endregion
        #region Constructores
        public Empleado(int idEmpleado, string grado, double salario)
        {
            this.idEmpleado=idEmpleado;
            this.grado=grado;
            this.salario=salario;
        }
        public Empleado(int idEmpleado, string grado, double salario, int idPersona, string nombres, string primerApellido, string segundoApellido, char sexo, DateTime fechaNacimiento, string documento, DateTime fUpdate, byte estado, int idUsuario, List<Telefono> telefonos)
        : base( idPersona,  nombres,  primerApellido,  segundoApellido, sexo, fechaNacimiento, documento, fUpdate,  estado,  idUsuario,  telefonos)
        {
            this.idEmpleado = idEmpleado;
            this.grado = grado;
            this.salario = salario;
        }
        public Empleado(string grado, double salario)
        {
            this.grado = grado;
            this.salario = salario;
        }
        public Empleado(string grado, double salario,List<HorarioEmpleado> horarios)
        {
            this.horarios = horarios;
            this.grado = grado;
            this.salario = salario;
        }
        public Empleado(int idEmpleado, string nombreCompleto, string documento2)
        {
            this.documento2 = documento2;
            this.idEmpleado = idEmpleado;
            this.nombreCompleto = nombreCompleto;
        }
        public Empleado()
        {

        }


        #endregion

    }
}
