﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
   public  class Consulta
    {
        private int idConsulta;
        private double total;
        private DateTime fUpdate;
        private int idCliente;
        private char estado;
        private DateTime fechaConsulta;
        private int idEmpleado;
        private DateTime horaInicio;
        private DateTime horaFin;
        private string diagnostico;
        private List<TratamientoConsulta> tratamientoConsulta;

        public List<TratamientoConsulta> TratamientoConsulta
        {
            get { return tratamientoConsulta; }
            set { tratamientoConsulta = value; }
        }



        #region propiedades
        public string Diagnostico
{
  get { return diagnostico; }
  set { diagnostico = value; }
}public DateTime HoraFin
        {
            get { return horaFin; }
            set { horaFin = value; }
        }

        public DateTime HoraInicio
        {
            get { return horaInicio; }
            set { horaInicio = value; }
        }

        public int IdEmpleado
        {
            get { return idEmpleado; }
            set { idEmpleado = value; }
        }

        public DateTime FechaConsulta
        {
            get { return fechaConsulta; }
            set { fechaConsulta = value; }
        }


        public char Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public DateTime FUpdate
        {
            get { return fUpdate; }
            set { fUpdate = value; }
        }

        public double Total
        {
            get { return total; }
            set { total = value; }
        }

        public int IdConsulta
        {
            get { return idConsulta; }
            set { idConsulta = value; }
        }
        #endregion
        public Consulta(int idConsulta, double total, DateTime fUpdate, int idCliente, char estado, DateTime fechaConsulta,int idEmpleado,DateTime horaInicio, DateTime horaFin, string diagnostico)
        {
            this.idConsulta=idConsulta;
            this.total=total;
            this.fUpdate=fUpdate;
            this.IdCliente=IdCliente;
            this.estado=estado;
            this.fechaConsulta=fechaConsulta;
            this.idEmpleado=idEmpleado;
            this.horaInicio=horaInicio;
            this.horaFin=horaFin;
            this.diagnostico=diagnostico;
        }
        public Consulta( double total, int idCliente, char estado, DateTime fechaConsulta, int idEmpleado, DateTime horaInicio, DateTime horaFin, string diagnostico)
        {
            this.total = total;
            this.IdCliente = idCliente;
            this.estado = estado;
            this.fechaConsulta = fechaConsulta;
            this.idEmpleado = idEmpleado;
            this.horaInicio = horaInicio;
            this.horaFin = horaFin;
            this.diagnostico = diagnostico;
        }
        public Consulta(double total, int idCliente, char estado, DateTime fechaConsulta, int idEmpleado, DateTime horaInicio, DateTime horaFin, string diagnostico, List<TratamientoConsulta> tratamientoConsulta)
        {
            this.total = total;
            this.IdCliente = idCliente;
            this.estado = estado;
            this.fechaConsulta = fechaConsulta;
            this.idEmpleado = idEmpleado;
            this.horaInicio = horaInicio;
            this.horaFin = horaFin;
            this.diagnostico = diagnostico;
            this.tratamientoConsulta = tratamientoConsulta;
        }
        public Consulta(int idConsulta, double total, int idCliente, char estado, DateTime fechaConsulta, int idEmpleado, DateTime horaInicio, DateTime horaFin, string diagnostico, List<TratamientoConsulta> tratamientoConsulta)
        {
            this.idConsulta = idConsulta;
            this.total = total;
            this.IdCliente = idCliente;
            this.estado = estado;
            this.fechaConsulta = fechaConsulta;
            this.idEmpleado = idEmpleado;
            this.horaInicio = horaInicio;
            this.horaFin = horaFin;
            this.diagnostico = diagnostico;
            this.tratamientoConsulta = tratamientoConsulta;
        }
        public Consulta()
        {

        }
    }
}
