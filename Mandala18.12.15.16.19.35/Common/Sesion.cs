﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class Sesion
    {
        #region atributos
        public static int idSesion;
        public static string usuarioSesion;
        public static string rolSesion;
        #endregion

        public static string VerInfo ()
        {
            return "Usuario: " + usuarioSesion + ", Rol: " + rolSesion;
        }
        public static string VerUsuario()
        {
            return usuarioSesion;
        }
    }
}
