﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Cliente
    {
        #region atributos
        private int idCliente;
        private string nit;
        private string razonSocial;

        #endregion
        #region propiedades

        public string RazonSocial
        {
            get { return razonSocial; }
            set { razonSocial = value; }
        }

        public string Nit
        {
            get { return nit; }
            set { nit = value; }
        }

        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        #endregion
        #region constructores
        public Cliente(int idCliente,string nit, string razonSocial)
        {
            this.idCliente=idCliente;
            this.nit=nit;
            this.razonSocial=razonSocial;
        }
        public Cliente(string nit,string razonSocial)
        {
            this.nit = nit;
            this.razonSocial = razonSocial;
        }
        public Cliente()
        {

        }
        #endregion
    }
}
