﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
   public  class Telefono
   {
       #region atributos
       private int idTelefono;
        private string numero;
        private char tipo;
        private int idPersona;
        private DateTime fUpdate;
       #endregion
        #region propiedades
        public DateTime FUpdate
        {
            get { return fUpdate; }
            set { fUpdate = value; }
        }

        public int IdPersona
        {
            get { return idPersona; }
            set { idPersona = value; }
        }
     

        public char Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
        

        public string Numero
        {
            get { return numero; }
            set { numero = value; }
        }
        

        public int IdTelefono
        {
            get { return idTelefono; }
            set { idTelefono = value; }
        }
        #endregion 
       #region constructores
        public Telefono(int idTelefono,string numero,char tipo, int idPersona, DateTime fUpdate)
        {
            this.idTelefono=idTelefono;
            this.numero=numero;
            this.tipo=tipo;
            this.idPersona=idPersona;
            this.fUpdate=fUpdate;
        }
        public Telefono(string numero,char tipo, int idPersona)
        {
             this.numero=numero;
             this.tipo=tipo;
             this.idPersona=idPersona;
        }

        public Telefono(int idTelefono,string numero, char tipo, int idPersona)
        {
            this.idTelefono = idTelefono;
            this.numero = numero;
            this.tipo = tipo;
            this.idPersona = idPersona;
        }
        public Telefono(string numero, char tipo)
        {
            this.numero = numero;
            this.tipo = tipo;
        }
        public Telefono()
        {

        }
       #endregion

   }
}
