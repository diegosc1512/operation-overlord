﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Dosificacion
    {
        private int idDosificacion;
        private double numeroAutorizacion;
        private DateTime fechaLimite;
        private string llaveDocificada;
        private byte estado;
        private double nroInicial;
        private double nroFinal;
        private string segundaLeyenda;
        private string actividadEconomica;
        private string sucursal;
        private DateTime fupdate;
        private int usuario;
        private string sfc;
        #region propiedades
        public string Sfc
        {
            get { return sfc; }
            set { sfc = value; }
        }

        public int Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        public DateTime Fupdate
        {
            get { return fupdate; }
            set { fupdate = value; }
        }

        public string Sucursal
        {
            get { return sucursal; }
            set { sucursal = value; }
        }

        public string ActividadEconomica
        {
            get { return actividadEconomica; }
            set { actividadEconomica = value; }
        }

        public string SegundaLeyenda
        {
            get { return segundaLeyenda; }
            set { segundaLeyenda = value; }
        }

        public double NroFinal
        {
            get { return nroFinal; }
            set { nroFinal = value; }
        }

        public double NroInicial
        {
            get { return nroInicial; }
            set { nroInicial = value; }
        }


        public byte Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        public string LlaveDocificada
        {
          get { return llaveDocificada; }
          set { llaveDocificada = value; }
        }


        public DateTime FechaLimite
        {
            get { return fechaLimite; }
            set { fechaLimite = value; }
        }

        public double NumeroAutorizacion
        {
            get { return numeroAutorizacion; }
            set { numeroAutorizacion = value; }
        }

        public int IdDosificacion
        {
            get { return idDosificacion; }
            set { idDosificacion = value; }
        }
        #endregion 

        public Dosificacion(int idDosificacion, double numeroAutorizacion, DateTime fechaLimite, string llaveDocificada, byte estado, double nroInicial, double nroFinal, string segundaLeyenda, string actividadEconomica, string sucursal, DateTime fupdate, int usuario, string sfc)
        {
            this.idDosificacion=idDosificacion;
            this.numeroAutorizacion=numeroAutorizacion;
            this.fechaLimite=fechaLimite;
            this.llaveDocificada=llaveDocificada;
            this.estado=estado;
            this.nroInicial=nroInicial;
            this.nroFinal=nroFinal;
            this.segundaLeyenda=segundaLeyenda;
            this.actividadEconomica=actividadEconomica;
            this.sucursal=sucursal;
            this.fupdate=fupdate;
            this.usuario = usuario;
            this.sfc = sfc;
        }

        public Dosificacion(int idDosificacion, double numeroAutorizacion, DateTime fechaLimite, string llaveDocificada, double nroInicial, double nroFinal, string segundaLeyenda, string actividadEconomica, string sucursal, string sfc)
        {
            this.idDosificacion = idDosificacion;
            this.numeroAutorizacion = numeroAutorizacion;
            this.fechaLimite = fechaLimite;
            this.llaveDocificada = llaveDocificada;
            this.nroInicial = nroInicial;
            this.nroFinal = nroFinal;
            this.segundaLeyenda = segundaLeyenda;
            this.actividadEconomica = actividadEconomica;
            this.sucursal = sucursal;
            this.sfc = sfc;
        }
        public Dosificacion( double numeroAutorizacion, DateTime fechaLimite, string llaveDocificada , double nroInicial, double nroFinal, string segundaLeyenda, string actividadEconomica, string sucursal , string sfc)
        {
            this.numeroAutorizacion = numeroAutorizacion;
            this.fechaLimite = fechaLimite;
            this.llaveDocificada = llaveDocificada;
            this.nroInicial = nroInicial;
            this.nroFinal = nroFinal;
            this.segundaLeyenda = segundaLeyenda;
            this.actividadEconomica = actividadEconomica;
            this.sucursal = sucursal;
            this.sfc = sfc;
        }
        public Dosificacion()
        {
                
        }
    }
}
