﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Tratamiento
    {
        #region atributos
        private int idTratamiento;
        private string nombreTratamiento;
        private string descripcion;
        private decimal precio;
        private byte estado;
        private DateTime fUpdate;
        #endregion
        #region propiedades
        public DateTime FUpdate
        {
            get { return fUpdate; }
            set { fUpdate = value; }
        }

        public byte Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        public decimal Precio
        {
            get { return precio; }
            set { precio = value; }
        } 

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        public string NombreTratamiento
        {
            get { return nombreTratamiento; }
            set { nombreTratamiento = value; }
        }

        public int IdTratamiento
        {
            get { return idTratamiento; }
            set { idTratamiento = value; }
        }
        #endregion

        #region Constructores

        public Tratamiento(int idTratamiento,string nombreTratamiento, string descripcion,decimal precio, byte estado, DateTime fUpdate)
        {
           this.idTratamiento=idTratamiento;
           this.nombreTratamiento=nombreTratamiento;
           this.descripcion=descripcion;
           this.precio=precio;
           this.estado=estado;
           this.fUpdate=fUpdate;
        }
        public Tratamiento(int idTratamiento, string nombreTratamiento, decimal precio)
        {
            this.idTratamiento = idTratamiento;
            this.nombreTratamiento = nombreTratamiento;
            this.precio = precio;
         
        }
        public Tratamiento(string nombreTratamiento,string descripcion, decimal precio)
        {
           this.nombreTratamiento=nombreTratamiento;
           this.descripcion=descripcion;
           this.precio=precio;
        }
        public Tratamiento()
        {

        }
        #endregion
    }
}
