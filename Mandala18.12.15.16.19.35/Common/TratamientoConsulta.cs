﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class TratamientoConsulta
    {
        private int idTratamiento;
        private int idConsulta;
        private int cantidadDeSesiones;
        private double precio;

public double Precio
{
  get { return precio; }
  set { precio = value; }
}



        public int CantidadDeSesiones
        {
            get { return cantidadDeSesiones; }
            set { cantidadDeSesiones = value; }
        }

        public int IdConsulta
        {
            get { return idConsulta; }
            set { idConsulta = value; }
        }

        public int IdTratamiento
        {
            get { return idTratamiento; }
            set { idTratamiento = value; }
        }
        public TratamientoConsulta( int idTratamiento,int idConsulta, int cantidadDeSesiones, double precio)
        {
             this.idTratamiento=idTratamiento;
             this.idConsulta=idConsulta;
             this.precio = precio;
             this.cantidadDeSesiones=cantidadDeSesiones;
        }
        public TratamientoConsulta()
        {

        }
    }
}
