﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Validacion
    {
        public static bool onlyLettersAndSpaces(string cad)
        {
            for (int i = 0; i < cad.Length; i++)
            {
                if (!char.IsLetter(cad[i])&&cad[i]!=' ')
                {
                    return false;
                }
            }
            return true;
        }

        public static bool ValidarCorreo(string correo)
        {
            if (correo.Contains("@"))
            {
                if (correo.Contains("."))
                {
                    if (correo.Substring(correo.Length - 3) == "com")
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
