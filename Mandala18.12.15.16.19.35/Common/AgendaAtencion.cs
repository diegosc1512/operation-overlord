﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class AgendaAtencion
    {
        #region atributos 
        private int idAgenda;
        private DateTime dia;
        private DateTime fUpdate;
        private int idReserva;
        private DateTime horaInicio;
        private DateTime horaFin;
        private char estado;
        #endregion
        #region Propiedades
        public int IdAgenda
        {
            get { return idAgenda; }
            set { idAgenda = value; }
        }

        public DateTime Dia
        {
            get { return dia; }
            set { dia = value; }
        }

        public DateTime HoraInicio
        {
            get { return horaInicio; }
            set { horaInicio = value; }
        }

        public DateTime HoraFin
        {
            get { return horaFin; }
            set { horaFin = value; }
        }

        public char Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        public DateTime FUpdate
        {
            get { return fUpdate; }
            set { fUpdate = value; }
        }

        public int IdReserva
        {
            get { return idReserva; }
            set { idReserva = value; }
        }
        #endregion
        #region Constructores
        public AgendaAtencion(int idAgenda,DateTime  dia, DateTime horaInicio, DateTime horaFin, DateTime fUpdate,char estado,int idReserva)
        {
             this.idAgenda=idAgenda;
             this.dia=dia;
             this.horaInicio=horaInicio;
             this.horaFin=horaFin;
             this.fUpdate=fUpdate;
             this.estado=estado;
             this.idReserva=idReserva;
        }
        public AgendaAtencion(DateTime dia, DateTime horaInicio, DateTime horaFin)
        {
            this.dia = dia;
            this.horaInicio = horaInicio;
            this.horaFin = horaFin;
        }
        public AgendaAtencion()
        {

        }
        #endregion
    }
}
