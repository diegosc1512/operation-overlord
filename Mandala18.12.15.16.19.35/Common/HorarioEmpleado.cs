﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class HorarioEmpleado
    {
        #region atributos
        private int idHorario;
        private char lunes;
        private char martes;
        private char miercoles;
        private char jueves;
        private char viernes;
        private char sabado;
        private char domingo;
        private DateTime horaInicio;
        private DateTime horaFin;
        private int idEmpleado;
        private DateTime fUpdate;
        private byte estado;
        private List<Empleado> empleados;
        #endregion
        #region propiedades
        public List<Empleado> Empleados
        {
            get { return empleados; }
            set { empleados = value; }
        }
        
        public byte Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        public DateTime FUpdate
        {
            get { return fUpdate; }
            set { fUpdate = value; }
        }
    
        public int IdEmpleado
        {
            get { return idEmpleado; }
            set { idEmpleado = value; }
        }
      
        public DateTime HoraFin
        {
            get { return horaFin; }
            set { horaFin = value; }
        }

        public DateTime HoraInicio
        {
            get { return horaInicio; }
            set { horaInicio = value; }
        }

        public char Domingo
        {
            get { return domingo; }
            set { domingo = value; }
        }

        public char Sabado
        {
            get { return sabado; }
            set { sabado = value; }
        }
        public char Viernes
        {
            get { return viernes; }
            set { viernes = value; }
        }
 
        public char Jueves
        {
            get { return jueves; }
            set { jueves = value; }
        }
      
        public char Miercoles
        {
            get { return miercoles; }
            set { miercoles = value; }
        }
     
        public char Martes
        {
            get { return martes; }
            set { martes = value; }
        }
      
        public char Lunes
        {
            get { return lunes; }
            set { lunes = value; }
        }
    
        public int IdHorario
        {
            get { return idHorario; }
            set { idHorario = value; }
        }
        #endregion
        #region constructores
        /// <summary>
        /// constructor con todos los atributos
        /// </summary>
        /// <param name="idHorario"></param>
        /// <param name="lunes"></param>
        /// <param name="martes"></param>
        /// <param name="miercoles"></param>
        /// <param name="jueves"></param>
        /// <param name="viernes"></param>
        /// <param name="sabado"></param>
        /// <param name="domingo"></param>
        /// <param name="horaInicio"></param>
        /// <param name="horaFin"></param>
        /// <param name="idEmpleado"></param>
        /// <param name="fUpdate"></param>
        /// <param name="estado"></param>
        public HorarioEmpleado(int idHorario,char lunes, char martes,char miercoles, char jueves, char viernes, char sabado, char domingo, DateTime horaInicio, DateTime horaFin,  DateTime fUpdate,byte estado)
        {
           this.idHorario = idHorario;
           this.lunes=lunes;
           this.martes=martes;
           this.miercoles=miercoles;
           this.jueves=jueves;
           this.viernes=viernes;
           this.sabado=sabado;
           this.domingo=domingo;
           this.horaInicio=horaInicio;
           this.horaFin=horaFin;
           this.fUpdate = fUpdate;
           this.estado = estado;
        }
        /// <summary>
        /// constructor para el inseret
        /// </summary>
        /// <param name="lunes"></param>
        /// <param name="martes"></param>
        /// <param name="miercoles"></param>
        /// <param name="jueves"></param>
        /// <param name="viernes"></param>
        /// <param name="sabado"></param>
        /// <param name="domingo"></param>
        /// <param name="horaInicio"></param>
        /// <param name="horaFin"></param>
        /// <param name="empleados"></param>
        public HorarioEmpleado(char lunes, char martes, char miercoles, char jueves, char viernes, char sabado, char domingo, DateTime horaInicio, DateTime horaFin, List<Empleado> empleados)
        {
            this.lunes = lunes;
            this.martes = martes;
            this.miercoles = miercoles;
            this.jueves = jueves;
            this.viernes = viernes;
            this.sabado = sabado;
            this.domingo = domingo;
            this.horaInicio = horaInicio;
            this.horaFin = horaFin;
            this.empleados = empleados;
        }
        /// <summary>
        /// COnstructor parar el update
        /// </summary>
        /// <param name="lunes"></param>
        /// <param name="martes"></param>
        /// <param name="miercoles"></param>
        /// <param name="jueves"></param>
        /// <param name="viernes"></param>
        /// <param name="sabado"></param>
        /// <param name="domingo"></param>
        /// <param name="horaInicio"></param>
        /// <param name="horaFin"></param>
        public HorarioEmpleado(int idHorario,char lunes, char martes, char miercoles, char jueves, char viernes, char sabado, char domingo, DateTime horaInicio, DateTime horaFin,List<Empleado> empleados)
        {
            this.idHorario = idHorario;
            this.lunes = lunes;
            this.martes = martes;
            this.miercoles = miercoles;
            this.jueves = jueves;
            this.viernes = viernes;
            this.sabado = sabado;
            this.domingo = domingo;
            this.horaInicio = horaInicio;
            this.horaFin = horaFin;
            this.empleados = empleados;
        }

        public HorarioEmpleado(int idHorario, char lunes, char martes, char miercoles, char jueves, char viernes, char sabado, char domingo, DateTime horaInicio, DateTime horaFin)
        {
            this.idHorario = idHorario;
            this.lunes = lunes;
            this.martes = martes;
            this.miercoles = miercoles;
            this.jueves = jueves;
            this.viernes = viernes;
            this.sabado = sabado;
            this.domingo = domingo;
            this.horaInicio = horaInicio;
            this.horaFin = horaFin;
        }
        /// <summary>
        /// constructor por defecto
        /// </summary>
        public HorarioEmpleado()
        {

        }
        #endregion

    }
}
