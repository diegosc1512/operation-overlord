﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Factura
    {
        #region atributos
        private int idFactura;
        private int numeroFactura;
        private DateTime fecha;
        private double total;
        private string nit;
        private string razonSocial;
        private string codigoControl;
        private char estado;
        private int idConsulta;
        private int idDosificacion;
        private double numeroAutorizacion;
        private double importeTotal;
        private double subTotal;
        private double descuento;
        private double creditoFiscal;
        #endregion
        #region propiedades
        public double CreditoFiscal
        {
            get { return creditoFiscal; }
            set { creditoFiscal = value; }
        }


        public double Descuento
        {
            get { return descuento; }
            set { descuento = value; }
        }


        public double SubTotal
        {
            get { return subTotal; }
            set { subTotal = value; }
        }

        public double NumeroAutorizacion
        {
            get { return numeroAutorizacion; }
            set { numeroAutorizacion = value; }
        }


        
        public int IdDosificacion
        {
            get { return idDosificacion; }
            set { idDosificacion = value; }
        }

        public int IdConsulta
        {
            get { return idConsulta; }
            set { idConsulta = value; }
        }

        public char Estado
        {
            get { return estado; }
            set { estado = value; }
        }


        public string CodigoControl
        {
            get { return codigoControl; }
            set { codigoControl = value; }
        }

        public string RazonSocial
        {
            get { return razonSocial; }
            set { razonSocial = value; }
        }

        public string Nit
        {
            get { return nit; }
            set { nit = value; }
        }

        public double Total
        {
            get { return total; }
            set { total = value; }
        }

        public DateTime Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }

        public int NumeroFactura
        {
            get { return numeroFactura; }
            set { numeroFactura = value; }
        }


        public int IdFactura
        {
            get { return idFactura; }
            set { idFactura = value; }
        }
        #endregion

        #region constructores
        public Factura(int idFactura, int numeroFactura, DateTime fecha, double total, string nit, string razonSocial, string codigoControl, char estado, int idConsulta, int idDosificacion, double numeroAutorizacion, double importeTotal, double subTotal, double descuento , double creditoFiscal)
        {
            this.idFactura=idFactura;
            this.numeroFactura=numeroFactura;
            this.fecha=fecha;
            this.total=total;
            this.nit=nit;
            this.razonSocial=razonSocial;
            this.codigoControl=codigoControl;
            this.estado=estado;
            this.idConsulta=idConsulta;
            this.idDosificacion = idDosificacion;
            this.numeroAutorizacion=numeroAutorizacion;
            this.importeTotal=importeTotal;
            this.subTotal=subTotal;
            this.descuento=descuento;
            this.creditoFiscal=creditoFiscal;
        }
        public Factura(int numeroFactura, DateTime fecha, double total, string nit, string razonSocial, string codigoControl, char estado , double numeroAutorizacion, double importeTotal, double subTotal, double descuento, double creditoFiscal)
        {
            this.numeroFactura = numeroFactura;
            this.fecha = fecha;
            this.total = total;
            this.nit = nit;
            this.razonSocial = razonSocial;
            this.codigoControl = codigoControl;
            this.estado = estado;
            this.numeroAutorizacion = numeroAutorizacion;
            this.importeTotal = importeTotal;
            this.subTotal = subTotal;
            this.descuento = descuento;
            this.creditoFiscal = creditoFiscal;
        }
        public Factura()
        {

        }
        #endregion
    }
}
