﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class AgendaNoAtencionGeneral
    {
        #region atributos
        private int idAgenda;
        private DateTime horaInicio;
        private DateTime horaFin;
        private DateTime fecha;
        private string motivo;
        private DateTime fUpdate;
        private byte estado;
        #endregion
        #region propiedades
        public byte Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        public DateTime FUpdate
        {
            get { return fUpdate; }
            set { fUpdate = value; }
        }

        public string Motivo
        {
            get { return motivo; }
            set { motivo = value; }
        }

        public DateTime Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }

        public DateTime HoraFin
        {
            get { return horaFin; }
            set { horaFin = value; }
        }

        public DateTime HoraInicio
        {
            get { return horaInicio; }
            set { horaInicio = value; }
        }


        public int IdAgenda
        {
            get { return idAgenda; }
            set { idAgenda = value; }
        }
        #endregion
        #region constructores
        public AgendaNoAtencionGeneral(int idAgenda, DateTime horaInicio,DateTime horaFin,DateTime fecha, string motivo,DateTime fUpdate, byte estado)
        {
           this.idAgenda=idAgenda;
           this.horaInicio=horaInicio;
           this.horaFin=horaFin;
           this.fecha=fecha;
           this.motivo=motivo;
           this.fUpdate = fUpdate;
           this.estado = estado;
        }
        public AgendaNoAtencionGeneral( DateTime horaInicio, DateTime horaFin, DateTime fecha, string motivo)
        {
            this.horaInicio = horaInicio;
            this.horaFin = horaFin;
            this.fecha = fecha;
            this.motivo = motivo;
        }
        public AgendaNoAtencionGeneral()
        {

        }
        #endregion
    }
}
