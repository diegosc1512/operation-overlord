﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Persona
    {
        #region atributos
        private int idPersona;
        private string nombres;
        private string primerApellido;
        private string segundoApellido;
        private char sexo;
        private DateTime fechaNacimiento;
        private string documento;
        private DateTime fUpdate;
        private byte estado;
        private int idUsuario;
        private List<Telefono> telefonos;
        #endregion
        #region propiedades
        public List<Telefono> Telefonos
        {
            get { return telefonos; }
            set { telefonos = value; }
        }

        public int IdUsuario
        {
            get { return idUsuario; }
            set { idUsuario = value; }
        }
      
        public byte Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        public DateTime FUpdate
        {
            get { return fUpdate; }
            set { fUpdate = value; }
        }

        public string Documento
        {
            get { return documento; }
            set { documento = value; }
        }

        public DateTime FechaNacimiento
        {
            get { return fechaNacimiento; }
            set { fechaNacimiento = value; }
        }

        public char Sexo
        {
            get { return sexo; }
            set { sexo = value; }
        }

        public string SegundoApellido
        {
            get { return segundoApellido; }
            set { segundoApellido = value; }
        }

        public string PrimerApellido
        {
          get { return primerApellido; }
          set { primerApellido = value; }
        }


        public string Nombres
        {
            get { return nombres; }
            set { nombres = value; }
        }

        public int IdPersona
        {
            get { return idPersona; }
            set { idPersona = value; }
        }
        #endregion
        #region constructores
        public Persona(int idPersona, string nombres, string primerApellido, string segundoApellido,char sexo,DateTime fechaNacimiento,string documento,DateTime fUpdate, byte estado, int idUsuario, List<Telefono> telefonos)
        {
            this.idPersona=idPersona;
            this.nombres=nombres;
            this.primerApellido=primerApellido;
            this.segundoApellido=segundoApellido;
            this.sexo=sexo;
            this.fechaNacimiento=fechaNacimiento;
            this.documento=documento;
            this.fUpdate=fUpdate;
            this.estado=estado;
            this.idUsuario = idUsuario;
            this.telefonos = telefonos;
        }
        public Persona(int idPersona, string nombres, string primerApellido, string segundoApellido, char sexo, DateTime fechaNacimiento, string documento, DateTime fUpdate, byte estado)
        {
            this.idPersona = idPersona;
            this.nombres = nombres;
            this.primerApellido = primerApellido;
            this.segundoApellido = segundoApellido;
            this.sexo = sexo;
            this.fechaNacimiento = fechaNacimiento;
            this.documento = documento;
            this.fUpdate = fUpdate;
            this.estado = estado;
           
        }
        public Persona(string nombres, string primerApellido, string segundoApellido, char sexo, DateTime fechaNacimiento, string documento, List<Telefono> telefonos)
        {
            this.nombres = nombres;
            this.primerApellido = primerApellido;
            this.segundoApellido = segundoApellido;
            this.sexo = sexo;
            this.fechaNacimiento = fechaNacimiento;
            this.documento = documento;
            this.telefonos = telefonos;
        }
        public Persona()
        {

        }
        #endregion
    }
}
