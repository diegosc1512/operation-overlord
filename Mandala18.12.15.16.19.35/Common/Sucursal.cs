﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
     public class Sucursal
    {
        #region atributos
        private int idSucursal;
        private string nombreSucursal;
        private double latitud;
        private double longitud;
        private string direccion;
        private List<Empleado> empleados;
        #endregion

        #region propiedades
        public List<Empleado> Empleados
        {
            get { return empleados; }
            set { empleados = value; }
        }

        public string Direccion
        {
            get { return direccion; }
            set { direccion = value; }
        }
        private byte estado;
        private DateTime fUpdate;
        
        public DateTime FUpdate
        {
            get { return fUpdate; }
            set { fUpdate = value; }
        }

        public byte Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        public double Longitud
        {
            get { return longitud; }
            set { longitud = value; }
        } 

        public double Latitud
        {
            get { return latitud; }
            set { latitud = value; }
        }

        public string NombreSucursal
        {
            get { return nombreSucursal; }
            set { nombreSucursal = value; }
        }

        public int IdSucursal
        {
            get { return idSucursal; }
            set { idSucursal = value; }
        }
        #endregion

        #region Constructores

        public Sucursal(int idSucursal, string nombreSucursal,  double latitud, double longitud, string direccion, byte estado, DateTime fUpdate)
        {
           this.idSucursal=idSucursal;
           this.nombreSucursal=nombreSucursal;
           this.latitud= latitud;
           this.longitud= longitud;
           this.direccion = direccion;
           this.estado= estado;
           this.fUpdate= fUpdate;
        }
        public Sucursal(int idSucursal, string nombreSucursal, double latitud, double longitud, string direccion, byte estado, DateTime fUpdate, List<Empleado> empleados)
        {
            this.idSucursal = idSucursal;
            this.nombreSucursal = nombreSucursal;
            this.latitud = latitud;
            this.longitud = longitud;
            this.direccion = direccion;
            this.estado = estado;
            this.fUpdate = fUpdate;
            this.empleados = empleados;
        }
        public Sucursal(string nombreSucursal, double latitud, double longitud, string direccion, List<Empleado> empleados)
        {
            this.nombreSucursal = nombreSucursal;
            this.latitud = latitud;
            this.longitud = longitud;
            this.direccion = direccion;
            this.empleados = empleados;
        }
        public Sucursal(string nombreSucursal,double latitud, double longitud, string direccion)
        {
           this.nombreSucursal=nombreSucursal;
           this.latitud=latitud;
           this.longitud=longitud;
           this.direccion = direccion;
        }
        public Sucursal()
        {

        }
        #endregion
    }
}
