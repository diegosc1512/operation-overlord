﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
   public  class Usuario
   {
       #region atributos 
       private int idUsuario;
       private string nombreUsuario;
       private string password;
       private string rol;
       private byte estado;
       private int idPersona;
       private DateTime fUpdate;
       private string email;

       #endregion
       #region propiedades
       public string Email
       {
           get { return email; }
           set { email = value; }
       }
      

       public DateTime FUpdate
       {
           get { return fUpdate; }
           set { fUpdate = value; }
       }

       public int IdPersona
       {
           get { return idPersona; }
           set { idPersona = value; }
       }


       public byte Estado
       {
           get { return estado; }
           set { estado = value; }
       }

       public string Rol
       {
           get { return rol; }
           set { rol = value; }
       }

       public string Password
       {
           get { return password; }
           set { password = value; }
       }

       public string NombreUsuario
       {
           get { return nombreUsuario; }
           set { nombreUsuario = value; }
       }
       public int IdUsuario
       {
           get { return idUsuario; }
           set { idUsuario = value; }
       }
       #endregion
       #region constructores
       public Usuario(int idUsuario, string nombreUsuario, string password,string rol, string email, byte estado, int idPersona,DateTime fUpdate)
       {
           this.idUsuario=idUsuario;
           this.nombreUsuario=nombreUsuario;
           this.password=password;
           this.rol=rol;
           this.estado=estado;
           this.idPersona=idPersona;
           this.fUpdate=fUpdate;
           this.email = email;
       }
       public Usuario(string nombreUsuario, string password,string rol, string email)
       {
           this.nombreUsuario = nombreUsuario;
           this.password = password;
           this.rol = rol;
           this.email = email;
       }
       public Usuario(int idPersona,string nombreUsuario, string password, string rol, string email)
       {
           this.idPersona = idPersona;
           this.nombreUsuario = nombreUsuario;
           this.password = password;
           this.rol = rol;
           this.email = email;
       }
       public Usuario()
       {

       }
       #endregion
   }
}
