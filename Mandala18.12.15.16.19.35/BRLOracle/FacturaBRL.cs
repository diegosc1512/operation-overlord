﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DALOracle;
using System.Data;

namespace BRLOracle
{
   public  class FacturaBRL
    {
               #region atributos, propiedades y constructores
        private Factura fac;
        private FacturaDAL dal;

        public FacturaDAL Dal
        {
            get { return dal; }
            set { dal = value; }
        }
        public FacturaBRL()
        {

        }
        public FacturaBRL(Factura fac)
        {
            this.fac = fac;
            dal = new FacturaDAL(fac);
        }

        public Factura Fac
        {
            get { return fac; }
            set { fac = value; }
        }



        #endregion
        #region metodos de la clase
        public  void Insert()
        {
            dal.Insert();
        }

        public  void Update()
        {
            dal.Update();
        }

        public  void Delete()
        {
            dal.Delete();
        }
        public void Delete(int id)
        {
            dal.Delete(id);
        }

        public  DataTable Select()
        {
            dal = new FacturaDAL();
            return dal.Select();
        }
       /// <summary>
       /// con este metodo se oibtiene el constructor de una factura
       /// </summary>
       /// <param name="idFactura"></param>
       /// <returns></returns>
        public Factura Get(int idFactura)
        {
            dal = new FacturaDAL();
            return dal.Get(idFactura);
        }
        public bool Existe(string nombre)
        {
            return dal.Existe(nombre);
        }
        #endregion
    }

}
