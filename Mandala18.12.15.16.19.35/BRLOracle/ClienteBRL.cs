﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DALOracle;
using System.Data;

namespace BRLOracle
{
    public class ClienteBRL
    {
        #region atributos y Constructores
        private Persona pers;
        private Cliente clie;
        private Usuario usu;
        private ClienteDAL dal;

        public Usuario Usu
        {
            get { return usu; }
            set { usu = value; }
        }

        public ClienteDAL Dal
        {
            get { return dal; }
            set { dal = value; }
        }

        public Cliente Clie
        {
            get { return clie; }
            set { clie = value; }
        }

        public Persona Pers
        {
            get { return pers; }
            set { pers = value; }
        }
        public ClienteBRL()
        {

        }
        public ClienteBRL(Persona pers, Cliente clie, Usuario usu)
        {
            this.usu = usu;
            this.pers = pers;
            this.clie = clie;
            dal = new ClienteDAL(pers, clie,usu);
        }

        #endregion
        public void Insert()
        {
            dal.Insert();
        }

        public void Update()
        {
            dal.Update();

        }

        public void Delete(int id)
        {
            dal.Delete(id);
        }
        //public override void Delete()
        //{
        //}
        public  DataTable Select()
        {
            dal = new ClienteDAL();
            return dal.Select();
        }
        public Persona Get(int idPersona)
        {
            dal = new ClienteDAL();
            return dal.Get(idPersona);
        }
        public Cliente Get2(int idCliente)
        {
            dal = new ClienteDAL();
            return dal.Get2(idCliente);
        }
        public Usuario Get3(int idUsuario)
        {
            dal = new ClienteDAL();
            return dal.Get3(idUsuario);
        }
        public DataTable GetTelefono(int idCliente)
        {
            dal = new ClienteDAL();
            return dal.GetTelefono(idCliente);
        }
        public Telefono GetUnTelefono(int idTelefono)
        {
            dal = new ClienteDAL();
            return dal.GetUnTelefono(idTelefono);
        }
        public bool Existe(string nombre)
        {
            return dal.Existe(nombre);
        }
    }
}
