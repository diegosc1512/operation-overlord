﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALOracle;
using Common;
using System.Data;

namespace BRLOracle
{
    public class EmpleadoBRL
    {
        #region atributos y Constructores
        private Persona pers;
        private Empleado emp;
        private EmpleadoDAL dal;
        private Usuario usu;

        public Usuario Usu
        {
            get { return usu; }
            set { usu = value; }
        }

        public EmpleadoDAL Dal
        {
            get { return dal; }
            set { dal = value; }
        }

        public Empleado Emp
        {
            get { return emp; }
            set { emp = value; }
        }

        public Persona Pers
        {
            get { return pers; }
            set { pers = value; }
        }
        public EmpleadoBRL()
        {

        }
        public EmpleadoBRL(Persona pers, Empleado emp, Usuario usu)
        {
            this.pers = pers;
            this.emp = emp;
            this.usu = usu;
            dal = new EmpleadoDAL(pers, emp,usu);
        }

        #endregion
        public  void Insert()
        {
            dal.Insert();
        }

        public  void Update()
        {
            dal.Update();
        }

        public  void Delete()
        {

        }
        public void Delete(int id)
        {
            dal.Delete(id);
        }

        public  DataTable Select()
        {
            dal = new EmpleadoDAL();
            return dal.Select();
        }

        public Persona Get(int idPersona)
        {
            dal = new EmpleadoDAL();
            return dal.Get(idPersona);
        }
        public Empleado Get2(int idEmpleado)
        {
            dal = new EmpleadoDAL();
            return dal.Get2(idEmpleado);
        }
        public Usuario Get3(int idUsuario)
        {
            dal = new EmpleadoDAL();
            return dal.Get3(idUsuario);
        }
        public DataTable GetTelefono(int idEmpleado)
        {
            dal = new EmpleadoDAL();
            return dal.GetTelefono(idEmpleado);
        }
        public Telefono GetUnTelefono(int idTelefono)
        {
            dal = new EmpleadoDAL();
            return dal.GetUnTelefono(idTelefono);
        }
        public bool Existe(string nombre)
        {
            return dal.Existe(nombre);
        }

        public DataTable GetHorarios(int id)
        {
            dal = new EmpleadoDAL();
            return dal.GetHorarios(id);
        }

        public DataTable SelectHorarios()
        {
            dal = new EmpleadoDAL();
            return dal.SelectHorarios();
        }
    }
}
