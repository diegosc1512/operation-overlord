﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALOracle;
using Common;
using System.Data;

namespace BRLOracle
{
    public class ConsultaBRL
    {
        #region atributos, propiedades y constructores
        private Consulta consulta;

        public Consulta Consulta
        {
            get { return consulta; }
            set { consulta = value; }
        }
        private ConsultaDAL dal;


        public ConsultaDAL Dal
        {
            get { return dal; }
            set { dal = value; }
        }




        public ConsultaBRL()
        {

        }
        public ConsultaBRL(Consulta consulta, List<TratamientoConsulta> trat)
        {
            //this.cons = cons;
            dal = new ConsultaDAL(consulta, trat);
        }
        #endregion
        public  void Insert()
        {
           // dal = new ConsultaDAL();
            dal.Insert();
        }
        //public void Insert(Consulta cons, List<TratamientoConsulta> lisTrat)
        //{
        //    dal = new ConsultaDAL();
        //    dal.Insert(cons, lisTrat);
        //}
        public  void Update()
        {
            throw new NotImplementedException();
        }

        public  void Delete()
        {
            throw new NotImplementedException();
        }

        public  DataTable Select()
        {
            throw new NotImplementedException();
        }
        public DataTable SelectCliente()
        {
            dal = new ConsultaDAL();
            return dal.SelectCliente();
        }
        public DataTable SelectLikeCiFullNameCliente(byte opcionLike, string texto)
        {
            dal = new ConsultaDAL();
            return dal.SelectLikeCiFullNameCliente(opcionLike, texto);
        }
        public DataTable SelectLikeCiFullNameEmpleado(byte opcionLike, string texto)
        {
            dal = new ConsultaDAL();
            return dal.SelectLikeCiFullNameEmpleado(opcionLike, texto);
        }

        public DataTable SelectHorariosEmpleado(int id)
        {
            dal = new ConsultaDAL();
            return dal.SelectHorariosEmpleado(id);
        }

        public DataTable LoadDatagridTratamiento(string txttra)
        {
            dal = new ConsultaDAL();
            return dal.LoadDatagridTratamiento(txttra);
        }

        public DataTable SelectConsulta()
        {
            dal = new ConsultaDAL();
            return dal.SelectConsulta();
        }
    }
}
