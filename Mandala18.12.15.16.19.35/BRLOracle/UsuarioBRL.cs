﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALOracle;
using Common;
using System.Data;

namespace BRLOracle
{
    public class UsuarioBRL
    {
        #region atributos, propiedades y constructores
        UsuarioDAL dal;
        Usuario user;

        public Usuario User
        {
            get { return user; }
            set { user = value; }
        }

        public UsuarioDAL Dal
        {
            get { return dal; }
            set { dal = value; }
        }

        public UsuarioBRL()
        {

        }
        public UsuarioBRL(Usuario user)
        {
            this.user = user;
            dal = new UsuarioDAL(user);
        }
        #endregion
        #region metodos

        public  void Insert()
        {
            dal.Insert();
        }

        public  void Update()
        {
            dal.Update();
        }
        public DataTable Login(string usuario, string password)
        {
            dal = new UsuarioDAL();
            return dal.Login(usuario, password);
        }

        #endregion
    }
}
