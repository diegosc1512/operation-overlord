﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DALOracle;
using System.Data;

namespace BRLOracle
{
    public class TratamientoBRL
    {
        #region atributos, propiedades y constructores
        private Tratamiento trat;
        private TratamientoDAL dal;

        public TratamientoDAL Dal
        {
            get { return dal; }
            set { dal = value; }
        }
        public TratamientoBRL()
        {

        }
        public TratamientoBRL(Tratamiento trat)
        {
            this.trat = trat;
            dal = new TratamientoDAL(trat);
        }

        public Tratamiento Trat
        {
            get { return trat; }
            set { trat = value; }
        }



        #endregion
        #region metodos de la clase
        public void Insert()
        {
            dal.Insert();
        }

        public void Update()
        {
            dal.Update();
        }

        public  void Delete()
        {
            dal.Delete();
        }
        public void Delete(int id)
        {
            dal.Delete(id);
        }

        public  DataTable Select()
        {
            dal = new TratamientoDAL();
            return dal.Select();
        }
        public Tratamiento Get(int idTatamiento)
        {
            dal = new TratamientoDAL();
            return dal.Get(idTatamiento);
        }
        public bool Existe(string nombre)
        {
            return dal.Existe(nombre);
        }
        #endregion
    }
}
