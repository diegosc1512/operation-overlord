﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALOracle;
using Common;
using System.Data;

namespace BRLOracle
{
    public class HorarioEmpleadoBRL
    {
                #region atributos y Constructores
        private HorarioEmpleado horario;
        private DALHorario dal;
       // private List<Empleado> empleados;
        public DALHorario Dal
        {
            get { return dal; }
            set { dal = value; }
        }

      

        public HorarioEmpleado Horario
        {
            get { return horario; }
            set { horario = value; }
        }
        public HorarioEmpleadoBRL()
        {

        }
        public HorarioEmpleadoBRL( HorarioEmpleado horario)
        {
            this.horario = horario;
           // this.emp = emp;
            dal = new DALHorario(horario);
        }

        #endregion
        public  void Insert()
        {
            dal.Insert();
        }

        public  void Update()
        {
            dal.Update();
        }

        public  void Delete()
        {

        }
        public void Delete(int id)
        {
            dal.Delete(id);
        }

        public  DataTable Select()
        {
            dal = new DALHorario();
            return dal.Select();
        }
        public DataTable SelectEmpleados()
        {
            dal = new DALHorario();
            return dal.SelectEmpleados();
        }

        public HorarioEmpleado Get(int idHorario)
        {
            dal = new DALHorario();
            return dal.Get(idHorario);
        }
        public DataTable SelectLikeCiFullName(byte opcionLike, string texto)
        {
            dal = new DALHorario();
            return dal.SelectLikeCiFullName(opcionLike, texto);
        }

        public DataTable getEmpleados(int idHorario)
        {
            dal = new DALHorario();
            return dal.GetEmpleados(idHorario);
        }
    }
}
