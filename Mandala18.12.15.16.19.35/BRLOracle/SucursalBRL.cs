﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DALOracle;
using System.Data;

namespace BRLOracle
{
    public class SucursalBRL
    {
        #region atributos, propiedades y constructores
        private Sucursal suc;
        private SucursalDAL dal;

        public SucursalDAL Dal
        {
            get { return dal; }
            set { dal = value; }
        }
        public SucursalBRL()
        {

        }
        public SucursalBRL(Sucursal suc)
        {
            this.suc = suc;
            dal = new SucursalDAL(suc);
        }

        public Sucursal Suc
        {
            get { return suc; }
            set { suc = value; }
        }



        #endregion
        #region metodos de la clase
        public  void Insert()
        {
            dal.Insert();
        }

        public  void Update()
        {
            dal.Update();
        }

        public  void Delete()
        {
            dal.Delete();
        }
        public void Delete(int id)
        {
            dal.Delete(id);
        }

        public  DataTable Select()
        {
            dal = new SucursalDAL();
            return dal.Select();
        }
        public Sucursal Get(int idSucursal)
        {
            dal = new SucursalDAL();
            return dal.Get(idSucursal);
        }
        public bool Existe(string nombre)
        {
            return dal.Existe(nombre);
        }
        public DataTable GetEmpleados(int idHorario)
        {
            dal = new SucursalDAL();
            return dal.GetEmpleados(idHorario);
        }
        public DataTable SelectEmpleados()
        {
            dal = new SucursalDAL();
            return dal.SelectEmpleados();
        }
        #endregion
    }
}
