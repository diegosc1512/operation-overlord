﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DALOracle;
using System.Data;

namespace BRLOracle
{
    public class DosificacionBRL
    {
        #region atributos, propiedades y constructores
        private Dosificacion dosi;
        private DosificacionDAL dal;

        public DosificacionDAL Dal
        {
            get { return dal; }
            set { dal = value; }
        }
       public DosificacionBRL ()
	{
            
	}
        public DosificacionBRL(Dosificacion dosi)
        {
            this.dosi = dosi;
            dal = new DosificacionDAL(dosi);
        }

        public Dosificacion Dosi
        {
            get { return dosi; }
            set { dosi = value; }
        }



        #endregion
        #region metodos de la clase
        public void Insert()
        {
            dal.Insert();
        }

        public void Update()
        {
            dal.Update();
        }

        public  void Delete()
        {
            dal.Delete();
        }
        public void Delete(int id)
        {
            dal.Delete(id);
        }

        public  DataTable Select()
        {
            dal = new DosificacionDAL();
            return dal.Select();
        }
        public Dosificacion Get(int idTatamiento)
        {
            dal = new DosificacionDAL();
            return dal.Get(idTatamiento);
        }
        public bool Existe(string nombre)
        {
            return dal.Existe(nombre);
        }
        #endregion
    }
}
