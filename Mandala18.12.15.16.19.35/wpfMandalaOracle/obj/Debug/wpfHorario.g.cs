﻿#pragma checksum "..\..\wpfHorario.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "A6A1CDEBAEEF73DA346C440A8658ADE00DC2B35E"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace wpfMandalaOracle {
    
    
    /// <summary>
    /// wpfHorario
    /// </summary>
    public partial class wpfHorario : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 38 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtId;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSalir;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GridMostrar;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GridMenu;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnOpenMenu;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCollapseMenu;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnInsertar;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnModificar;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnEliminar;
        
        #line default
        #line hidden
        
        
        #line 105 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGuardar;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancelar;
        
        #line default
        #line hidden
        
        
        #line 115 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgvDatos;
        
        #line default
        #line hidden
        
        
        #line 116 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblAnuncio;
        
        #line default
        #line hidden
        
        
        #line 119 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtHoraInicio;
        
        #line default
        #line hidden
        
        
        #line 120 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtHoraFin;
        
        #line default
        #line hidden
        
        
        #line 123 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox cbxLunes;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox cbxMartes;
        
        #line default
        #line hidden
        
        
        #line 125 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox cbxMiercoles;
        
        #line default
        #line hidden
        
        
        #line 126 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox cbxJueves;
        
        #line default
        #line hidden
        
        
        #line 127 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox cbxViernes;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox cbxSabado;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox cbxDomingo;
        
        #line default
        #line hidden
        
        
        #line 130 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtEmpleado;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgvEmpleados;
        
        #line default
        #line hidden
        
        
        #line 133 "..\..\wpfHorario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgvEmpleadosSeleccionados;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/wpfMandalaOracle;component/wpfhorario.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\wpfHorario.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 12 "..\..\wpfHorario.xaml"
            ((wpfMandalaOracle.wpfHorario)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.txtId = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.btnSalir = ((System.Windows.Controls.Button)(target));
            
            #line 51 "..\..\wpfHorario.xaml"
            this.btnSalir.Click += new System.Windows.RoutedEventHandler(this.BtnSalir_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.GridMostrar = ((System.Windows.Controls.Grid)(target));
            return;
            case 5:
            this.GridMenu = ((System.Windows.Controls.Grid)(target));
            return;
            case 6:
            this.btnOpenMenu = ((System.Windows.Controls.Button)(target));
            
            #line 86 "..\..\wpfHorario.xaml"
            this.btnOpenMenu.Click += new System.Windows.RoutedEventHandler(this.BtnOpenMenu_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.btnCollapseMenu = ((System.Windows.Controls.Button)(target));
            
            #line 89 "..\..\wpfHorario.xaml"
            this.btnCollapseMenu.Click += new System.Windows.RoutedEventHandler(this.BtnCollapseMenu_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btnInsertar = ((System.Windows.Controls.Button)(target));
            
            #line 95 "..\..\wpfHorario.xaml"
            this.btnInsertar.Click += new System.Windows.RoutedEventHandler(this.BtnInsertar_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.btnModificar = ((System.Windows.Controls.Button)(target));
            
            #line 98 "..\..\wpfHorario.xaml"
            this.btnModificar.Click += new System.Windows.RoutedEventHandler(this.btnModificar_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.btnEliminar = ((System.Windows.Controls.Button)(target));
            
            #line 101 "..\..\wpfHorario.xaml"
            this.btnEliminar.Click += new System.Windows.RoutedEventHandler(this.btnEliminar_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.btnGuardar = ((System.Windows.Controls.Button)(target));
            
            #line 105 "..\..\wpfHorario.xaml"
            this.btnGuardar.Click += new System.Windows.RoutedEventHandler(this.btnGuardar_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.btnCancelar = ((System.Windows.Controls.Button)(target));
            
            #line 108 "..\..\wpfHorario.xaml"
            this.btnCancelar.Click += new System.Windows.RoutedEventHandler(this.btnCancelar_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.dgvDatos = ((System.Windows.Controls.DataGrid)(target));
            
            #line 115 "..\..\wpfHorario.xaml"
            this.dgvDatos.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.dgvDatos_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 14:
            this.lblAnuncio = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.txtHoraInicio = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.txtHoraFin = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.cbxLunes = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 18:
            this.cbxMartes = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 19:
            this.cbxMiercoles = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 20:
            this.cbxJueves = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 21:
            this.cbxViernes = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 22:
            this.cbxSabado = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 23:
            this.cbxDomingo = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 24:
            this.txtEmpleado = ((System.Windows.Controls.TextBox)(target));
            
            #line 130 "..\..\wpfHorario.xaml"
            this.txtEmpleado.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txtEmpleado_TextChanged);
            
            #line default
            #line hidden
            return;
            case 25:
            this.dgvEmpleados = ((System.Windows.Controls.DataGrid)(target));
            
            #line 132 "..\..\wpfHorario.xaml"
            this.dgvEmpleados.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.dgvEmpleados_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 26:
            this.dgvEmpleadosSeleccionados = ((System.Windows.Controls.DataGrid)(target));
            
            #line 133 "..\..\wpfHorario.xaml"
            this.dgvEmpleadosSeleccionados.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.dgvEmpleadosSeleccionados_SelectionChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

