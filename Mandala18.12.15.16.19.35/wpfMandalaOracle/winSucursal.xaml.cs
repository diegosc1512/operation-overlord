﻿using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Common;
using BRLOracle;
using System.Data;

namespace wpfMandalaOracle
{
    /// <summary>
    /// Lógica de interacción para winSucursal.xaml
    /// </summary>
    public partial class winSucursal : Window
    {
        public winSucursal()
        {
            InitializeComponent();
        }
        Sucursal suc= new Sucursal();
        SucursalBRL brl;
        Location puntoUbicacion;
        byte operacion = 0;
        int idSeleccionado = 0;
        int idEmpleadoSeleccionado = 0;
        List<Empleado> empleados= new List<Empleado>();

        #region habilitar
        private void LoadDatagrid()
        {
            try
            {
                brl = new SucursalBRL();
                dgvDatos.ItemsSource = brl.Select().DefaultView;
                dgvDatos.Columns[0].Visibility = Visibility.Hidden;
                dgvDatos.Columns[2].Visibility = Visibility.Hidden;
                dgvDatos.Columns[3].Visibility = Visibility.Hidden;

                dgvEmpleados.ItemsSource = brl.SelectEmpleados().DefaultView;
                dgvEmpleados.Columns[0].Visibility = Visibility.Hidden;
                dgvEmpleados.Columns[2].Visibility = Visibility.Hidden;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        static DataTable ConvertToDataTable(List<Empleado> lista)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("IdEmpleado");
            dt.Columns.Add("Nombre Completo");
            dt.Columns.Add("Documento");

            foreach (var item in lista)
            {
                var row = dt.NewRow();
                row["IdEmpleado"] = item.IdEmpleado;
                row["Nombre Completo"] = item.NombreCompleto;
                row["Documento"] = item.Documento2;
                dt.Rows.Add(row);
            }
            return dt;
        }
        #endregion


        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnSatelite_Click(object sender, RoutedEventArgs e)
        {
            miMapa.Focus();
            miMapa.Mode = new AerialMode(true);


        }

        private void btnCalle_Click(object sender, RoutedEventArgs e)
        {
            miMapa.Focus();
            miMapa.Mode = new RoadMode();
        }

        private void btnAcercar_Click(object sender, RoutedEventArgs e)
        {
            miMapa.Focus();
            miMapa.ZoomLevel++;
        }

        private void btnAlejar_Click(object sender, RoutedEventArgs e)
        {
            miMapa.Focus();
            miMapa.ZoomLevel--;
        }

        private void miMapa_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            var mousePosicion = e.GetPosition((UIElement)sender);

             puntoUbicacion = miMapa.ViewportPointToLocation(mousePosicion);

            Pushpin marcador = new Pushpin();
            marcador.Location = puntoUbicacion;

            miMapa.Children.Clear();
            miMapa.Children.Add(marcador);

            //MessageBox.Show(puntoUbicacion.Latitude+"  "+puntoUbicacion.Longitude);

        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtNombre.Text != string.Empty && txtDireccion.Text != string.Empty)
                {
                    suc = new Sucursal(txtNombre.Text, puntoUbicacion.Latitude, puntoUbicacion.Longitude, txtDireccion.Text, empleados);
                    brl = new SucursalBRL(suc);
                    brl.Insert();

                    MessageBox.Show("Sucursal Registrada con éxito");
                }
                else
                {
                    MessageBox.Show("Debe llenar los campos");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            LoadDatagrid();

        }

        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void dgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvDatos.Items.Count > 0 && dgvDatos.SelectedItem != null)
            {
                try
                {
                    DataRowView dataRow = (DataRowView)dgvDatos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    idSeleccionado = id;
                    brl = new SucursalBRL();
                    suc = brl.Get(id);
                    txtNombre.Text = suc.NombreSucursal;
                    txtDireccion.Text = suc.Direccion;

                    Pushpin marcador = new Pushpin();
                    marcador.Location= new Location(suc.Latitud, suc.Longitud);

                    miMapa.Children.Clear();
                    miMapa.Children.Add(marcador);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadDatagrid();
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                suc = new Sucursal(txtNombre.Text, puntoUbicacion.Latitude, puntoUbicacion.Longitude, txtDireccion.Text);
                brl = new SucursalBRL(suc);
                brl.Update();

                MessageBox.Show("Sucursal atualizada con éxito");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            LoadDatagrid();
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            brl.Delete(idSeleccionado);
            LoadDatagrid();
        }

        private void dgvEmpleados_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvEmpleados.Items.Count > 0 && dgvEmpleados.SelectedItem != null)
            {
                try
                {

                    DataRowView dataRow = (DataRowView)dgvEmpleados.SelectedItem;
                    int idEmpleado = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    idEmpleadoSeleccionado = idEmpleado;
                    Empleado empl = new Empleado(int.Parse(dataRow.Row.ItemArray[0].ToString()), dataRow.Row.ItemArray[1].ToString(), dataRow.Row.ItemArray[2].ToString());
                    empleados.Add(empl);
                    dgvEmpleadosSeleccionados.ItemsSource = ConvertToDataTable(empleados).DefaultView;
                    dgvEmpleadosSeleccionados.Columns[0].Visibility = Visibility.Hidden;

                    try
                    {
                        brl = new SucursalBRL();
                        dgvDatos.ItemsSource = brl.Select().DefaultView;
                        dgvDatos.Columns[0].Visibility = Visibility.Hidden;
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void dgvEmpleadosSeleccionados_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

    }
}
