﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BRLOracle;
using Common;
using System.Data;

namespace wpfMandalaOracle
{
    /// <summary>
    /// Lógica de interacción para winDosificacion.xaml
    /// </summary>
    public partial class winDosificacion : Window
    {
        public winDosificacion()
        {
            InitializeComponent();
        }

        byte operacion = 0;
        DosificacionBRL brl;
        Dosificacion dosi;
        Sucursal suc;
        DataTable dt = new DataTable();
        string sucursal = "";


        #region Metodos
        private void Habilitar(byte operacion)
        {
            btnGuardar.IsEnabled = true;
            btnCancelar.IsEnabled = true;
            btnInsertar.IsEnabled = false;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;
            txtActividadEconomica.IsEnabled = true;
            txtLeyenda.IsEnabled = true;
            txtLlaveDosificacion.IsEnabled = true;
            txtNriInicial.IsEnabled = true;
            txtNroFinal.IsEnabled = true;
            txtNumeroAutorizacion.IsEnabled = true;
            txtSfc.IsEnabled = true;
            cmbSucursales.IsEnabled = true;
            this.operacion = operacion;
            lblAnuncio.Visibility = Visibility.Visible;
            lblAnuncio.Content = "";
            lblAnuncio.Content = "";
            dtFechaEmicionLimite.IsEnabled = true;
        }
        private void DesHabilitar()
        {
            btnGuardar.IsEnabled = false;
            btnCancelar.IsEnabled = false;
            btnInsertar.IsEnabled = true;
            btnModificar.IsEnabled = true;
            btnEliminar.IsEnabled = true;
            txtActividadEconomica.IsEnabled = false;
            txtLeyenda.IsEnabled = false;
            txtLlaveDosificacion.IsEnabled = false;
            txtNriInicial.IsEnabled = false;
            txtNroFinal.IsEnabled = false;
            txtNumeroAutorizacion.IsEnabled = false;
            txtSfc.IsEnabled = false;
            cmbSucursales.IsEnabled = false;
            txtActividadEconomica.Text = "";
            txtLeyenda.Text = "";
            txtLlaveDosificacion.Text = "";
            txtNriInicial.Text = "";
            txtNroFinal.Text = "";
            txtNumeroAutorizacion.Text = "";
            txtSfc.Text = "";
            dtFechaEmicionLimite.IsEnabled = false;
        }
        private void LoadDatagrid()
        {
            try
            {
                brl = new DosificacionBRL();
                dgvDatos.ItemsSource = brl.Select().DefaultView;
                dgvDatos.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        #endregion
        private void lbxNumeros_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(1);

            LoadDatagrid();
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(2);

            LoadDatagrid();
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {

            brl.Delete(Convert.ToInt32(txtId.Text));
            LoadDatagrid();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            switch (operacion)
            {
                case 1:

                    if (cmbSucursales.Text == "Casa Matriz")
                    {
                        sucursal = "Casa matriz";
                    }
                    else
                    {
                       sucursal= "Casa matriz";
                    }
                    dosi = new Dosificacion(double.Parse(txtNumeroAutorizacion.Text),DateTime.Parse(dtFechaEmicionLimite.SelectedDate.ToString()),txtLlaveDosificacion.Text,double.Parse(txtNriInicial.Text),double.Parse(txtNroFinal.Text),txtLeyenda.Text,txtActividadEconomica.Text,sucursal,txtSfc.Text);
                    brl = new DosificacionBRL(dosi);
                    if (brl.Existe(dosi.LlaveDocificada))
                    {
                        lblAnuncio.Content = "Dosificación duplicada";
                    }
                    else
                    {
                        brl.Insert();
                        lblAnuncio.Content = "Dosificación insertada con exito";
                    }
                    break;
                case 2:
                    if (cmbSucursales.Text == "Casa Matriz")
                    {
                        sucursal = "Casa matriz";
                    }
                    else
                    {
                        sucursal = "Casa matriz";
                    }
                    dosi = new Dosificacion(double.Parse(txtNumeroAutorizacion.Text), DateTime.Parse(dtFechaEmicionLimite.SelectedDate.ToString()), txtLlaveDosificacion.Text, double.Parse(txtNriInicial.Text), double.Parse(txtNroFinal.Text), txtLeyenda.Text, txtActividadEconomica.Text, sucursal, txtSfc.Text);
                    dosi.IdDosificacion = Convert.ToInt32(txtId.Text);
                    brl = new DosificacionBRL(dosi);
                    brl.Update();
                    lblAnuncio.Content = "Dosificación actualizadA con exito";
                    break;
            }
            DesHabilitar();
            LoadDatagrid();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DesHabilitar();
        }

        private void btnTelefono_Click(object sender, RoutedEventArgs e)
        {
        
        }

        private void BtnCollapseMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Visible;
            btnCollapseMenu.Visibility = Visibility.Collapsed;
        }

        private void BtnOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Collapsed;
            btnCollapseMenu.Visibility = Visibility.Visible;
        }

        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void dgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvDatos.Items.Count > 0 && dgvDatos.SelectedItem != null)
            {
                try
                {
                    dosi = new Dosificacion();
                    DataRowView dataRow = (DataRowView)dgvDatos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    brl = new DosificacionBRL();
                    dosi = brl.Get(id);
                    txtId.Text = id.ToString();
                    txtActividadEconomica.Text = dosi.ActividadEconomica;
                    txtLeyenda.Text = dosi.SegundaLeyenda;
                    txtLlaveDosificacion.Text = dosi.LlaveDocificada;
                    txtNriInicial.Text = dosi.NroInicial.ToString();
                    txtNroFinal.Text = dosi.NroFinal.ToString();
                    txtNumeroAutorizacion.Text = dosi.NumeroAutorizacion.ToString();
                    txtSfc.Text = dosi.Sfc;
                    cmbSucursales.Text = dosi.Sucursal;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void dgvTelefonos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadDatagrid();
        }
    }
}
