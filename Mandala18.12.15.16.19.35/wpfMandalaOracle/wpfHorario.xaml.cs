﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BRLOracle;
using Common;

namespace wpfMandalaOracle
{
    /// <summary>
    /// Lógica de interacción para wpfHorario.xaml
    /// </summary>
    public partial class wpfHorario : Window
    {
        public wpfHorario()
        {
            InitializeComponent();
        }
        byte operacion = 0;
        int idSeleccionado = 0;
        int idSeleccionadoEmpleado = 0;
        List<Empleado> empleados = new List<Empleado>();
        HorarioEmpleadoBRL brl;
        HorarioEmpleado trat;
        Empleado emp;
        char lunes;
        char martes;
        char miercoles;
        char jueves;
        char viernes;
        char sabado;
        char domingo;
        byte opcionLike = 0;
        #region Metodos
        private void Habilitar(byte operacion)
        {
            btnGuardar.IsEnabled = true;
            btnCancelar.IsEnabled = true;
            btnInsertar.IsEnabled = false;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;
            txtEmpleado.IsEnabled = true;
            txtHoraInicio.IsEnabled = true;
            txtHoraFin.IsEnabled = true;
            cbxLunes.IsEnabled = true;
            cbxMartes.IsEnabled = true;
            cbxMiercoles.IsEnabled = true;
            cbxJueves.IsEnabled = true;
            cbxViernes.IsEnabled = true;
            cbxSabado.IsEnabled = true;
            cbxDomingo.IsEnabled = true;
            txtEmpleado.Focus();
            this.operacion = operacion;
            lblAnuncio.Visibility = Visibility.Visible;
            lblAnuncio.Content = "";
            lblAnuncio.Content = "";
            dgvEmpleados.IsEnabled = true;

        }
        private void DesHabilitar()
        {
            btnGuardar.IsEnabled = false;
            btnCancelar.IsEnabled = false;
            btnInsertar.IsEnabled = true;
            btnModificar.IsEnabled = true;
            btnEliminar.IsEnabled = true;
            txtEmpleado.IsEnabled = false;
            txtHoraInicio.IsEnabled = false;
            txtHoraFin.IsEnabled = false;
            cbxLunes.IsEnabled = false;
            cbxMartes.IsEnabled = false;
            cbxMiercoles.IsEnabled = false;
            cbxJueves.IsEnabled = false;
            cbxViernes.IsEnabled = false;
            cbxSabado.IsEnabled = false;
            cbxDomingo.IsEnabled = false;
            txtEmpleado.Text = "";
            txtHoraInicio.Text = "";
            txtHoraFin.Text = "";
            cbxLunes.IsChecked = false;
            cbxMartes.IsChecked = false;
            cbxMiercoles.IsChecked = false;
            cbxJueves.IsChecked = false;
            cbxViernes.IsChecked = false;
            cbxSabado.IsChecked = false;
            cbxDomingo.IsChecked = false;
            dgvEmpleados.IsEnabled = true;
        }
        private void DesHabilitarCombos()
        {
            cbxLunes.IsChecked = false;
            cbxMartes.IsChecked = false;
            cbxMiercoles.IsChecked = false;
            cbxJueves.IsChecked = false;
            cbxViernes.IsChecked = false;
            cbxSabado.IsChecked = false;
            cbxDomingo.IsChecked = false;
        }
        private void VerficarCombos()
        {
            if (cbxLunes.IsChecked == true)
            {
                lunes = 'A';
            }
            else
            {
                lunes = 'N';
            }

            if (cbxMartes.IsChecked == true)
            {
                martes = 'A';
            }
            else
            {
                martes = 'N';
            }

            if (cbxMiercoles.IsChecked == true)
            {
                miercoles = 'A';
            }
            else
            {
                miercoles = 'N';
            }

            if (cbxJueves.IsChecked == true)
            {
                jueves = 'A';
            }
            else
            {
                jueves = 'N';
            }

            if (cbxViernes.IsChecked == true)
            {
                viernes = 'A';
            }
            else
            {
                viernes = 'N';
            }

            if (cbxSabado.IsChecked == true)
            {
                sabado = 'A';
            }
            else
            {
                sabado = 'N';
            }
            if (cbxDomingo.IsChecked == true)
            {
                domingo = 'A';
            }
            else
            {
                domingo = 'N';
            }
        }

        private int ObtenerIdEmpleado(string nombreEmpleado)
        {
            return 1030;
        }

        private void LoadDatagrid()
        {
            try
            {
                brl = new HorarioEmpleadoBRL();
                dgvDatos.ItemsSource = brl.Select().DefaultView;
                dgvDatos.Columns[0].Visibility = Visibility.Hidden;

                dgvEmpleados.ItemsSource = brl.SelectEmpleados().DefaultView;
                dgvEmpleados.Columns[0].Visibility = Visibility.Hidden;
                dgvEmpleados.Columns[2].Visibility = Visibility.Hidden;


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        private void LoadDatagridEmpleado()
        {
            try
            {

                if (txtEmpleado.Text != string.Empty && txtEmpleado.Text.Length > 2)
                {
                    brl = new HorarioEmpleadoBRL();
                    dgvEmpleados.ItemsSource = brl.SelectLikeCiFullName(opcionLike, txtEmpleado.Text).DefaultView;
                    dgvEmpleados.Columns[0].Visibility = Visibility.Hidden;

                }
                else
                {
                    dgvEmpleados.ItemsSource = null;
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        static DataTable ConvertToDataTable(List<Empleado> lista)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("IdEmpleado");
            dt.Columns.Add("Nombre Completo");
            dt.Columns.Add("Documento");

            foreach (var item in lista)
            {
                var row = dt.NewRow();
                row["IdEmpleado"] = item.IdEmpleado;
                row["Nombre Completo"] = item.NombreCompleto;
                row["Documento"] = item.Documento2;
                dt.Rows.Add(row);
            }
            return dt;
        }

        #endregion

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DesHabilitar();
        }



        private void BtnCollapseMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Visible;
            btnCollapseMenu.Visibility = Visibility.Collapsed;
            //imgLogo.Visibility = Visibility.Collapsed;
        }



        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            brl.Delete(idSeleccionado);
            LoadDatagrid();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            switch (operacion)
            {
                case 1: txtHoraInicio.Text = txtHoraInicio.Text.Trim();
                    txtHoraFin.Text = txtHoraFin.Text.Trim();
                    txtEmpleado.Text = txtEmpleado.Text.Trim();
                    if (Validacion.onlyLettersAndSpaces(txtEmpleado.Text))
                    {
                        try
                        {
                            VerficarCombos();
                            trat = new HorarioEmpleado(lunes, martes, miercoles, jueves, viernes, sabado, domingo, DateTime.Parse(txtHoraInicio.Text), DateTime.Parse(txtHoraFin.Text), empleados);
                            brl = new HorarioEmpleadoBRL(trat);
                            brl.Insert();
                            lblAnuncio.Content = "Registro insertado con exito";
                        }
                        catch (Exception ex)
                        {
                            lblAnuncio.Content = (ex.Message);
                        }
                        finally
                        {
                            LoadDatagrid();
                        }

                    }
                    else
                    {
                        lblAnuncio.Content = ("Verifique haber llenado todos los textos");
                    }
                    break;
                case 2: txtHoraInicio.Text = txtHoraInicio.Text.Trim();
                    txtHoraFin.Text = txtHoraFin.Text.Trim();
                    txtEmpleado.Text = txtEmpleado.Text.Trim();
                    if (Validacion.onlyLettersAndSpaces(txtEmpleado.Text))
                    {
                        try
                        {
                            VerficarCombos();
                            trat.Lunes = lunes;
                            trat.Martes = martes;
                            trat.Miercoles = miercoles;
                            trat.Jueves = jueves;
                            trat.Viernes = viernes;
                            trat.Sabado = sabado;
                            trat.Domingo = domingo;
                            trat.HoraInicio = DateTime.Parse(txtHoraInicio.Text);
                            trat.HoraFin = DateTime.Parse(txtHoraFin.Text);
                            trat = new HorarioEmpleado(idSeleccionado,lunes, martes, miercoles, jueves, viernes, sabado, domingo, DateTime.Parse(txtHoraInicio.Text), DateTime.Parse(txtHoraFin.Text), empleados);
                            brl = new HorarioEmpleadoBRL(trat);
                            brl.Update();
                            lblAnuncio.Content = "Registro Actualizado con exito";
                        }
                        catch (Exception ex)
                        {
                            lblAnuncio.Content = (ex.Message);
                        }
                        finally
                        {
                            LoadDatagrid();
                        }

                    }
                    else
                    {
                        lblAnuncio.Content = ("Revise haber llenado todos los textos");
                    }
                    break;
            }
            DesHabilitar();
        }




        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(1);

            LoadDatagrid();
        }


        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(2);
            LoadDatagrid();
        }



        private void BtnOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Collapsed;
            btnCollapseMenu.Visibility = Visibility.Visible;
            //imgLogo.Visibility = Visibility.Visible;
        }


        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void dgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvDatos.Items.Count > 0 && dgvDatos.SelectedItem != null)
            {
                try
                {

                    DataRowView dataRow = (DataRowView)dgvDatos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    idSeleccionado = id;
                    brl = new HorarioEmpleadoBRL();
                    trat = brl.Get(id);
                    txtHoraInicio.Text = trat.HoraInicio.ToShortTimeString();
                    txtHoraFin.Text = trat.HoraFin.ToShortTimeString();
                    DesHabilitarCombos();
                    if (trat.Lunes == 'A')
                    {
                        cbxLunes.IsChecked = true;
                    }
                    if (trat.Martes == 'A')
                    {
                        cbxMartes.IsChecked = true;
                    }
                    if (trat.Miercoles == 'A')
                    {
                        cbxMiercoles.IsChecked = true;
                    }
                    if (trat.Jueves == 'A')
                    {
                        cbxJueves.IsChecked = true;
                    }
                    if (trat.Viernes == 'A')
                    {
                        cbxViernes.IsChecked = true;
                    }
                    if (trat.Sabado == 'A')
                    {
                        cbxSabado.IsChecked = true;
                    }
                    if (trat.Domingo == 'A')
                    {
                        cbxDomingo.IsChecked = true;
                    }
                    //trat = brl.getEmpleados(id);
                    dgvEmpleadosSeleccionados.ItemsSource = brl.getEmpleados(id).DefaultView;
                    dgvEmpleadosSeleccionados.Columns[0].Visibility = Visibility.Hidden;
                    dgvEmpleadosSeleccionados.Columns[2].Visibility = Visibility.Hidden;

                      List<Empleado> lstemp=new List<Empleado>();
                     Empleado emplea=new Empleado();
                     DataTable tabla = new DataTable();
                     tabla = brl.getEmpleados(id);
                   // lstemp = tabla.ToList<Empleado>();
                   // lstemp= tabla
                   // emplea = brl.getEmpleados(id).toentidad<Empleado>;   
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }



        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadDatagrid();
            DesHabilitar();
        }

        private void txtEmpleado_TextChanged(object sender, TextChangedEventArgs e)
        {
            brl = new HorarioEmpleadoBRL();
            txtEmpleado.Text = txtEmpleado.Text.Trim();
            if (txtEmpleado.Text.Contains("%") || txtEmpleado.Text.Contains("$"))
            {
                MessageBox.Show("valor no permitido");
                txtEmpleado.Text = string.Empty;
            }
            if (Validacion.onlyLettersAndSpaces(txtEmpleado.Text))
            {
                opcionLike = 1;
            }
            else
            {
                opcionLike = 0;
            }
            LoadDatagridEmpleado();
        }

        private void dgvEmpleados_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvEmpleados.Items.Count > 0 && dgvEmpleados.SelectedItem != null)
            {
                try
                {

                    DataRowView dataRow = (DataRowView)dgvEmpleados.SelectedItem;
                    int idEmpleado = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    idSeleccionadoEmpleado = idEmpleado;
                    Empleado empl = new Empleado(int.Parse(dataRow.Row.ItemArray[0].ToString()), dataRow.Row.ItemArray[1].ToString(), dataRow.Row.ItemArray[2].ToString());
                    empleados.Add(empl);
                    dgvEmpleadosSeleccionados.ItemsSource = ConvertToDataTable(empleados).DefaultView;
                    dgvEmpleadosSeleccionados.Columns[0].Visibility = Visibility.Hidden;

                    try
                    {

                        brl = new HorarioEmpleadoBRL();
                        dgvDatos.ItemsSource = brl.Select().DefaultView;
                        dgvDatos.Columns[0].Visibility = Visibility.Hidden;
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }     
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void dgvEmpleadosSeleccionados_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
                        if (dgvEmpleadosSeleccionados.Items.Count > 0 && dgvEmpleadosSeleccionados.SelectedItem != null)
            {
                try
                {

                    DataRowView dataRow = (DataRowView)dgvEmpleadosSeleccionados.SelectedItem;
                   // empleados.Add(new Empleado(int.Parse(dataRow.Row.ItemArray[0].ToString())));
                    int idEmpleado = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    idSeleccionadoEmpleado = idEmpleado;
                    txtEmpleado.Text = dataRow.Row.ItemArray[2].ToString();
                    //brl = new HorarioEmpleadoBRL();
                    //trat = brl.Get(idEmpleado);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
      }
    }

