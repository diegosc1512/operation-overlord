﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BRLOracle;
using Common;
using System.Data;

namespace wpfMandalaOracle
{
    /// <summary>
    /// Lógica de interacción para winConsulta.xaml
    /// </summary>
    public partial class winConsulta : Window
    {
        public winConsulta()
        {
            InitializeComponent();
        }
        byte operacion = 0;
        int idSeleccionado = 0;
        int idSeleccionadoCliente = 0;
        int idSeleccionadoEmpleado = 0;
        int idSeleccionadoHorario = 0;
        int idSeleccionadoTratamiento = 0;
        byte opcionLike = 0;
        double costoTotal = 0;

        ConsultaBRL brl;
        Consulta cons;

        List<Tratamiento> tratamientoList = new List<Tratamiento>();
        List<TratamientoConsulta> tratamientoConsultaList = new List<TratamientoConsulta>();


        private void LoadDataConsultas()
        {
            try
            {
                brl = new ConsultaBRL();
                dgvDatos.ItemsSource = brl.SelectConsulta().DefaultView;
                dgvDatos.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        private void LoadDataCliente()
        {
            try
            {
                brl = new ConsultaBRL();
                dgvCliente.ItemsSource = brl.SelectCliente().DefaultView;
                dgvCliente.Columns[0].Visibility = Visibility.Hidden;
                dgvCliente.Columns[2].Visibility = Visibility.Hidden;
                dgvCliente.Columns[6].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        private void LoadDatagridHorario(string dia)
        {
            try
            {
                brl = new ConsultaBRL();
                dgvHorarioEmpleado.ItemsSource = brl.SelectHorariosEmpleado(idSeleccionadoEmpleado).DefaultView;
                dgvHorarioEmpleado.Columns[0].Visibility = Visibility.Hidden;
                dgvHorarioEmpleado.Columns[1].Visibility = Visibility.Hidden;
                dgvHorarioEmpleado.Columns[2].Visibility = Visibility.Hidden;
                dgvHorarioEmpleado.Columns[3].Visibility = Visibility.Hidden;
                dgvHorarioEmpleado.Columns[4].Visibility = Visibility.Hidden;
                dgvHorarioEmpleado.Columns[5].Visibility = Visibility.Hidden;
                dgvHorarioEmpleado.Columns[6].Visibility = Visibility.Hidden;
                dgvHorarioEmpleado.Columns[7].Visibility = Visibility.Hidden;
                //dgvHorarioEmpleado.Columns[8].Visibility = Visibility.Hidden;
                //dgvHorarioEmpleado.Columns[9].Visibility = Visibility.Hidden;
                dgvHorarioEmpleado.Columns[10].Visibility = Visibility.Hidden;
                switch (dia)
                {
                    case "lunes":
                        dgvHorarioEmpleado.Columns[1].Visibility = Visibility.Visible;
                        break;
                    case "martes":
                        dgvHorarioEmpleado.Columns[2].Visibility = Visibility.Visible;
                        break;
                    case "miercoles":
                        dgvHorarioEmpleado.Columns[3].Visibility = Visibility.Visible;
                        break;
                    case "jueves":
                        dgvHorarioEmpleado.Columns[4].Visibility = Visibility.Visible;
                        break;
                    case "viernes":
                        dgvHorarioEmpleado.Columns[5].Visibility = Visibility.Visible;
                        break;
                    case "sabado":
                        dgvHorarioEmpleado.Columns[6].Visibility = Visibility.Visible;
                        break;
                    case "domingo":
                        dgvHorarioEmpleado.Columns[7].Visibility = Visibility.Visible;
                        break;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        private void LoadDatagridCliente()
        {
            try
            {

                if (txtCliente.Text != string.Empty && txtCliente.Text.Length > 2)
                {
                    brl = new ConsultaBRL();
                    dgvCliente.ItemsSource = brl.SelectLikeCiFullNameCliente(opcionLike, txtCliente.Text).DefaultView;
                    dgvCliente.Columns[0].Visibility = Visibility.Hidden;
                    dgvCliente.Columns[2].Visibility = Visibility.Hidden;

                }
                else
                {
                    dgvCliente.ItemsSource = null;
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        private void LoadDatagridEmpleado()
        {
            try
            {

                if (txtEmpleado.Text != string.Empty && txtEmpleado.Text.Length > 2)
                {
                    brl = new ConsultaBRL();
                    dgvEmpleados.ItemsSource = brl.SelectLikeCiFullNameEmpleado(opcionLike, txtEmpleado.Text).DefaultView;
                    dgvEmpleados.Columns[0].Visibility = Visibility.Hidden;
                    dgvEmpleados.Columns[2].Visibility = Visibility.Hidden;

                }
                else
                {
                    dgvEmpleados.ItemsSource = null;
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        private void LoadDatagridTratamiento(string txttra)
        {
            //lblPrecio.Content = 0;
            try
            {

                if (txtTratamiento.Text != string.Empty && txtTratamiento.Text.Length > 2)
                {
                    brl = new ConsultaBRL();
                    dgvTratamientos.ItemsSource = brl.LoadDatagridTratamiento(txttra).DefaultView;
                    dgvTratamientos.Columns[0].Visibility = Visibility.Hidden;
                    dgvTratamientos.Columns[2].Visibility = Visibility.Hidden;
                    dgvTratamientos.Columns[3].Visibility = Visibility.Hidden;

                }
                else
                {
                    dgvTratamientos.ItemsSource = null;
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void dgvClientes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvCliente.Items.Count > 0 && dgvCliente.SelectedItem != null)
            {
                try
                {

                    DataRowView dataRow = (DataRowView)dgvCliente.SelectedItem;
                    int idCliente = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    idSeleccionadoCliente = idCliente;
                    txtCliente.Text = dataRow.Row.ItemArray[1].ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void dgvEmpleados_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvEmpleados.Items.Count > 0 && dgvEmpleados.SelectedItem != null)
            {
                try
                {

                    DataRowView dataRow = (DataRowView)dgvEmpleados.SelectedItem;
                    int idEmpleados = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    idSeleccionadoEmpleado = idEmpleados;
                    txtEmpleado.Text = dataRow.Row.ItemArray[1].ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void dgvHorarioEmpleado_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvHorarioEmpleado.Items.Count > 0 && dgvHorarioEmpleado.SelectedItem != null)
            {
                try
                {

                    DataRowView dataRow = (DataRowView)dgvHorarioEmpleado.SelectedItem;
                    int idHorario = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    idSeleccionadoHorario = idHorario;
                    HorarioEmpleado.Text = dataRow.Row.ItemArray[1].ToString(); // verificar si no debe ser con value
                    lblHoraInicjo.Content = dataRow.Row.ItemArray[8].ToString();
                    lblHoraFin.Content = dataRow.Row.ItemArray[9].ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void dgvTratamientos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvTratamientos.Items.Count > 0 && dgvTratamientos.SelectedItem != null)
            {
                try
                {

                    DataRowView dataRow = (DataRowView)dgvTratamientos.SelectedItem;
                    int idTratamientos = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    idSeleccionadoTratamiento = idTratamientos;
                    txtTratamiento.Text = dataRow.Row.ItemArray[1].ToString();
                    lblPrecio.Content = dataRow.Row.ItemArray[3].ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //cons = new Consulta(Convert.ToDouble(total().ToString()), idSeleccionadoCliente, 'R', DateTime.Parse(HorarioEmpleado.SelectedDate.ToString()), idSeleccionadoEmpleado, DateTime.Parse(lblHoraInicjo.Content.ToString()), DateTime.Parse(lblHoraFin.Content.ToString()), txtDiagnostico.Text);
                cons = new Consulta(costoTotal, idSeleccionadoCliente, 'R', DateTime.Parse(HorarioEmpleado.SelectedDate.ToString()), idSeleccionadoEmpleado, DateTime.Parse(lblHoraInicjo.Content.ToString()), DateTime.Parse(lblHoraFin.Content.ToString()), txtDiagnostico.Text, tratamientoConsultaList);
                brl = new ConsultaBRL(cons, tratamientoConsultaList);
                //brl.Insert(cons, tratamientoConsultaList);
                brl.Insert();
                lblAnuncio.Content = "Registro Insertado Correctamente";
                lblAnuncio.Visibility = Visibility.Visible;
            }
            catch (Exception)
            {
                lblAnuncio.Content = "Faltan llenar Datos";
                lblAnuncio.Visibility = Visibility.Visible;
            }



        }

        private double total()
        {
            double res = 0;
            foreach (Tratamiento item in tratamientoList)
            {
                res = res + Convert.ToDouble(item.Precio)*Convert.ToDouble(txtCantidaSesiones.Text);
            }
            return res;
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // LoadDatagridCliente();
            LoadDataConsultas();

        }

        private void BtnCollapseMenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnOpenMenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void dgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void txtEmpleado_TextChanged(object sender, TextChangedEventArgs e)
        {
            brl = new ConsultaBRL();
            txtEmpleado.Text = txtEmpleado.Text.Trim();
            if (txtEmpleado.Text.Contains("%") || txtEmpleado.Text.Contains("$"))
            {
                MessageBox.Show("valor no permitido");
                txtEmpleado.Text = string.Empty;
            }
            if (Validacion.onlyLettersAndSpaces(txtEmpleado.Text))
            {
                opcionLike = 1;
            }
            else
            {
                opcionLike = 0;
            }
            LoadDatagridEmpleado();
        }

        private void txtCliente_TextChanged(object sender, TextChangedEventArgs e)
        {
            brl = new ConsultaBRL();
            txtCliente.Text = txtCliente.Text.Trim();
            if (txtCliente.Text.Contains("%") || txtCliente.Text.Contains("$"))
            {
                MessageBox.Show("valor no permitido");
                txtCliente.Text = string.Empty;
            }
            if (Validacion.onlyLettersAndSpaces(txtCliente.Text))
            {
                opcionLike = 1;
            }
            else
            {
                opcionLike = 0;
            }
            LoadDatagridCliente();
        }
        private void HorarioEmpleado_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            brl = new ConsultaBRL();
            string dia = Convert.ToDateTime(HorarioEmpleado.SelectedDate.ToString()).Day.ToString();
            LoadDatagridHorario(dia);
        }

        private void txtTratamineto_TextChanged(object sender, TextChangedEventArgs e)
        {
            brl = new ConsultaBRL();
            txtTratamiento.Text = txtTratamiento.Text.Trim();
            LoadDatagridTratamiento(txtTratamiento.Text);

        }

        private void btnAgregarTratamiento_Click(object sender, RoutedEventArgs e)
        {
            Tratamiento trata;
            if (lblPrecio.Content != "0")
            {
                trata = new Tratamiento(idSeleccionadoTratamiento, txtTratamiento.Text, Convert.ToDecimal(lblPrecio.Content));
            }
            else
            {
                trata = new Tratamiento(idSeleccionadoTratamiento, txtTratamiento.Text, 0);
            }
            TratamientoConsulta trataConsulta = new TratamientoConsulta(idSeleccionadoTratamiento, 0, int.Parse(txtCantidaSesiones.Text), double.Parse(trata.Precio.ToString()));
            tratamientoConsultaList.Add(trataConsulta);
            tratamientoList.Add(trata);
            //lbxTratamientos.Items.Add(trata.NombreTratamiento + " " + trata.Precio + " " + txtCantidaSesiones.Text);
            lbxTratamientos.Items.Add(trata.NombreTratamiento + " " + lblPrecio.Content + " " + txtCantidaSesiones.Text);
            costoTotal = costoTotal + Convert.ToDouble(total().ToString());
            lblPrecio.Content = 0;
        }

        private void btnQuitarTratamineto_Click(object sender, RoutedEventArgs e)
        {
            if (lbxTratamientos.SelectedIndex != -1)
            {
                tratamientoConsultaList.RemoveAt(lbxTratamientos.SelectedIndex);
                tratamientoConsultaList.RemoveAt(lbxTratamientos.SelectedIndex);
                tratamientoList.RemoveAt(lbxTratamientos.SelectedIndex);
                lbxTratamientos.Items.RemoveAt(lbxTratamientos.SelectedIndex);
            }

        }

        private void btnAgregarSesion_Click(object sender, RoutedEventArgs e)
        {
            if (lbxTratamientos.SelectedIndex != -1)
            {

                int idTratamientoParaSesion = lbxTratamientos.SelectedIndex;

                foreach (Tratamiento item in tratamientoList)
                {

                    if (item.IdTratamiento == idTratamientoParaSesion + 2)
                    {
                        lblnumeroSesiones.Content = item.IdTratamiento.ToString();
                        lblNombreTratamiento.Content = item.NombreTratamiento.ToString();
                    }
                }
            }


        }
    }
}
