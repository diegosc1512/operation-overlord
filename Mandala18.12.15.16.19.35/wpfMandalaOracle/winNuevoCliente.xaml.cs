﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BRLOracle;
using Common;
using System.Data;
using System.Security.Cryptography;


namespace wpfMandalaOracle
{
    /// <summary>
    /// Lógica de interacción para winNuevoCliente.xaml
    /// </summary>
    public partial class winNuevoCliente : Window
    {
        public winNuevoCliente()
        {
            InitializeComponent();
        }
        byte operacion = 0;
        ClienteBRL brl;
        Cliente clie;
        Persona pers;
        Telefono tel;
        Usuario usu;
        //winTelefono fono;
        DataTable dt = new DataTable();
        public List<Telefono> telefonosClientes = new List<Telefono>();
        private char tipoTel;
        char rol;

        #region Metodos
        private void Habilitar(byte operacion)
        {
            btnGuardar.IsEnabled = true;
            btnCancelar.IsEnabled = true;
            btnInsertar.IsEnabled = false;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;
            txtDocumento.IsEnabled = true;
            txtNombreCliente.IsEnabled = true;
            txtPrimerApellido.IsEnabled = true;
            txtSegundoApellido.IsEnabled = true;
            cmbSexo.IsEnabled = true;
            dtFechaNacimiento.IsEnabled = true;
            txtRazon.IsEnabled = true;
            txtNit.IsEnabled = true;
            txtNombreUsuario.IsEnabled = true;
            txtPassword.IsEnabled = true;
            btnTelefono.IsEnabled = true;
            txtNombreCliente.Focus();
            this.operacion = operacion;
            lblAnuncio.Visibility = Visibility.Visible;
            lblAnuncio.Content = "";
            lblAnuncio.Content = "";
            dgvDatos.IsEnabled = true;
            dgvTelefonos.IsEnabled = true;
            cmbTipo.IsEnabled = true;
            txtNumero.IsEnabled = true;
            txtNombreUsuario.IsEnabled = true;
            cmbTipo.IsEnabled = true;
            txtPassword.IsEnabled = true;
            txtEmail.IsEnabled = true;
            cmbRol.IsEnabled = true;
        }
        private void DesHabilitar()
        {
            txtNombreUsuario.Text = "";
            txtNombreUsuario.IsEnabled = false;
            txtPassword.Password = "";
            txtPassword.IsEnabled = false;
            txtEmail.Text = "";
            txtEmail.IsEnabled = false;
            cmbRol.IsEditable = false;
            btnGuardar.IsEnabled = false;
            btnCancelar.IsEnabled = false;
            btnInsertar.IsEnabled = true;
            btnModificar.IsEnabled = true;
            btnEliminar.IsEnabled = true;
            txtDocumento.IsEnabled = false;
            txtNombreCliente.IsEnabled = false;
            txtPrimerApellido.IsEnabled = false;
            txtSegundoApellido.IsEnabled = false;
            cmbSexo.IsEnabled = false;
            dtFechaNacimiento.IsEnabled = false;
            txtRazon.IsEnabled = false;
            txtNit.IsEnabled = false;
            btnTelefono.IsEnabled = false;
            txtNombreCliente.Text = "";
            txtPrimerApellido.Text = "";
            txtSegundoApellido.Text = "";
            cmbSexo.Text = "";
            txtDocumento.Text = "";
            txtRazon.Text = "";
            txtNit.Text = "";
            txtPassword.IsEnabled = false;
            txtNombreUsuario.Text = "";
            txtNombreUsuario.IsEnabled = false;
            txtNumero.IsEnabled = false;
            txtNumero.Text = "";
            cmbTipo.IsEnabled = false;
            cmbRol.IsEnabled = false;
            //txbFoto.Text = "Registrar imagen.";

        }
        private void LoadDatagrid()
        {
            try
            {
                brl = new ClienteBRL();
                dgvDatos.ItemsSource = brl.Select().DefaultView;
                dgvDatos.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        static DataTable ConvertToDataTable(List<Telefono> lista)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("IdTelefono");
            dt.Columns.Add("Numero");
            dt.Columns.Add("Tipo");
            dt.Columns.Add("IdPersona");

            foreach (var item in lista)
            {
                var row = dt.NewRow();
                row["IdTelefono"] = item.IdTelefono;
                row["Numero"] = item.Numero;
                row["Tipo"] = item.Tipo;
                row["IdPersona"] = item.IdPersona;
                dt.Rows.Add(row);
            }
            return dt;
        }
        #endregion
        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DesHabilitar();
        }

        private void BtnCollapseMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Visible;
            btnCollapseMenu.Visibility = Visibility.Collapsed;
            //imgLogo.Visibility = Visibility.Collapsed;
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            //pers = new Persona();
            // pers = new Persona(txtNombreCliente.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, sexo, Convert.ToDateTime(dtFechaNacimiento.SelectedDate.ToString()), txtDocumento.Text, telefonosClientes);

            brl.Delete(Convert.ToInt32(txtId.Text));
            LoadDatagrid();
            DesHabilitar();

        }
        char sexo;

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            switch (operacion)
            {
                case 1:
                    if (cmbSexo.Text == "Masculino")
                    {
                        sexo = 'M';
                    }
                    else
                    {
                        sexo = 'F';
                    }
                    if (txtEmail.Text != string.Empty && txtDocumento.Text != string.Empty
                       && txtNombreCliente.Text != string.Empty &&
                       txtPassword.Password != string.Empty && txtPrimerApellido.Text != string.Empty &&
                       txtSegundoApellido.Text != string.Empty)
                    {
                        txtNombreCliente.Text.Trim();
                        txtPrimerApellido.Text.Trim();
                        txtSegundoApellido.Text.Trim();
                        txtNombreUsuario.Text.Trim();

                        if (Validacion.onlyLettersAndSpaces(txtNombreCliente.Text) && Validacion.onlyLettersAndSpaces(txtPrimerApellido.Text) && Validacion.onlyLettersAndSpaces(txtSegundoApellido.Text)
                            && Validacion.ValidarCorreo(txtEmail.Text))
                        {
                            pers = new Persona(txtNombreCliente.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, sexo, Convert.ToDateTime(dtFechaNacimiento.SelectedDate.ToString()), txtDocumento.Text, telefonosClientes);
                            clie = new Cliente(txtNit.Text, txtRazon.Text);
                            MD5 md5Hash = MD5.Create();// revisar si esto funciona
                            usu = new Usuario(txtNombreUsuario.Text, GetMd5Hash(md5Hash, txtPassword.Password).ToString(), rol.ToString(), txtEmail.Text);
                            brl = new ClienteBRL(pers, clie, usu);
                            brl.Insert();
                            lblAnuncio.Content = "Paciente insertado con exito";
                        }
                        else
                        {
                            MessageBox.Show("Formato de nombre o correo incorrecto");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Debe llenar todos los campos");
                    }
                    break;
                case 2:
                    if (cmbSexo.Text == "Masculino")
                    {
                        sexo = 'M';
                    }
                    else
                    {
                        sexo = 'F';
                    }
                    if (txtEmail.Text != string.Empty && txtDocumento.Text != string.Empty &&
                       txtNombreCliente.Text != string.Empty &&
                       txtPassword.Password != string.Empty && txtPrimerApellido.Text != string.Empty &&
                       txtSegundoApellido.Text != string.Empty)
                    {
                        txtNombreCliente.Text.Trim();
                        txtPrimerApellido.Text.Trim();
                        txtSegundoApellido.Text.Trim();
                        txtNombreUsuario.Text.Trim();

                        if (Validacion.onlyLettersAndSpaces(txtNombreCliente.Text) && Validacion.onlyLettersAndSpaces(txtPrimerApellido.Text) && Validacion.onlyLettersAndSpaces(txtSegundoApellido.Text)
                            && Validacion.ValidarCorreo(txtEmail.Text))
                        {
                            pers = new Persona(txtNombreCliente.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, sexo, Convert.ToDateTime(dtFechaNacimiento.SelectedDate.ToString()), txtDocumento.Text, telefonosClientes);
                            pers.IdPersona = Convert.ToInt32(txtId.Text);
                            clie = new Cliente(txtNit.Text, txtRazon.Text);
                            clie.IdCliente = Convert.ToInt32(txtId.Text);
                            MD5 md5Hash2 = MD5.Create();// revisar si esto funciona
                            usu = new Usuario(txtNombreUsuario.Text, GetMd5Hash(md5Hash2, txtPassword.Password).ToString(), rol.ToString(), txtEmail.Text);
                            brl = new ClienteBRL(pers, clie, usu);
                            brl.Update();
                            lblAnuncio.Content = "Paciente actualizado con exito";
                        }
                        else
                        {
                            MessageBox.Show("Formato de nombre o correo incorrecto");
                        }
                    }
                    else
                    {
                            MessageBox.Show("Debe llenar todos los campos");
                    }
                        break;
            }
            LoadDatagrid();
            DesHabilitar();

        }
        static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(1);

            LoadDatagrid();
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(2);

            LoadDatagrid();
        }

        private void BtnOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Collapsed;
            btnCollapseMenu.Visibility = Visibility.Visible;
            //imgLogo.Visibility = Visibility.Visible;
        }

        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void dgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvDatos.Items.Count > 0 && dgvDatos.SelectedItem != null)
            {
                try
                {
                    DataRowView dataRow = (DataRowView)dgvDatos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    brl = new ClienteBRL();
                    pers = brl.Get(id);
                    clie = brl.Get2(id);
                    txtId.Text = id.ToString();

                    txtNombreCliente.Text = pers.Nombres;
                    txtPrimerApellido.Text = pers.PrimerApellido;
                    txtSegundoApellido.Text = pers.SegundoApellido;
                    cmbSexo.Text = pers.Sexo.ToString();
                    dtFechaNacimiento.SelectedDate = pers.FechaNacimiento;
                    txtDocumento.Text = pers.Documento;
                    txtNit.Text = clie.Nit;
                    txtRazon.Text = clie.RazonSocial;
                    txtNombreUsuario.Text = usu.NombreUsuario;
                    txtEmail.Text = usu.Email;
                    cmbRol.Text = usu.Rol;

                    dgvTelefonos.ItemsSource = brl.GetTelefono(id).DefaultView;
                    dgvTelefonos.Columns[0].Visibility = Visibility.Hidden;
                   // dgvTelefonos.Columns[3].Visibility = Visibility.Hidden;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadDatagrid();
            //DesHabilitar();
        }

        private void btnTelefono_Click(object sender, RoutedEventArgs e)
        {
            if (cmbTipo.Text == "celular")
            {
                tipoTel = 'C';
            }
            else
            {
                tipoTel = 'F';
            }
            Telefono telefono = new Telefono(txtNumero.Text, tipoTel);
            telefonosClientes.Add(telefono);
            dgvTelefonos.ItemsSource = ConvertToDataTable(telefonosClientes).DefaultView;
            dgvTelefonos.Columns[0].Visibility = Visibility.Hidden;
            dgvTelefonos.Columns[3].Visibility = Visibility.Hidden;

            try
            {

                brl = new ClienteBRL();
                dgvDatos.ItemsSource = brl.Select().DefaultView;
                dgvDatos.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void dgvTelefonos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvTelefonos.Items.Count > 0 && dgvTelefonos.SelectedItem != null)
            {
                try
                {
                    DataRowView dataRow = (DataRowView)dgvTelefonos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    brl = new ClienteBRL();
                    tel = brl.GetUnTelefono(id);
                    txtDocumento.Text = pers.Documento;
                    txtNumero.Text = tel.Numero;
                    if (tel.Tipo == 'C')
                    {
                        cmbTipo.Text = "celular";
                    }
                    else
                    {
                        cmbTipo.Text = "Fijo";
                    }
                    //dgvTelefonos.ItemsSource = brl.GetTelefono(id).DefaultView;
                    //dgvTelefonos.Columns[0].Visibility = Visibility.Hidden;
                    //dgvTelefonos.Columns[3].Visibility = Visibility.Hidden;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void txtNombreUsuario_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}