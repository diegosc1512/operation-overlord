﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfMandalaOracle
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void BtnCollapseMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Visible;
            btnCollapseMenu.Visibility = Visibility.Collapsed;
         //   imgLogo.Visibility = Visibility.Collapsed;
        }

        private void BtnOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Collapsed;
            btnCollapseMenu.Visibility = Visibility.Visible;
        //    imgLogo.Visibility = Visibility.Visible;
        }

        private void BtnInicio_Click(object sender, RoutedEventArgs e)
        {
            GridMostrar.Children.Clear();
         //   GridMostrar.Children.Add(new UserControlInicio());
            winSucursal form = new winSucursal();
            form.ShowDialog();
        }

        private void btnTratamiento_Click(object sender, RoutedEventArgs e)
        {
            winAdmTratamientos form = new winAdmTratamientos();
           form.ShowDialog();
        }

        private void btnAgenda_Click(object sender, RoutedEventArgs e)
        {
          
        //    empleado.winHorarioEmpleado form = new empleado.winHorarioEmpleado();
         //   form.ShowDialog();
        }

        private void btnAdministrarUsuario_Click(object sender, RoutedEventArgs e)
        {
         //   usuario.winAdministrarUsuario form = new usuario.winAdministrarUsuario();
          //  form.ShowDialog();
        }

        private void btnPaciente_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {

        }

        private void btnPaciente_Click(object sender, RoutedEventArgs e)
        {
            winNuevoCliente form = new winNuevoCliente();
            form.ShowDialog();
        }

        private void btnEmpleado_Click(object sender, RoutedEventArgs e)
        {
            winEmpleado from = new winEmpleado();
            from.ShowDialog();
        }

        private void btnAusencias_Click(object sender, RoutedEventArgs e)
        {
         //   empleado.winNoAtencion form = new empleado.winNoAtencion();
         //   form.ShowDialog();
        }

        private void btnCamara_Click(object sender, RoutedEventArgs e)
        {
          //  TakeSnapsWithWebcamUsingWpfMvvm.MainWindow2 frm = new TakeSnapsWithWebcamUsingWpfMvvm.MainWindow2();
         //   frm.ShowDialog();
        }

        private void btnReservas_Click(object sender, RoutedEventArgs e)
        {
          //  winConsulta form = new winConsulta();
         //   form.Show();
        }

        private void btnSucursal_Click(object sender, RoutedEventArgs e)
        {
            winSucursal form = new winSucursal();
            form.ShowDialog();
        }

        private void btnPaciente1_Click(object sender, RoutedEventArgs e)
        {
            winNuevoCliente form = new winNuevoCliente();
            form.ShowDialog();
        }

        private void btnTratamiento1_Click(object sender, RoutedEventArgs e)
        {
            winAdmTratamientos form = new winAdmTratamientos();
            form.ShowDialog();
        }

        private void btnEmpleado1_Click(object sender, RoutedEventArgs e)
        {
            winEmpleado form = new winEmpleado();
            form.ShowDialog();
        }

        private void btnHorario_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnDosificacion_Click(object sender, RoutedEventArgs e)
        {
            winDosificacion form = new winDosificacion();
            form.ShowDialog();
        }

        private void btnReserva_Click(object sender, RoutedEventArgs e)
        {
            winConsulta form = new winConsulta();
            form.ShowDialog();
        }
    }
}
