﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Common;
using BRLOracle;
using System.Data;

namespace wpfMandalaOracle
{
    /// <summary>
    /// Lógica de interacción para winAdmTratamientos.xaml
    /// </summary>
    public partial class winAdmTratamientos : Window
    {
        public winAdmTratamientos()
        {
            InitializeComponent();
        }


        byte operacion = 0;
        int idSeleccionado = 0;
        TratamientoBRL brl;
        Tratamiento trat;
        #region Metodos
        private void Habilitar(byte operacion)
        {
            btnGuardar.IsEnabled = true;
            btnCancelar.IsEnabled = true;
            btnInsertar.IsEnabled = false;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;
            txtDescripcion.IsEnabled = true;
            txtNombreTratamiento.IsEnabled = true;
            txtPrecio.IsEnabled = true;
            txtNombreTratamiento.Focus();
            this.operacion = operacion;
            lblAnuncio.Visibility = Visibility.Visible;
            lblAnuncio.Content = "";
            lblAnuncio.Content = "";

        }
        private void DesHabilitar()
        {
            btnGuardar.IsEnabled = false;
            btnCancelar.IsEnabled = false;
            btnInsertar.IsEnabled = true;
            btnModificar.IsEnabled = true;
            btnEliminar.IsEnabled = true;
            txtNombreTratamiento.IsEnabled = false;
            txtPrecio.IsEnabled = false;
            txtDescripcion.IsEnabled = false;
            txtNombreTratamiento.Text = "";
            txtPrecio.Text = "";
            txtDescripcion.Text = "";

        }
        private void LoadDatagrid()
        {
            try
            {
                brl = new TratamientoBRL();
                dgvDatos.ItemsSource = brl.Select().DefaultView;
                //dgvDatos.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        #endregion

        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void BtnInicio_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnCollapseMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Visible;
            btnCollapseMenu.Visibility = Visibility.Collapsed;
            //  imgLogo.Visibility = Visibility.Collapsed;
        }
        private void BtnOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Collapsed;
            btnCollapseMenu.Visibility = Visibility.Visible;
            //  imgLogo.Visibility = Visibility.Visible;
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            brl.Delete(idSeleccionado);
            LoadDatagrid();
        }

        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(1);

            LoadDatagrid();
        }

        private void btnTratamientoReporte_Click(object sender, RoutedEventArgs e)
        {
           // WinReporteTratamientos form = new WinReporteTratamientos();
           // form.ShowDialog();
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(2);
            LoadDatagrid();

        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {

            switch (operacion)
            {
                case 1: txtNombreTratamiento.Text = txtNombreTratamiento.Text.Trim();
                    txtDescripcion.Text = txtDescripcion.Text.Trim();
                    txtPrecio.Text = txtPrecio.Text.Trim();
                    if (Validacion.onlyLettersAndSpaces(txtNombreTratamiento.Text) && Validacion.onlyLettersAndSpaces(txtDescripcion.Text))
                    {
                        try
                        {
                            trat = new Tratamiento(txtNombreTratamiento.Text, txtDescripcion.Text, Convert.ToDecimal(txtPrecio.Text));
                            brl = new TratamientoBRL(trat);
                            //if (!brl.Existe(txtNombreTratamiento.Text))
                           // {
                                brl.Insert();
                                lblAnuncio.Content = "Registro insertado con exito";
                            //}
                           // else
                           // {
                               // lblAnuncio.Content = "Registro duplicado";

                           // }
                        }
                        catch (Exception ex)
                        {
                            lblAnuncio.Content = (ex.Message);
                        }
                        finally
                        {
                            LoadDatagrid();
                        }

                    }
                    else
                    {
                        lblAnuncio.Content = ("solo se admiten letras y espacios en Nombre y descripcion");
                    }
                    break;
                case 2: txtNombreTratamiento.Text = txtNombreTratamiento.Text.Trim();
                    txtDescripcion.Text = txtDescripcion.Text.Trim();
                    txtPrecio.Text = txtPrecio.Text.Trim();
                    if (Validacion.onlyLettersAndSpaces(txtNombreTratamiento.Text) && Validacion.onlyLettersAndSpaces(txtDescripcion.Text))
                    {
                        try
                        {
                            //  trat = new Tratamiento(txtNombreTratamiento.Text, txtDescripcion.Text, Convert.ToDecimal(txtPrecio.Text));

                            trat.NombreTratamiento = txtNombreTratamiento.Text;
                            trat.Descripcion = txtDescripcion.Text;
                            trat.Precio = decimal.Parse(txtPrecio.Text);

                            brl = new TratamientoBRL(trat);
                           // if (brl.Existe(txtNombreTratamiento.Text))

                            //{
                                brl.Update();
                                lblAnuncio.Content = "Registro Actualizado con exito";
                           // }
                            //else
                            //{
                               // lblAnuncio.Content = "Registro duplicado";

                            //}


                        }
                        catch (Exception ex)
                        {
                            lblAnuncio.Content = (ex.Message);
                        }
                        finally
                        {
                            LoadDatagrid();
                        }

                    }
                    else
                    {
                        lblAnuncio.Content = ("solo se admiten letras y espacios en Nombre y descripcion");
                    }
                    break;
            }
            DesHabilitar();

        }


        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DesHabilitar();

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadDatagrid();
        }

        private void dgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvDatos.Items.Count > 0 && dgvDatos.SelectedItem != null)
            {
                try
                {
                    DataRowView dataRow = (DataRowView)dgvDatos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    idSeleccionado = id;
                    brl = new TratamientoBRL();
                    trat = brl.Get(id);
                    txtNombreTratamiento.Text = trat.NombreTratamiento;
                    txtDescripcion.Text = trat.Descripcion;
                    txtPrecio.Text = trat.Precio.ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
    }

