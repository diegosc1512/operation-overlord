﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BRLOracle;
using Common;
using System.Data;
using System.Security.Cryptography;

namespace wpfMandalaOracle
{
    /// <summary>
    /// Lógica de interacción para winEmpleado.xaml
    /// </summary>
    public partial class winEmpleado : Window
    {
        public winEmpleado()
        {
            InitializeComponent();
        }
        byte operacion = 0;
        EmpleadoBRL brl;
        Empleado emp;
        Persona pers;
        Usuario usu;
        Telefono tel;
        DataTable dt = new DataTable();
        public List<Telefono> telefonosEmpleado = new List<Telefono>();
        public List<HorarioEmpleado> horarios = new List<HorarioEmpleado>();
        private char tipoTel;
        char sexo;
        char rol;
        int id = 0;


        #region Metodos
        private void Habilitar(byte operacion)
        {
            btnGuardar.IsEnabled = true;
            btnCancelar.IsEnabled = true;
            btnInsertar.IsEnabled = false;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;
            txtDocumento.IsEnabled = true;
            txtNombreEmpleado.IsEnabled = true;
            txtPrimerApellido.IsEnabled = true;
            txtSegundoApellido.IsEnabled = true;
            cmbSexo.IsEnabled = true;
            dtFechaNacimiento.IsEnabled = true;
            txtSalario.IsEnabled = true;
            txtGrado.IsEnabled = true;
            btnTelefono.IsEnabled = true;
            txtNombreUsuario.IsEnabled = true;
            cmbTipo.IsEnabled = true;
            txtPassword.IsEnabled = true;
            txtEmail.IsEnabled = true;
            cmbRol.IsEditable = true;
            txtNombreEmpleado.Focus();
            this.operacion = operacion;
            lblAnuncio.Visibility = Visibility.Visible;
            lblAnuncio.Content = "";
            lblAnuncio.Content = "";
            cmbRol.IsEnabled = true;
            txtNumero.IsEnabled = true;

        }
        private void DesHabilitar()
        {
            txtNombreUsuario.Text = "";
            txtNombreUsuario.IsEnabled = false;
            txtPassword.Password = "";
            txtPassword.IsEnabled = false;
            txtEmail.Text = "";
            txtEmail.IsEnabled = false;
            cmbRol.IsEditable = false;
            btnGuardar.IsEnabled = false;
            btnCancelar.IsEnabled = false;
            btnInsertar.IsEnabled = true;
            btnModificar.IsEnabled = true;
            btnEliminar.IsEnabled = true;
            txtDocumento.IsEnabled = false;
            txtNombreEmpleado.IsEnabled = false;
            txtPrimerApellido.IsEnabled = false;
            txtSegundoApellido.IsEnabled = false;
            cmbSexo.IsEnabled = false;
            dtFechaNacimiento.IsEnabled = false;
            txtSalario.IsEnabled = false;
            txtGrado.IsEnabled = false;
            btnTelefono.IsEnabled = false;
            txtNombreEmpleado.Text = "";
            txtPrimerApellido.Text = "";
            txtSegundoApellido.Text = "";
            txtDocumento.Text = "";
            txtSalario.Text = "";
            txtGrado.Text = "";
            txtNumero.IsEnabled = false;
            cmbRol.IsEnabled = false;
            cmbTipo.IsEnabled = false;
        }
        private bool validar()
        {
            bool res;
            if (txtDocumento.Text==""||txtEmail.Text==""||txtGrado.Text==""||txtNombreEmpleado.Text==""||txtNombreUsuario.Text==""||txtNumero.Text==""||txtPassword.Password==""||txtPrimerApellido.Text==""||txtSalario.Text=="")
            {
                res = false;
            }  
            else
            {
                res= true;
            }
            return res;
        }
        private void LoadDatagrid()
        {
            try
            {
                brl = new EmpleadoBRL();
                dgvDatos.ItemsSource = brl.Select().DefaultView;
                dgvDatos.Columns[0].Visibility = Visibility.Hidden;

                dgvHorariosDisponibles.ItemsSource = brl.SelectHorarios().DefaultView;
                dgvHorariosDisponibles.Columns[0].Visibility = Visibility.Hidden;
                dgvHorariosDisponibles.Columns[11].Visibility = Visibility.Hidden;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        static DataTable ConvertToDataTable(List<Telefono> lista)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("IdTelefono");
            dt.Columns.Add("Numero");
            dt.Columns.Add("Tipo");
            dt.Columns.Add("IdPersona");

            foreach (var item in lista)
            {
                var row = dt.NewRow();
                row["IdTelefono"] = item.IdTelefono;
                row["Numero"] = item.Numero;
                row["Tipo"] = item.Tipo;
                row["IdPersona"] = item.IdPersona;
                dt.Rows.Add(row);
            }
            return dt;
        }

        static DataTable ConvertToDataTableHorarios(List<HorarioEmpleado> lista)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("IdHorario");
            dt.Columns.Add("Lunes");
            dt.Columns.Add("Martes");
            dt.Columns.Add("Miercoles");
            dt.Columns.Add("Jueves");
            dt.Columns.Add("Viernes");
            dt.Columns.Add("Sabado");
            dt.Columns.Add("Domingo");
            dt.Columns.Add("HoraInicio");
            dt.Columns.Add("HoraFin");

            foreach (var item in lista)
            {
                var row = dt.NewRow();
                row["IdHorario"] = item.IdHorario;
                row["Lunes"] = item.Lunes;
                row["Martes"] = item.Martes;
                row["Miercoles"] = item.Miercoles;
                row["Jueves"] = item.Jueves;
                row["Viernes"] = item.Viernes;
                row["Sabado"] = item.Sabado;
                row["Domingo"] = item.Domingo;
                row["HoraInicio"] = item.HoraInicio;
                row["HoraFin"] = item.HoraFin;

                dt.Rows.Add(row);
            }
            return dt;
        }
        #endregion
        private void lbxNumeros_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(1);

            LoadDatagrid();
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(2);

            LoadDatagrid();
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {

            brl.Delete(Convert.ToInt32(txtId.Text));
            LoadDatagrid();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            switch (operacion)
            {
                case 1:
                    if (cmbSexo.Text == "Masculino")
                    {
                        sexo = 'M';
                    }
                    else
                    {
                        sexo = 'F';
                    }
                    if (cmbRol.Text == "Admin")
                    {
                        rol='A';
                    }
                    else
                    {
                        rol = 'S';
                    }

                    if (txtEmail.Text!=string.Empty && txtDocumento.Text!=string.Empty &&
                        txtGrado.Text != string.Empty && txtNombreEmpleado.Text != string.Empty &&
                        txtPassword.Password != string.Empty && txtPrimerApellido.Text != string.Empty &&
                        txtSalario.Text != string.Empty && txtSegundoApellido.Text != string.Empty)
                    {
                        txtNombreEmpleado.Text.Trim();
                        txtPrimerApellido.Text.Trim();
                        txtSegundoApellido.Text.Trim();
                        txtNombreUsuario.Text.Trim();

                        if (Validacion.onlyLettersAndSpaces(txtNombreEmpleado.Text) && Validacion.onlyLettersAndSpaces(txtPrimerApellido.Text) && Validacion.onlyLettersAndSpaces(txtSegundoApellido.Text)
                            && Validacion.ValidarCorreo(txtEmail.Text))
                        {
                            pers = new Persona(txtNombreEmpleado.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, sexo, Convert.ToDateTime(dtFechaNacimiento.SelectedDate.ToString()), txtDocumento.Text, telefonosEmpleado);
                            emp = new Empleado(txtGrado.Text, double.Parse(txtSalario.Text), horarios);
                            MD5 md5Hash = MD5.Create();// revisar si esto funciona
                            usu = new Usuario(txtNombreUsuario.Text, GetMd5Hash(md5Hash, txtPassword.Password).ToString(), rol.ToString(), txtEmail.Text);
                            brl = new EmpleadoBRL(pers, emp, usu);
                            brl.Insert();
                            lblAnuncio.Content = "Empleado insertado con exito";
                        }
                        else
                        {
                            MessageBox.Show("Formato de nombres incorrecto");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Todos los campos deben de estar llenados");
                    }
                    break;
                case 2:
                    if (cmbSexo.Text == "Masculino")
                    {
                        sexo = 'M';
                    }
                    else
                    {
                        sexo = 'F';
                    }
                    if (cmbRol.Text == "Admin")
                    {
                        rol = 'A';
                    }
                    else
                    {
                        rol = 'S';
                    }
                    if (txtEmail.Text != string.Empty && txtDocumento.Text != string.Empty &&
                        txtGrado.Text != string.Empty && txtNombreEmpleado.Text != string.Empty &&
                        txtPassword.Password != string.Empty && txtPrimerApellido.Text != string.Empty &&
                        txtSalario.Text != string.Empty && txtSegundoApellido.Text != string.Empty)
                    {
                        txtNombreEmpleado.Text.Trim();
                        txtPrimerApellido.Text.Trim();
                        txtSegundoApellido.Text.Trim();
                        txtNombreUsuario.Text.Trim();

                        if (Validacion.onlyLettersAndSpaces(txtNombreEmpleado.Text) && Validacion.onlyLettersAndSpaces(txtPrimerApellido.Text) && Validacion.onlyLettersAndSpaces(txtSegundoApellido.Text)
                            && Validacion.ValidarCorreo(txtEmail.Text))
                        {
                            pers = new Persona(txtNombreEmpleado.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, sexo, Convert.ToDateTime(dtFechaNacimiento.SelectedDate.ToString()), txtDocumento.Text, telefonosEmpleado);
                            pers.IdPersona = Convert.ToInt32(txtId.Text);
                            emp = new Empleado(txtGrado.Text, double.Parse(txtSalario.Text));
                            emp.IdEmpleado = Convert.ToInt32(txtId.Text);
                            MD5 md5Hash2 = MD5.Create();// revisar si esto funciona
                            usu = new Usuario(txtNombreUsuario.Text, GetMd5Hash(md5Hash2, txtPassword.Password).ToString(), rol.ToString(), txtEmail.Text);
                            brl = new EmpleadoBRL(pers, emp, usu);
                            brl.Update();
                            lblAnuncio.Content = "Paciente actualizado con exito";
                        }
                        else
                        {
                            MessageBox.Show("Formato de nombres incorrecto");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Todos los campos deben de estar llenados");
                    }
                    break;
            }
            DesHabilitar();
            LoadDatagrid();
        }
        static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DesHabilitar();
        }

        private void btnTelefono_Click(object sender, RoutedEventArgs e)
        {
            if (cmbTipo.Text == "celular")
            {
                tipoTel = 'C';
            }
            else
            {
                tipoTel = 'F';
            }
            Telefono telefono = new Telefono(txtNumero.Text, tipoTel);
            telefonosEmpleado.Add(telefono);
            dgvTelefonos.ItemsSource = ConvertToDataTable(telefonosEmpleado).DefaultView;
            dgvTelefonos.Columns[0].Visibility = Visibility.Hidden;
            dgvTelefonos.Columns[3].Visibility = Visibility.Hidden;

            try
            {

                brl = new EmpleadoBRL();
                dgvDatos.ItemsSource = brl.Select().DefaultView;
                dgvDatos.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void BtnCollapseMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Visible;
            btnCollapseMenu.Visibility = Visibility.Collapsed;
            //imgLogo.Visibility = Visibility.Collapsed;
        }

        private void BtnOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Collapsed;
            btnCollapseMenu.Visibility = Visibility.Visible;
            //imgLogo.Visibility = Visibility.Visible;
        }

        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void dgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvDatos.Items.Count > 0 && dgvDatos.SelectedItem != null)
            {
                try
                {
                    emp = new Empleado();
                    DataRowView dataRow = (DataRowView)dgvDatos.SelectedItem;
                    id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    brl = new EmpleadoBRL();
                    pers = brl.Get(id);
                    emp = brl.Get2(id);
                    usu = brl.Get3(id);
                    txtId.Text = id.ToString();
                    txtNombreEmpleado.Text = pers.Nombres;
                    txtPrimerApellido.Text = pers.PrimerApellido;
                    txtSegundoApellido.Text = pers.SegundoApellido;
                    cmbSexo.Text = pers.Sexo.ToString();
                    dtFechaNacimiento.SelectedDate = pers.FechaNacimiento;
                    txtDocumento.Text = pers.Documento;
                    txtGrado.Text = emp.Grado;
                    txtSalario.Text = emp.Salario.ToString();
                    txtNombreUsuario.Text = usu.NombreUsuario;
                    txtEmail.Text = usu.Email;
                    cmbRol.Text = usu.Rol;
                    //foreach (var item in brl.GetTelefono(id).DefaultView)
                    //{
                    //    Telefono telefono = new Telefono(item);
                    //    telefonosClientes.Add(telefono); 
                    //}


                    dgvTelefonos.ItemsSource = brl.GetTelefono(id).DefaultView;
                    dgvTelefonos.Columns[0].Visibility = Visibility.Hidden;
                    dgvHorariosSeleccionados.ItemsSource = brl.GetHorarios(id).DefaultView;
                    dgvHorariosSeleccionados.Columns[0].Visibility = Visibility.Hidden;
                    //dgvTelefonos.Columns[3].Visibility = Visibility.Hidden;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void dgvTelefonos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvTelefonos.Items.Count > 0 && dgvTelefonos.SelectedItem != null)
            {
                try
                {
                    DataRowView dataRow = (DataRowView)dgvTelefonos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    brl = new EmpleadoBRL();
                    tel = brl.GetUnTelefono(id);
                    txtDocumento.Text = pers.Documento;
                    txtNumero.Text = tel.Numero;
                    if (tel.Tipo == 'C')
                    {
                        cmbTipo.Text = "celular";
                    }
                    else
                    {
                        cmbTipo.Text = "Fijo";
                    }
                    //dgvTelefonos.ItemsSource = brl.GetTelefono(id).DefaultView;
                    //dgvTelefonos.Columns[0].Visibility = Visibility.Hidden;
                    //dgvTelefonos.Columns[3].Visibility = Visibility.Hidden;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadDatagrid();
        }

        private void btnGenerarPassword_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnHorarios_Click(object sender, RoutedEventArgs e)
        {
            wpfHorario form = new wpfHorario();
            form.ShowDialog();
        }

        private void dgvHorariosDisponibles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvHorariosDisponibles.Items.Count > 0 && dgvHorariosDisponibles.SelectedItem != null)
            {
                
                try
                {

                    DataRowView dataRow = (DataRowView)dgvHorariosDisponibles.SelectedItem;
                    int idEmpleado = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    id = idEmpleado;
                   // Empleado empl = new Empleado(int.Parse(dataRow.Row.ItemArray[0].ToString()), dataRow.Row.ItemArray[1].ToString(), dataRow.Row.ItemArray[2].ToString());
                    HorarioEmpleado hor = new HorarioEmpleado(int.Parse(dataRow.Row.ItemArray[0].ToString()), char.Parse(dataRow.Row.ItemArray[1].ToString()), char.Parse(dataRow.Row.ItemArray[2].ToString()), char.Parse(dataRow.Row.ItemArray[3].ToString()), char.Parse(dataRow.Row.ItemArray[4].ToString()), char.Parse(dataRow.Row.ItemArray[5].ToString()), char.Parse(dataRow.Row.ItemArray[6].ToString()), char.Parse(dataRow.Row.ItemArray[7].ToString()), DateTime.Parse(dataRow.Row.ItemArray[8].ToString()), DateTime.Parse(dataRow.Row.ItemArray[9].ToString()));
                    horarios.Add(hor);
                    dgvHorariosSeleccionados.ItemsSource = ConvertToDataTableHorarios(horarios).DefaultView;
                    dgvHorariosSeleccionados.Columns[0].Visibility = Visibility.Hidden;



                    try
                    {

                        //brl = new HorarioEmpleadoBRL();
                        //dgvDatos.ItemsSource = brl.Select().DefaultView;
                        //dgvDatos.Columns[0].Visibility = Visibility.Hidden;
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void dgvHorariosSeleccionados_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
