﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Common;
using BRL;
using System.Data;
using System.IO;

namespace MandalaWPF.usuario
{
    /// <summary>
    /// Lógica de interacción para winAdministrarUsuario.xaml
    /// </summary>
    public partial class winAdministrarUsuario : Window
    {
        public winAdministrarUsuario()
        {
            InitializeComponent();
        }
        byte operacion = 0;
        UsuarioBRL brl;
       Usuario user;
       string nombreUsuario = Sesion.VerUsuario();
        #region Metodos
        private void Habilitar(byte operacion)
        {
            btnGuardar.IsEnabled = true;
            btnCancelar.IsEnabled = true;
            btnInsertar.IsEnabled = false;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;
            txtNombreUsuario.IsEnabled = true;
            txtRol.IsEnabled = true;
            txtPassword.IsEnabled = true;
            txtNombreUsuario.Focus();
            this.operacion = operacion;
            lblAnuncio.Visibility = Visibility.Visible;
            lblAnuncio.Content = "";
            lblAnuncio.Content = "";


        }
        private void DesHabilitar()
        {
            btnGuardar.IsEnabled = false;
            btnCancelar.IsEnabled = false;
            btnInsertar.IsEnabled = true;
            btnModificar.IsEnabled = true;
            btnEliminar.IsEnabled = true;
            txtNombreUsuario.IsEnabled = false;
            txtPassword.IsEnabled = false;
            txtRol.IsEnabled = false;
            txtNombreUsuario.Text = "";
            txtPassword.Text = "";
            txtRol.Text = "";

        }
        private void LoadDatagrid()
        {
            try
            {
                brl = new UsuarioBRL();
                dgvDatos.ItemsSource = brl.Select().DefaultView;
                dgvDatos.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        private void LoadDatagrid2()
        {
            try
            {
                brl = new UsuarioBRL();
                dgvDatos.ItemsSource = brl.Select().DefaultView;
                dgvDatos.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        public static string GenerarCadenaLetras(int maxLenght)
        {
            try
            {
                var obj = new Random();
                const string allowedChars = "abcdefghijklmnopqrstuvwxyz";
                var longitud = allowedChars.Length;
                var res = "";

                for (int i = 0; i < maxLenght; i++)
                {
                    res += allowedChars[obj.Next(longitud)];
                }
                return res;

            }
            catch (Exception)
            {
                throw new Exception("No se puede generar una cadena aleatoria");
            }
        }
        public static string GenerarCadenaNumeros(int maxLenght)
        {
            try
            {
                var obj = new Random();
                const string allowedChars = "0123456789";
                var longitud = allowedChars.Length;
                var res = "";

                for (int i = 0; i < maxLenght; i++)
                {
                    res += allowedChars[obj.Next(longitud)];
                }
                return res;

            }
            catch (Exception)
            {
                throw new Exception("No se puede generar una cadena aleatoria");
            }
        }
        #endregion

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DesHabilitar();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadDatagrid();
           if(Sesion.rolSesion!="administrador")
           {
               btnEliminar.Visibility = Visibility.Collapsed;
               btnInsertar.Visibility = Visibility.Collapsed;

           }
        }
        static string BytesToString(byte[] bytes)
        {
            using (var stream = new MemoryStream(bytes))
            {
                using (var streamReader = new StreamReader(stream))
                {
                    return streamReader.ReadToEnd();
                }
            }
        }
        private void dgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UsuarioBRL brl;
            if (dgvDatos.Items.Count > 0 && dgvDatos.SelectedItem != null)
            {
                try
                {
                    DataRowView dataRow = (DataRowView)dgvDatos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    brl = new UsuarioBRL();
                    user = brl.Get(id);
                    txtNombreUsuario.Text = user.NombreUsuario;
                    txtRol.Text = user.Rol;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Collapsed;
            btnCollapseMenu.Visibility = Visibility.Visible;
            imgLogo.Visibility = Visibility.Visible;
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(2);

            LoadDatagrid();
            if (Sesion.rolSesion != "administrador")
            {
                LoadDatagrid2();
                txtNombreUsuario.Text = Sesion.usuarioSesion;
                txtRol.Text = Sesion.rolSesion;
                dgvDatos.Visibility = Visibility.Collapsed;
            }
        }

        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(1);

            LoadDatagrid();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            UsuarioBRL brl;
            switch (operacion)
            {
                case 1: txtNombreUsuario.Text = txtNombreUsuario.Text.Trim();
                    txtRol.Text = txtRol.Text.Trim();
                    txtPassword.Text = txtPassword.Text.Trim();
                    if (Validacion.onlyLettersAndSpaces(txtRol.Text))
                    {
                        try
                        {
                            user = new Usuario(txtNombreUsuario.Text, txtPassword.Text, txtRol.Text,"ejemplo@gmail.com");
                            brl = new UsuarioBRL(user);
                            if (!brl.Existe(txtNombreUsuario.Text))
                            {
                                brl.Insert();
                                lblAnuncio.Content = "Registro insertado con exito";
                            }
                            else
                            {
                                lblAnuncio.Content = "Registro duplicado";

                            }
                        }
                        catch (Exception ex)
                        {
                            lblAnuncio.Content = (ex.Message);
                        }
                        finally
                        {
                            LoadDatagrid();
                        }

                    }
                    else
                    {
                        lblAnuncio.Content = ("solo se admiten letras y espacios en Rol de usuario");
                    }
                    break;
                case 2: txtNombreUsuario.Text = txtNombreUsuario.Text.Trim();
                    txtRol.Text = txtRol.Text.Trim();
                    txtPassword.Text = txtPassword.Text.Trim();
                    if ( Validacion.onlyLettersAndSpaces(txtRol.Text))
                    {
                        try
                        {
                            //  trat = new Tratamiento(txtNombreTratamiento.Text, txtDescripcion.Text, Convert.ToDecimal(txtPrecio.Text));

                            user.NombreUsuario = txtNombreUsuario.Text;
                            user.Rol = txtRol.Text;
                            user.Password = txtPassword.Text;

                            brl = new UsuarioBRL(user);
                            if (brl.Existe(txtNombreUsuario.Text)) /// le quite el ! a brl.existe 

                            //if (!brl.Existe(txtNombreTratamiento.Text) || ((brl.Existe(txtNombreTratamiento.Text)&&(trat.NombreTratamiento==txtNombreTratamiento.Text))))
                            {
                                brl.Update2();
                                lblAnuncio.Content = "Registro Actualizado con exito";
                            }
                            else
                            {
                                lblAnuncio.Content = "EL usuario no existe";

                            }


                        }
                        catch (Exception ex)
                        {
                            lblAnuncio.Content = (ex.Message);
                        }
                        finally
                        {
                            LoadDatagrid();
                        }

                    }
                    else
                    {
                        lblAnuncio.Content = ("solo se admiten letras y espacios en Rol de usuario");
                    }
                    break;
            }
            DesHabilitar();

        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            txtNombreUsuario.Text = txtNombreUsuario.Text.Trim();
            txtRol.Text = txtRol.Text.Trim();
            txtPassword.Text = txtPassword.Text.Trim();
            if (txtNombreUsuario.Text!=""&&txtNombreUsuario.Text!=nombreUsuario)
            {
                try
                {
                    user = new Usuario(txtNombreUsuario.Text, txtPassword.Text, txtRol.Text,"ejemplo@gmail.com");
                    brl = new UsuarioBRL(user);
                    if (brl.Existe(txtNombreUsuario.Text))
                    {
                        brl.Delete();
                        lblAnuncio.Content = "Registro eliminado con exito";
                    }
                    else
                    {
                        lblAnuncio.Content = "Registro no encontrado";

                    }
                }
                catch (Exception ex)
                {
                    lblAnuncio.Content = (ex.Message);
                }
                finally
                {
                    LoadDatagrid();
                }

            }
            else
            {
                lblAnuncio.Content = ("Debe asignar un usuario a eliminar o seleccionar un usuario diferente al tuyo");
            }
        }

        private void BtnCollapseMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Visible;
            btnCollapseMenu.Visibility = Visibility.Collapsed;
            imgLogo.Visibility = Visibility.Collapsed;
        }

        private void btnNombreAleatorio_Click(object sender, RoutedEventArgs e)
        {
            txtPassword.Text = GenerarCadenaLetras(3) + GenerarCadenaNumeros(3);
        }
    }
}
