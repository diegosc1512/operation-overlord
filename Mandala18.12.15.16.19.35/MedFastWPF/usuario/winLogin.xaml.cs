﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Common;
using BRL;
using System.Data;

namespace MandalaWPF.usuario
{
    /// <summary>
    /// Lógica de interacción para winLogin.xaml
    /// </summary>
    public partial class winLogin : Window
    {
        public winLogin()
        {
            InitializeComponent();
        }
        UsuarioBRL brl;
        byte contador = 0;
        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void txtPassword_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {

        }

        private void btnIniciar_Click(object sender, RoutedEventArgs e)
        {
            txtLogin.Text = txtLogin.Text.Trim();
            if(txtLogin.Text!=""&& txtPassword.Password!="")
            {
                try
                {
                    brl = new UsuarioBRL();
                    DataTable dt = brl.Login(txtLogin.Text, txtPassword.Password);

                    if (dt.Rows.Count>0)
                    {

                        Sesion.idSesion = int.Parse(dt.Rows[0][0].ToString());
                        Sesion.usuarioSesion=dt.Rows[0][1].ToString();
                        Sesion.rolSesion = dt.Rows[0][2].ToString();
                        this.Visibility = Visibility.Hidden;
                        MainWindow form = new MainWindow();
                        form.ShowDialog();


                    }
                    else
                    {
                        contador++;
                        txbMensajes.Visibility = Visibility.Visible;
                        txbMensajes.Text = "El usuario o la contraseña son incorrectos, "+"Intento N°"+contador;
                        txtPassword.Password = "";
                        txtLogin.Text = "";
                        if(contador>3)
                        {
                            this.Close();
                        }
                    }
                }
                catch (Exception ex )
                {
                    
                    txbMensajes.Text= ex.Message;
                    txbMensajes.Visibility = Visibility.Visible;
                }
            }
            else
            {
                txbMensajes.Text = "La contraseña o el usuario estan en blanco";
                txbMensajes.Visibility = Visibility.Visible;
            }

        }

        private void BtnOlvidePassword_Click(object sender, RoutedEventArgs e)
        {
            winRecuperarPassword form=new winRecuperarPassword();
            form.ShowDialog();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
