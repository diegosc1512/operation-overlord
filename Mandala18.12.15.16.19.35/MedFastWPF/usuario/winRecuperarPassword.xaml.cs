﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Common;
using BRL;
using System.Data;
using System.Diagnostics;
using System.Net.Mail;
using System.Net;



namespace MandalaWPF.usuario
{
    /// <summary>
    /// Lógica de interacción para winRecuperarPassword.xaml
    /// </summary>
    public partial class winRecuperarPassword : Window
    {
        public winRecuperarPassword()
        {
            InitializeComponent();
        }
        UsuarioBRL brl;
        byte contador = 0;
        Usuario user;
        #region metodos adicionales
        class SendMail
        {
            string emisor;
            public string Emisor
            {
                get { return emisor; }
                set { emisor = value; }
            }
            string pwd;
            public string Pwd
            {
                get { return pwd; }
                set { pwd = value; }
            }
            //Constructor
            public SendMail(string de, string pass)
            {
                emisor = de;
                pwd = pass;
            }
            /// <summary>
            /// Se envía un correo electronico con texto simple
            /// </summary>
            /// <param name="destinatario">Quien escribe el correo</param>
            /// <param name="asunto">Asunto del correo</param>
            /// <param name="correo">Cuerpo del correo (contenido)</param>
            public void sendMail(string destinatario, string asunto, string correo)
            {
                MailMessage msg = new MailMessage();
                //Quien escribe al correo
                msg.From = new MailAddress(emisor);
                //A quien va dirigido
                msg.To.Add(new MailAddress(destinatario));
                //Asunto
                msg.Subject = asunto;
                //Contenido del correo
                msg.Body = correo;
                //Servidor smtp de hotmail
                SmtpClient clienteSmtp = new SmtpClient();
                clienteSmtp.Host = "smtp.gmail.com";
                clienteSmtp.Port = 25;
                clienteSmtp.EnableSsl = false;
                clienteSmtp.UseDefaultCredentials = false;
                //Se envia el correo
                clienteSmtp.Credentials = new NetworkCredential(emisor, pwd);
                clienteSmtp.EnableSsl = true;
                try
                {
                    clienteSmtp.Send(msg);
                    //MessageBox.Show("Correo enviado");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al enviar el correo " + ex.Message);
                }
            }
        }
     
        public static string GenerarCadenaAleatoria(int maxLenght)
        {
            try
            {
                var obj = new Random();
                const string allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                var longitud = allowedChars.Length;
                var res = "";

                for (int i = 0; i < maxLenght; i++)
                {
                    res += allowedChars[obj.Next(longitud)];
                }
                return res;

            }
            catch (Exception)
            {
                throw new Exception("No se puede generar una cadena aleatoria");
            }
        }
        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void btnIniciar_Click(object sender, RoutedEventArgs e)
        {
            txtLogin.Text = txtLogin.Text.Trim();
            if (txtLogin.Text != "")
            {
                try
                {
                    user = new Usuario();
                    user.NombreUsuario = txtLogin.Text;
                    user.Password = GenerarCadenaAleatoria(5);
                    //txbMensajes.Text = user.Password;
                    SendMail m = new SendMail("mandala.system000@gmail.com", "Mandala1234");
                    m.sendMail("diegosc1512@gmail.com", "Nueva contraseña", "Al usuario: " + user.NombreUsuario + " ,se le otorgo la siguiente contraseña: " + user.Password + " , una vez haya accedido, cambiar la contraseña.");
                    brl = new UsuarioBRL(user);
                    //if (!brl.Existe(txtLogin.Text))
                    //{
                    brl.Update();
                    //txbMensajes.Text = "Registro Actualizado con exito";
                    //}
                    //else
                    //{
                    //    txbMensajes.Text = "Registro duplicado";
                    //}
                    txbMensajes.Text = "La contraseña se a cambiado exitosamente, por favor verificar su email.";
                    txbMensajes.Visibility = Visibility.Visible;
                }
                catch (Exception ex)
                {
                    txbMensajes.Text = (ex.Message);
                }
                finally
                {
                    //LoadDatagrid();
                }
            }
            else
            {
                txbMensajes.Text = "El usuario estan en blanco";
                txbMensajes.Visibility = Visibility.Visible;
            }



        }


        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }
    }
}
