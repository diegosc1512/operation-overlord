﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Common;
using BRL;

namespace MandalaWPF
{
    /// <summary>
    /// Lógica de interacción para winAdmAgendaAtencion.xaml
    /// </summary>
    public partial class winAdmAgendaAtencion : Window
    {
        public winAdmAgendaAtencion()
        {
            InitializeComponent();
        }
        byte operacion = 0;
        AgendaAtencion agat;
        AgendaAtencionBRL brl;

        #region metodos
        private void Habilitar(byte operacion)
        {
            btnGuardar.IsEnabled = true;
            btnCancelar.IsEnabled = true;
            btnInsertar.IsEnabled = false;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;
            txtHoraInicio.IsEnabled = true;
            txtHoraFin.IsEnabled = true;
            this.operacion = operacion;
            lblAnuncio.Visibility = Visibility.Visible;
            lblAnuncio.Content = "";
            lblAnuncio.Content = "";

        }
        private void DesHabilitar()
        {
            btnGuardar.IsEnabled = false;
            btnCancelar.IsEnabled = false;
            btnInsertar.IsEnabled = true;
            btnModificar.IsEnabled = true;
            btnEliminar.IsEnabled = true;
            txtHoraInicio.IsEnabled = false;
            txtHoraFin.IsEnabled = false;

        }
        #endregion 

        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void BtnInicio_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnCollapseMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Visible;
            btnCollapseMenu.Visibility = Visibility.Collapsed;
            imgLogo.Visibility = Visibility.Collapsed;
        }
        private void BtnOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Collapsed;
            btnCollapseMenu.Visibility = Visibility.Visible;
            imgLogo.Visibility = Visibility.Visible;
        }

        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(1);
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(2);
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            switch (operacion)
            {
                case 1: 
                        try
                        {
                            DateTime entrada = Convert.ToDateTime(txtHoraInicio.Text);
                            DateTime salida = entrada.AddHours(12);
                            DateTime entrada2= Convert.ToDateTime(txtHoraFin.Text);
                            DateTime salida2 = entrada.AddHours(12);
                            agat = new AgendaAtencion(Convert.ToDateTime(dtpDia.SelectedDate.ToString()), Convert.ToDateTime(salida.ToString("hh:mm")), Convert.ToDateTime(salida2.ToString("hh:mm")));
                            //agat = new AgendaAtencion(Calendar.SelectedDateProperty, Convert.ToDateTime(cbxHoraInicio.SelectedItem.ToString()), Convert.ToDateTime(cbxHoraFin.SelectedItem.ToString()));
                            brl = new AgendaAtencionBRL(agat);
                            brl.Insert();

                            lblAnuncio.Content = "Registro insertado con exito";
                            dataTratamiento.ItemsSource= brl.Select().DefaultView;
                        }
                        catch (Exception ex)
                        {
                            lblAnuncio.Content = (ex.Message);
                        }

                    break;
                case 2: try
                    {
                        DateTime entrada = Convert.ToDateTime(txtHoraInicio.Text);
                        DateTime salida = entrada.AddHours(12);
                        DateTime entrada2 = Convert.ToDateTime(txtHoraFin.Text);
                        DateTime salida2 = entrada.AddHours(12);
                        agat = new AgendaAtencion(Convert.ToDateTime(dtpDia.SelectedDate.ToString()), Convert.ToDateTime(salida.ToString("hh:mm")), Convert.ToDateTime(salida2.ToString("hh:mm")));
                        brl = new AgendaAtencionBRL(agat);
                        brl.Update();

                        lblAnuncio.Content = "MOdificacion insertada con exito";
                        dataTratamiento.ItemsSource = brl.Select().DefaultView;
                    }
                    catch (Exception ex)
                    {
                        lblAnuncio.Content = (ex.Message);
                    }
                    break;
            }
            DesHabilitar();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DesHabilitar();

        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Activated(object sender, EventArgs e)
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DesHabilitar();
        }
    }
}
