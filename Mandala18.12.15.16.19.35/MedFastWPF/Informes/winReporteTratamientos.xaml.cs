﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BRL;
namespace MandalaWPF
{
    /// <summary>
    /// Interaction logic for WinReporteTratamientos.xaml
    /// </summary>
    public partial class WinReporteTratamientos : Window
    {
        public WinReporteTratamientos()
        {
            InitializeComponent();
            _reportViewer.Load += ReportViewer_Load;
        }
        private bool _isReportViewerLoaded;

        private void ReportViewer_Load(object sender, EventArgs e)
        {
            if (!_isReportViewerLoaded)
            {
                Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new
                Microsoft.Reporting.WinForms.ReportDataSource();
                Informes.DSTratamientos dataset = new Informes.DSTratamientos();

                dataset.BeginInit();

                reportDataSource1.Name = "DataSet1";
                    
                TratamientoBRL brl = new TratamientoBRL();
                reportDataSource1.Value = brl.Select().DefaultView;
                this._reportViewer.LocalReport.DataSources.Add(reportDataSource1);

                this._reportViewer.LocalReport.ReportPath = "../../Informes/repTratamiento.rdlc";
                dataset.EndInit();

                _reportViewer.RefreshReport();
                _isReportViewerLoaded = true;
            }

        }
    }
}
