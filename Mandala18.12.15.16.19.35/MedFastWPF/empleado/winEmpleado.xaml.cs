﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BRL;
using Common;
using System.Data;

namespace MandalaWPF.empleado
{
    /// <summary>
    /// Lógica de interacción para winEmpleado.xaml
    /// </summary>
    public partial class winEmpleado : Window
    {
        public winEmpleado()
        {
            InitializeComponent();
        }
        byte operacion = 0;
        EmpleadoBRL brl;
        Empleado emp;
        Persona pers;
        Telefono tel;
        DataTable dt = new DataTable();
        public List<Telefono> telefonosEmpleado = new List<Telefono>();
        private char tipoTel;
        char sexo;


        #region Metodos
        private void Habilitar(byte operacion)
        {
            btnGuardar.IsEnabled = true;
            btnCancelar.IsEnabled = true;
            btnInsertar.IsEnabled = false;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;
            txtDocumento.IsEnabled = true;
            txtNombreEmpleado.IsEnabled = true;
            txtPrimerApellido.IsEnabled = true;
            txtSegundoApellido.IsEnabled = true;
            cmbSexo.IsEnabled = true;
            dtFechaNacimiento.IsEnabled = true;
            txtSalario.IsEnabled = true;
            txtGrado.IsEnabled = true;
            btnTelefono.IsEnabled = true;
            txtNombreEmpleado.Focus();
            this.operacion = operacion;
            lblAnuncio.Visibility = Visibility.Visible;
            lblAnuncio.Content = "";
            lblAnuncio.Content = "";

        }
        private void DesHabilitar()
        {
            btnGuardar.IsEnabled = false;
            btnCancelar.IsEnabled = false;
            btnInsertar.IsEnabled = true;
            btnModificar.IsEnabled = true;
            btnEliminar.IsEnabled = true;
            txtDocumento.IsEnabled = false;
            txtNombreEmpleado.IsEnabled = false;
            txtPrimerApellido.IsEnabled = false;
            txtSegundoApellido.IsEnabled = false;
            cmbSexo.IsEnabled = false;
            dtFechaNacimiento.IsEnabled = false;
            txtSalario.IsEnabled = false;
            txtGrado.IsEnabled = false;
            btnTelefono.IsEnabled = false;
            txtNombreEmpleado.Text = "";
            txtPrimerApellido.Text = "";
            txtSegundoApellido.Text = "";
            cmbSexo.Text = "";
            txtDocumento.Text = "";
            txtSalario.Text = "";
            txtGrado.Text = "";

        }
        private void LoadDatagrid()
        {
            try
            {
                brl = new EmpleadoBRL();
                dgvDatos.ItemsSource = brl.Select().DefaultView;
                dgvDatos.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        static DataTable ConvertToDataTable(List<Telefono> lista)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("IdTelefono");
            dt.Columns.Add("Numero");
            dt.Columns.Add("Tipo");
            dt.Columns.Add("IdPersona");

            foreach (var item in lista)
            {
                var row = dt.NewRow();
                row["IdTelefono"] = item.IdTelefono;
                row["Numero"] = item.Numero;
                row["Tipo"] = item.Tipo;
                row["IdPersona"] = item.IdPersona;
                dt.Rows.Add(row);
            }
            return dt;
        }
        #endregion
        private void lbxNumeros_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(1);

            LoadDatagrid();
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(2);

            LoadDatagrid();
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {

            brl.Delete(Convert.ToInt32(txtId.Text));
            LoadDatagrid();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            switch (operacion)
            {
                case 1:
                    if (cmbSexo.Text == "Masculino")
                    {
                        sexo = 'M';
                    }
                    else
                    {
                        sexo = 'F';
                    }
                    pers = new Persona(txtNombreEmpleado.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, sexo, Convert.ToDateTime(dtFechaNacimiento.SelectedDate.ToString()), txtDocumento.Text, telefonosEmpleado);
                    emp = new Empleado(txtGrado.Text, double.Parse(txtSalario.Text));
                    brl = new EmpleadoBRL(pers, emp);
                    brl.Insert();
                    lblAnuncio.Content = "Cliente insertado con exito";
                    break;
                case 2:
                    if (cmbSexo.Text == "Masculino")
                    {
                        sexo = 'M';
                    }
                    else
                    {
                        sexo = 'F';
                    }
                    pers = new Persona(txtNombreEmpleado.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, sexo, Convert.ToDateTime(dtFechaNacimiento.SelectedDate.ToString()), txtDocumento.Text, telefonosEmpleado);
                    pers.IdPersona = Convert.ToInt32(txtId.Text);
                    emp = new Empleado(txtGrado.Text, double.Parse(txtSalario.Text));
                    emp.IdEmpleado = Convert.ToInt32(txtId.Text);
                    brl = new EmpleadoBRL(pers, emp);
                    brl.Update();
                    lblAnuncio.Content = "Cliente actualizado con exito";
                    break;
            }
            DesHabilitar();
            LoadDatagrid();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DesHabilitar();
        }

        private void btnTelefono_Click(object sender, RoutedEventArgs e)
        {
            if (cmbTipo.Text == "celular")
            {
                tipoTel = 'C';
            }
            else
            {
                tipoTel = 'F';
            }
            Telefono telefono = new Telefono(txtNumero.Text, tipoTel);
            telefonosEmpleado.Add(telefono);
            dgvTelefonos.ItemsSource = ConvertToDataTable(telefonosEmpleado).DefaultView;
            dgvTelefonos.Columns[0].Visibility = Visibility.Hidden;
            dgvTelefonos.Columns[3].Visibility = Visibility.Hidden;

            try
            {

                brl = new EmpleadoBRL();
                dgvDatos.ItemsSource = brl.Select().DefaultView;
                dgvDatos.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void BtnCollapseMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Visible;
            btnCollapseMenu.Visibility = Visibility.Collapsed;
            imgLogo.Visibility = Visibility.Collapsed;
        }

        private void BtnOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Collapsed;
            btnCollapseMenu.Visibility = Visibility.Visible;
            imgLogo.Visibility = Visibility.Visible;
        }

        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void dgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvDatos.Items.Count > 0 && dgvDatos.SelectedItem != null)
            {
                try
                {
                    DataRowView dataRow = (DataRowView)dgvDatos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    brl = new EmpleadoBRL();
                    pers = brl.Get(id);
                    emp = brl.Get2(id);
                    txtId.Text = id.ToString();
                    txtNombreEmpleado.Text = pers.Nombres;
                    txtPrimerApellido.Text = pers.PrimerApellido;
                    txtSegundoApellido.Text = pers.SegundoApellido;
                    cmbSexo.Text = pers.Sexo.ToString();
                    dtFechaNacimiento.SelectedDate = pers.FechaNacimiento;
                    txtDocumento.Text = pers.Documento;
                    txtGrado.Text = emp.Grado;
                    txtSalario.Text = emp.Salario.ToString();
                    //foreach (var item in brl.GetTelefono(id).DefaultView)
                    //{
                    //    Telefono telefono = new Telefono(item);
                    //    telefonosClientes.Add(telefono); 
                    //}


                    dgvTelefonos.ItemsSource = brl.GetTelefono(id).DefaultView;
                    dgvTelefonos.Columns[0].Visibility = Visibility.Hidden;
                    dgvTelefonos.Columns[3].Visibility = Visibility.Hidden;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void dgvTelefonos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvTelefonos.Items.Count > 0 && dgvTelefonos.SelectedItem != null)
            {
                try
                {
                    DataRowView dataRow = (DataRowView)dgvTelefonos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    brl = new EmpleadoBRL();
                    tel = brl.GetUnTelefono(id);
                    txtDocumento.Text = pers.Documento;
                    txtNumero.Text = tel.Numero;
                    if (tel.Tipo == 'C')
                    {
                        cmbTipo.Text = "celular";
                    }
                    else
                    {
                        cmbTipo.Text = "Fijo";
                    }
                    //dgvTelefonos.ItemsSource = brl.GetTelefono(id).DefaultView;
                    //dgvTelefonos.Columns[0].Visibility = Visibility.Hidden;
                    //dgvTelefonos.Columns[3].Visibility = Visibility.Hidden;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadDatagrid();
        }
    }
}
