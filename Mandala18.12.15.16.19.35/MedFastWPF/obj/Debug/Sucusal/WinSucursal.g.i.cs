﻿#pragma checksum "..\..\..\Sucusal\WinSucursal.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "F1065D8AB98D474541220B7CFFEB3536FBA9DFC5"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using Microsoft.Maps.MapControl.WPF;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace MandalaWPF.Sucusal {
    
    
    /// <summary>
    /// WinSucursal
    /// </summary>
    public partial class WinSucursal : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 37 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSalir;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GridMostrar;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Maps.MapControl.WPF.Map miMapa;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSatelite;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAcercar;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAlejar;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNombreSucursal;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtLatitud;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtLongitud;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkBotones;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGuardar;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancelar;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GridMenu;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnOpenMenu;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCollapseMenu;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnInsertar;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnModificar;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnEliminar;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGuardar2;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancelar2;
        
        #line default
        #line hidden
        
        
        #line 104 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnTratamientoReporte;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgLogo;
        
        #line default
        #line hidden
        
        
        #line 109 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgvDatos;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\..\Sucusal\WinSucursal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblAnuncio;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/MandalaWPF;component/sucusal/winsucursal.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Sucusal\WinSucursal.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.btnSalir = ((System.Windows.Controls.Button)(target));
            
            #line 37 "..\..\..\Sucusal\WinSucursal.xaml"
            this.btnSalir.Click += new System.Windows.RoutedEventHandler(this.BtnSalir_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.GridMostrar = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.miMapa = ((Microsoft.Maps.MapControl.WPF.Map)(target));
            
            #line 46 "..\..\..\Sucusal\WinSucursal.xaml"
            this.miMapa.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.miMapa_MouseDoubleClick);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btnSatelite = ((System.Windows.Controls.Button)(target));
            
            #line 48 "..\..\..\Sucusal\WinSucursal.xaml"
            this.btnSatelite.Click += new System.Windows.RoutedEventHandler(this.btnSatelite_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btnAcercar = ((System.Windows.Controls.Button)(target));
            
            #line 49 "..\..\..\Sucusal\WinSucursal.xaml"
            this.btnAcercar.Click += new System.Windows.RoutedEventHandler(this.btnAcercar_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.btnAlejar = ((System.Windows.Controls.Button)(target));
            
            #line 52 "..\..\..\Sucusal\WinSucursal.xaml"
            this.btnAlejar.Click += new System.Windows.RoutedEventHandler(this.btnAlejar_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.txtNombreSucursal = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.txtLatitud = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.txtLongitud = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.stkBotones = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 11:
            this.btnGuardar = ((System.Windows.Controls.Button)(target));
            
            #line 72 "..\..\..\Sucusal\WinSucursal.xaml"
            this.btnGuardar.Click += new System.Windows.RoutedEventHandler(this.btnGuardar_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.btnCancelar = ((System.Windows.Controls.Button)(target));
            
            #line 73 "..\..\..\Sucusal\WinSucursal.xaml"
            this.btnCancelar.Click += new System.Windows.RoutedEventHandler(this.btnCancelar_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.GridMenu = ((System.Windows.Controls.Grid)(target));
            return;
            case 14:
            this.btnOpenMenu = ((System.Windows.Controls.Button)(target));
            
            #line 79 "..\..\..\Sucusal\WinSucursal.xaml"
            this.btnOpenMenu.Click += new System.Windows.RoutedEventHandler(this.BtnOpenMenu_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.btnCollapseMenu = ((System.Windows.Controls.Button)(target));
            
            #line 82 "..\..\..\Sucusal\WinSucursal.xaml"
            this.btnCollapseMenu.Click += new System.Windows.RoutedEventHandler(this.BtnCollapseMenu_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.btnInsertar = ((System.Windows.Controls.Button)(target));
            
            #line 88 "..\..\..\Sucusal\WinSucursal.xaml"
            this.btnInsertar.Click += new System.Windows.RoutedEventHandler(this.BtnInsertar_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.btnModificar = ((System.Windows.Controls.Button)(target));
            
            #line 91 "..\..\..\Sucusal\WinSucursal.xaml"
            this.btnModificar.Click += new System.Windows.RoutedEventHandler(this.btnModificar_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.btnEliminar = ((System.Windows.Controls.Button)(target));
            
            #line 94 "..\..\..\Sucusal\WinSucursal.xaml"
            this.btnEliminar.Click += new System.Windows.RoutedEventHandler(this.btnEliminar_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.btnGuardar2 = ((System.Windows.Controls.Button)(target));
            
            #line 98 "..\..\..\Sucusal\WinSucursal.xaml"
            this.btnGuardar2.Click += new System.Windows.RoutedEventHandler(this.btnGuardar_Click);
            
            #line default
            #line hidden
            return;
            case 20:
            this.btnCancelar2 = ((System.Windows.Controls.Button)(target));
            
            #line 101 "..\..\..\Sucusal\WinSucursal.xaml"
            this.btnCancelar2.Click += new System.Windows.RoutedEventHandler(this.btnCancelar_Click);
            
            #line default
            #line hidden
            return;
            case 21:
            this.btnTratamientoReporte = ((System.Windows.Controls.Button)(target));
            
            #line 104 "..\..\..\Sucusal\WinSucursal.xaml"
            this.btnTratamientoReporte.Click += new System.Windows.RoutedEventHandler(this.btnTratamientoReporte_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            this.imgLogo = ((System.Windows.Controls.Image)(target));
            return;
            case 23:
            this.dgvDatos = ((System.Windows.Controls.DataGrid)(target));
            
            #line 109 "..\..\..\Sucusal\WinSucursal.xaml"
            this.dgvDatos.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.dgvDatos_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 24:
            this.lblAnuncio = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

