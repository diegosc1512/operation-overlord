﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BRL;
using Common;

namespace MandalaWPF.cliente
{
    /// <summary>
    /// Lógica de interacción para winTelefono.xaml
    /// </summary>
    public partial class winTelefono : Window
    {
        public winTelefono()
        {
            InitializeComponent();
        }
        
        #region 
        private string numero;
        private char tipo;

        public char Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
        public string Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        //private Telefono telefono;


        public List<Telefono> telefonosClientes = new List<Telefono>();
         private char tipoTel;
        #endregion
        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            if (cmbTipo.Text=="celular")
            {
                tipoTel='C';
            }
            else
            {
                tipoTel='F';
            }
            Telefono telefono = new Telefono(txtNumero.Text, tipoTel);
            telefonosClientes.Add(telefono);
            //for (int i = 0; i < telefonosClientes.Count; i++)
            //{
            //    dgvDatos.Items.Add(telefonosClientes[i]);
            //}
            foreach (Telefono item in telefonosClientes)
            {
                dgvDatos.Items.Add(item);  
            }

            
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            //List<
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnCollapseMenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnOpenMenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {

        }

        private void dgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
