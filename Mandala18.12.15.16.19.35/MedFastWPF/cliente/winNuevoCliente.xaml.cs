﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BRL;
using Common;
using System.Data;

namespace MandalaWPF.cliente
{
    /// <summary>
    /// Lógica de interacción para winNuevoCliente.xaml
    /// </summary>
    public partial class winNuevoCliente : Window
    {
        public winNuevoCliente()
        {
            InitializeComponent();
        }
        byte operacion = 0;
        ClienteBRL brl;
        Cliente clie;
        Persona pers;
        Telefono tel;
        winTelefono fono;
        DataTable dt = new DataTable();
        public List<Telefono> telefonosClientes = new List<Telefono>();
        private char tipoTel;
        
        #region Metodos
        private void Habilitar(byte operacion)
        {
            btnGuardar.IsEnabled = true;
            btnCancelar.IsEnabled = true;
            btnInsertar.IsEnabled = false;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;
            txtDocumento.IsEnabled = true;
            txtNombreCliente.IsEnabled = true;
            txtPrimerApellido.IsEnabled = true;
            txtSegundoApellido.IsEnabled = true;
            cmbSexo.IsEnabled = true;
            dtFechaNacimiento.IsEnabled = true;
            txtRazon.IsEnabled = true;
            txtNit.IsEnabled = true;
            btnTelefono.IsEnabled = true;
            txtNombreCliente.Focus();
            this.operacion = operacion;
            lblAnuncio.Visibility = Visibility.Visible;
            lblAnuncio.Content = "";
            lblAnuncio.Content = "";

        }
        private void DesHabilitar()
        {
            btnGuardar.IsEnabled = false;
            btnCancelar.IsEnabled = false;
            btnInsertar.IsEnabled = true;
            btnModificar.IsEnabled = true;
            btnEliminar.IsEnabled = true;
            txtDocumento.IsEnabled = false;
            txtNombreCliente.IsEnabled = false;
            txtPrimerApellido.IsEnabled = false;
            txtSegundoApellido.IsEnabled = false;
            cmbSexo.IsEnabled = false;
            dtFechaNacimiento.IsEnabled = false;
            txtRazon.IsEnabled = false;
            txtNit.IsEnabled = false;
            btnTelefono.IsEnabled = false;
            txtNombreCliente.Text = "";
            txtPrimerApellido.Text = "";
            txtSegundoApellido.Text = "";
            cmbSexo.Text = "";
            txtDocumento.Text = "";
            txtRazon.Text = "";
            txtNit.Text = "";
            //txbFoto.Text = "Registrar imagen.";

        }
        private void LoadDatagrid()
        {
            try
            {
                brl = new ClienteBRL();
                dgvDatos.ItemsSource = brl.Select().DefaultView;
                dgvDatos.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
         static DataTable ConvertToDataTable(List<Telefono> lista)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("IdTelefono");
            dt.Columns.Add("Numero");
            dt.Columns.Add("Tipo");
            dt.Columns.Add("IdPersona");

            foreach (var item in lista)
            {
                var row = dt.NewRow();
                row["IdTelefono"]=item.IdTelefono;
                row["Numero"] = item.Numero;
                row["Tipo"] = item.Tipo;
                row["IdPersona"]=item.IdPersona;
                dt.Rows.Add(row);
            }
            return dt;
        }
        #endregion
        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DesHabilitar();
        }

        private void BtnCollapseMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Visible;
            btnCollapseMenu.Visibility = Visibility.Collapsed;
            imgLogo.Visibility = Visibility.Collapsed;
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            //pers = new Persona();
           // pers = new Persona(txtNombreCliente.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, sexo, Convert.ToDateTime(dtFechaNacimiento.SelectedDate.ToString()), txtDocumento.Text, telefonosClientes);
           
            brl.Delete( Convert.ToInt32(txtId.Text));
            LoadDatagrid();
            
        }
            char sexo;

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            switch (operacion)
            {
                case 1:
                    if (cmbSexo.Text == "Masculino")
                    {
                        sexo = 'M';
                    }
                    else
                    {
                        sexo = 'F';
                    }
                    pers = new Persona(txtNombreCliente.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, sexo, Convert.ToDateTime(dtFechaNacimiento.SelectedDate.ToString()), txtDocumento.Text, telefonosClientes);
                    clie = new Cliente(txtNit.Text, txtRazon.Text);
                    brl = new ClienteBRL(pers, clie);
                    brl.Insert();
                    lblAnuncio.Content = "Cliente insertado con exito";
                    break;
                case 2:
                    if (cmbSexo.Text == "Masculino")
                    {
                        sexo = 'M';
                    }
                    else
                    {
                        sexo = 'F';
                    }
                    pers = new Persona(txtNombreCliente.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, sexo, Convert.ToDateTime(dtFechaNacimiento.SelectedDate.ToString()), txtDocumento.Text, telefonosClientes);
                    pers.IdPersona = Convert.ToInt32(txtId.Text);
                    clie = new Cliente(txtNit.Text, txtRazon.Text);
                    clie.IdCliente = Convert.ToInt32(txtId.Text);
                    brl = new ClienteBRL(pers, clie);
                    brl.Update();
                    lblAnuncio.Content = "Cliente actualizado con exito";
                    break;
            }
            DesHabilitar();
            LoadDatagrid();
        }

        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(1);

            LoadDatagrid();
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(2);

            LoadDatagrid();
        }

        private void BtnOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenMenu.Visibility = Visibility.Collapsed;
            btnCollapseMenu.Visibility = Visibility.Visible;
            imgLogo.Visibility = Visibility.Visible;
        }

        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void dgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvDatos.Items.Count > 0 && dgvDatos.SelectedItem != null)
            {
                try
                {
                    DataRowView dataRow = (DataRowView)dgvDatos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    brl = new ClienteBRL();
                    pers = brl.Get(id);
                   clie = brl.Get2(id);
                   txtId.Text = id.ToString();

                    txtNombreCliente.Text = pers.Nombres;
                    txtPrimerApellido.Text = pers.PrimerApellido;
                    txtSegundoApellido.Text = pers.SegundoApellido;
                    cmbSexo.Text = pers.Sexo.ToString();
                    dtFechaNacimiento.SelectedDate = pers.FechaNacimiento;
                    txtDocumento.Text = pers.Documento;
                    txtNit.Text = clie.Nit;
                    txtRazon.Text = clie.RazonSocial;
                    //foreach (var item in brl.GetTelefono(id).DefaultView)
                    //{
                    //    Telefono telefono = new Telefono(item);
                    //    telefonosClientes.Add(telefono); 
                    //}
                    

                    dgvTelefonos.ItemsSource = brl.GetTelefono(id).DefaultView;
                    dgvTelefonos.Columns[0].Visibility = Visibility.Hidden;
                    dgvTelefonos.Columns[3].Visibility = Visibility.Hidden;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadDatagrid();
        }

        private void btnTelefono_Click(object sender, RoutedEventArgs e)
        {
            if (cmbTipo.Text == "celular")
            {
                tipoTel = 'C';
            }
            else
            {
                tipoTel = 'F';
            }
            Telefono telefono = new Telefono(txtNumero.Text, tipoTel);
            telefonosClientes.Add(telefono);
            dgvTelefonos.ItemsSource = ConvertToDataTable(telefonosClientes).DefaultView;
            dgvTelefonos.Columns[0].Visibility = Visibility.Hidden;
            dgvTelefonos.Columns[3].Visibility = Visibility.Hidden;
            
            try
            {

                brl = new ClienteBRL();
                dgvDatos.ItemsSource = brl.Select().DefaultView;
                dgvDatos.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void dgvTelefonos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvTelefonos.Items.Count > 0 && dgvTelefonos.SelectedItem != null)
            {
                try
                {
                    DataRowView dataRow = (DataRowView)dgvTelefonos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    brl = new ClienteBRL();
                    tel = brl.GetUnTelefono(id);
                    txtDocumento.Text = pers.Documento;
                    txtNumero.Text = tel.Numero;
                    if (tel.Tipo=='C')
                    {
                        cmbTipo.Text = "celular";
                    }
                    else
                    {
                        cmbTipo.Text = "Fijo";
                    }
                    //dgvTelefonos.ItemsSource = brl.GetTelefono(id).DefaultView;
                    //dgvTelefonos.Columns[0].Visibility = Visibility.Hidden;
                    //dgvTelefonos.Columns[3].Visibility = Visibility.Hidden;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
